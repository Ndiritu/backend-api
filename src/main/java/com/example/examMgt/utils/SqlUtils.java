package com.example.examMgt.utils;

import org.apache.log4j.Logger;
import org.yaml.snakeyaml.Yaml;

import java.io.InputStream;
import java.sql.Array;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SqlUtils {

    private static final Logger logger = Logger.getLogger(SqlUtils.class);

    public static final SqlUtils instance = new SqlUtils();

    private Map<String, String> queryMap = new HashMap<>();

    private SqlUtils() {
        try {
            InputStream inputStream = this.getClass().getResourceAsStream("/sql.yml");
            queryMap = (Map<String, String>) new Yaml().load(inputStream);
        } catch (Exception ex) {
            logger.error("sql.yml not found", ex);
        }
    }

    public String findSqlStatement(String key) {
        return queryMap.get(key);
    }

    public static Array getPostgresBigintArray(List<Long> listOfItems, Connection dataSourceConnection) throws SQLException {
        Long[] longArr = listOfItems.toArray(new Long[listOfItems.size()]);
        Array longSqlArr = dataSourceConnection.createArrayOf("bigint", longArr);
        dataSourceConnection.close();
        return longSqlArr;
    }

}
