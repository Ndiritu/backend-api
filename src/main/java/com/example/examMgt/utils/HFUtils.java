package com.example.examMgt.utils;

import com.example.examMgt.model.HFUser;
import org.apache.log4j.Logger;

import java.io.*;

public class HFUtils {

    private static final Logger logger = Logger.getLogger(HFUtils.class);

    public static void serialiseHFUser(HFUser hfUser) throws Exception {
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(
                new FileOutputStream(hfUser.getName() + ".jso")
        );
        objectOutputStream.writeObject(hfUser);
    }

    public static HFUser tryDeserialiseHFUser(String name) throws Exception {
        File file = new File(name + ".jso");
        if (file.exists()) {
            return deserialiseHFUser(name);
        }
        return null;
    }

    private static HFUser deserialiseHFUser(String name) throws Exception {
        ObjectInputStream objectInputStream = new ObjectInputStream(
                new FileInputStream(name + ".jso")
        );
        return (HFUser) objectInputStream.readObject();
    }
}
