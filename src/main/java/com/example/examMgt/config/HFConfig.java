package com.example.examMgt.config;

import com.example.examMgt.model.HFUser;
import com.example.examMgt.model.UserSession;
import com.example.examMgt.utils.HFUtils;
import org.apache.log4j.Logger;
import org.hyperledger.fabric.sdk.*;
import org.hyperledger.fabric.sdk.security.CryptoSuite;
import org.hyperledger.fabric_ca.sdk.Attribute;
import org.hyperledger.fabric_ca.sdk.HFCAClient;
import org.hyperledger.fabric_ca.sdk.RegistrationRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Configuration
public class HFConfig {

    private static final Logger logger = Logger.getLogger(HFConfig.class);

    private static String hfCaClientUrl;
    private static String adminUsername;
    private static String adminPassword;
    private static String adminOrganisation;
    private static String adminMspId;

    private static String peer0ContainerId;
    private static String peer0Url;
    private static String eventHubContainerId;
    private static String eventHubUrl;
    private static String ordererContainerId;
    private static String ordererUrl;

    public static String channelExamsId;
    public static String channelResultsId;

    private static String defaultOrganisation;
    private static String defaultMSP;

    private static HFCAClient hfCaClient = null;
    public static HFUser admin;

    //TODO: Figure out whether I can use @beans to init vars and make vars final

    @Autowired
    public HFConfig(
            @Value("${hf.ca.url}") String hfCaClientUrl,
            @Value("${hf.admin.username}") String adminUsername,
            @Value("${hf.admin.password}") String adminPassword,
            @Value("${hf.admin.organisation}") String adminOrganisation,
            @Value("${hf.admin.mspId}") String adminMspId,
            @Value("${hf.peer0.containerId}") String peer0ContainerId,
            @Value("${hf.peer0.url}") String peer0Url,
            @Value("${hf.eventHub.containerId}") String eventHubContainerId,
            @Value("${hf.eventHub.url}") String eventHubUrl,
            @Value("${hf.orderer.containerId}") String ordererContainerId,
            @Value("${hf.orderer.url}") String ordererUrl,
            @Value("${hf.channel.channelExams.id}") String channelExamsId,
            @Value("${hf.channel.channelResults.id}") String channelResultsId,
            @Value("${hf.default.organisation}") String defaultOrganisation,
            @Value("${hf.default.msp}") String defaultMSP
    ) {
        try {
            HFConfig.hfCaClientUrl = hfCaClientUrl;
            HFConfig.adminUsername = adminUsername;
            HFConfig.adminPassword = adminPassword;
            HFConfig.adminOrganisation = adminOrganisation;
            HFConfig.adminMspId = adminMspId;
            HFConfig.peer0ContainerId = peer0ContainerId;
            HFConfig.peer0Url = peer0Url;
            HFConfig.eventHubContainerId = eventHubContainerId;
            HFConfig.eventHubUrl = eventHubUrl;
            HFConfig.ordererContainerId = ordererContainerId;
            HFConfig.ordererUrl = ordererUrl;
            HFConfig.channelExamsId = channelExamsId;
            HFConfig.channelResultsId = channelResultsId;
            HFConfig.defaultOrganisation = defaultOrganisation;
            HFConfig.defaultMSP = defaultMSP;

            initHFCAClient();
            initAdmin();
        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
        }
    }

    public static HFClient getHfClient(long userId,
                                       UserSession.UserType userType,
                                       String email) throws Exception {
        String username = email;
        HFUser hfUser = HFUtils.tryDeserialiseHFUser(username);
        if (hfUser == null) {
            Attribute userIdAttr = new Attribute("userId", String.valueOf(userId));
            Attribute userTypeAttr = new Attribute("userType", userType.toString());

            RegistrationRequest rr = new RegistrationRequest(username, defaultOrganisation);
            rr.addAttribute(userIdAttr);
            rr.addAttribute(userTypeAttr);
            String enrollmentSecret = hfCaClient.register(rr, admin);
            Enrollment enrollment = hfCaClient.enroll(username, enrollmentSecret);
            hfUser = new HFUser(username, defaultOrganisation, defaultMSP, enrollment);
            HFUtils.serialiseHFUser(hfUser);
        }

        HFClient hfClient = HFClient.createNewInstance();
        CryptoSuite cryptoSuite = CryptoSuite.Factory.getCryptoSuite();
        hfClient.setCryptoSuite(cryptoSuite);
        hfClient.setUserContext(hfUser);

        return hfClient;
    }

    public static Channel getChannel(HFClient hfClient, String channelId) throws Exception {
        String grpcUrl = "grpc://";
        Peer peer0 = hfClient.newPeer(peer0ContainerId, grpcUrl + peer0Url);
        EventHub eventHub = hfClient.newEventHub(eventHubContainerId, grpcUrl + eventHubUrl);
        Orderer orderer = hfClient.newOrderer(ordererContainerId, grpcUrl + ordererUrl);

        Channel channel = hfClient.newChannel(channelId);
        channel.addPeer(peer0);
        channel.addEventHub(eventHub);
        channel.addOrderer(orderer);
        channel.initialize();

        return channel;
    }

    private static void initAdmin() throws Exception {
        HFUser admin = HFUtils.tryDeserialiseHFUser(adminUsername);
        if (admin == null) {
            Enrollment adminEnrollment = hfCaClient.enroll(adminUsername, adminPassword);
            admin = new HFUser(adminUsername, adminOrganisation, adminMspId, adminEnrollment);
            HFUtils.serialiseHFUser(admin);
        }
        HFConfig.admin = admin;
    }

    private void initHFCAClient() throws Exception {
        CryptoSuite cryptoSuite = CryptoSuite.Factory.getCryptoSuite();
        HFCAClient hfcaClient = HFCAClient.createNewInstance(hfCaClientUrl, null);
        hfcaClient.setCryptoSuite(cryptoSuite);
        HFConfig.hfCaClient = hfcaClient;
    }
}
