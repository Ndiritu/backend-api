package com.example.examMgt.controller;

import com.example.examMgt.config.HFConfig;
import com.example.examMgt.model.ApiResponse;
import com.example.examMgt.model.UserSession;
import com.example.examMgt.model.db.*;
import com.example.examMgt.model.viewModel.ChairpersonView;
import com.example.examMgt.model.viewModel.NotificationView;
import com.example.examMgt.service.*;
import io.swagger.annotations.ApiOperation;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/v1/user")
public class UserController extends BaseController {

    private static final Logger logger = Logger.getLogger(UserController.class);

    @Autowired private LecturerService lecturerService;
    @Autowired private ChairpersonService chairpersonService;
    @Autowired private ExternalExaminerService externalExaminerService;
    @Autowired private ExamCentreStaffService examCentreStaffService;
    @Autowired private SysAdminSerice sysAdminSerice;
    @Autowired private UserSession userSession;
    @Autowired private LoginSessionService loginSessionService;
    @Autowired private NotificationService notificationService;

    //todo: log in
    //todo: log out

    @RequestMapping(value = "/notifications/new", method = RequestMethod.POST)
    @ApiOperation(value = "Create new notification", notes = "Create new notification")
    public ResponseEntity<ApiResponse> createNotification(@RequestBody Notification notification) {
        long startTime = System.currentTimeMillis();
        try {
            notificationService.createNotification(notification);
            return getApiResponse(startTime, "", null, HttpStatus.CREATED);
        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/notifications/seen", method = RequestMethod.GET)
    @ApiOperation(value = "Mark notification as seen", notes = "Mark notification as seen")
    public ResponseEntity<ApiResponse> markNotificationsAsSeen(@RequestParam List<Long> ids) {
        long startTime = System.currentTimeMillis();
        try {
            notificationService.markNotificationAsSeen(ids);
            return getApiResponse(startTime, "", null, HttpStatus.OK);
        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/notifications", method = RequestMethod.GET)
    @ApiOperation(value = "Get all user notifications", notes = "Get all user notifications")
    public ResponseEntity<ApiResponse> getAllUserNotifications() {
        long startTime = System.currentTimeMillis();
        try {
            List<Notification> notifications = notificationService.getNotificationsBySender(
                    getUserSession().getUserType(), getUserSession().getId()
            );
            List<NotificationView> notificationViews = notificationService.convertNotificationToNotificationView(notifications);

            HashMap<String, List<NotificationView>> results = new HashMap<>();
            results.put("notifications", notificationViews);
            return getApiResponse(startTime, "", null, HttpStatus.OK);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    @ApiOperation(value = "Log into account", notes = "Log into account")
    public ResponseEntity<ApiResponse> logIn(@RequestParam String email,
                                             @RequestParam String password) {
        long startTime = System.currentTimeMillis();
        try {
            UserSession authedSession = new UserSession();

            Lecturer lecturer = lecturerService.authenticateLecturer(email, password);
            if (lecturer != null) {
                ChairpersonView chairpersonView = chairpersonService.getCurrentChairperson();
                if (chairpersonView.getLecturerId() == lecturer.getId()) {
                    //is chairperson
                    authedSession.setUserType(UserSession.UserType.CHAIRPERSON);
                    authedSession.setId(chairpersonView.getId());
                    authedSession.setFirstName(chairpersonView.getFirstName());
                    authedSession.setSurname(chairpersonView.getSurname());
                    authedSession.setEmail(chairpersonView.getEmail());
                    authedSession.setHfClient(
                            HFConfig.getHfClient(
                                    authedSession.getId(),
                                    UserSession.UserType.CHAIRPERSON,
                                    email)
                    );
                    authedSession.setChannelExams(HFConfig.getChannel(
                            authedSession.getHfClient(), HFConfig.channelExamsId
                    ));
                    authedSession.setChannelResults(HFConfig.getChannel(
                            authedSession.getHfClient(), HFConfig.channelResultsId
                    ));
                } else {
                    //is lec
                    authedSession.setUserType(UserSession.UserType.LECTURER);
                    authedSession.setId(lecturer.getId());
                    authedSession.setFirstName(lecturer.getFirstName());
                    authedSession.setSurname(lecturer.getSurname());
                    authedSession.setEmail(lecturer.getEmail());
                    authedSession.setHfClient(
                            HFConfig.getHfClient(
                                    lecturer.getId(),
                                    UserSession.UserType.LECTURER,
                                    email)
                    );
                    logger.debug("user session hfClient set");
                    authedSession.setChannelExams(HFConfig.getChannel(
                            authedSession.getHfClient(), HFConfig.channelExamsId
                    ));
                    logger.debug("user session channel exams set");
                    authedSession.setChannelResults(HFConfig.getChannel(
                            authedSession.getHfClient(), HFConfig.channelResultsId
                    ));
                    logger.debug("user session channel results set");
                }
            } else {
                ExternalExaminer externalExaminer = externalExaminerService.authenticate(email, password);
                if (externalExaminer != null) {
                    //is ext examiner
                    authedSession.setUserType(UserSession.UserType.EXT_EXAMINER);
                    authedSession.setId(externalExaminer.getId());
                    authedSession.setFirstName(externalExaminer.getFirstName());
                    authedSession.setSurname(externalExaminer.getSurname());
                    authedSession.setEmail(externalExaminer.getEmail());
                    authedSession.setHfClient(
                            HFConfig.getHfClient(
                                    authedSession.getId(),
                                    UserSession.UserType.EXT_EXAMINER,
                                    email)
                    );
                    authedSession.setChannelExams(HFConfig.getChannel(
                            authedSession.getHfClient(), HFConfig.channelExamsId
                    ));
                    authedSession.setChannelResults(HFConfig.getChannel(
                            authedSession.getHfClient(), HFConfig.channelResultsId
                    ));
                } else {
                    ExamCentreStaff examCentreStaff = examCentreStaffService.authenticate(email, password);
                    if (examCentreStaff != null) {
                        //is exam centre staff
                        authedSession.setUserType(UserSession.UserType.EXAM_CENTRE_STAFF);
                        authedSession.setId(examCentreStaff.getId());
                        authedSession.setFirstName(examCentreStaff.getFirstName());
                        authedSession.setSurname(examCentreStaff.getSurname());
                        authedSession.setEmail(examCentreStaff.getEmail());
                        authedSession.setHfClient(
                                HFConfig.getHfClient(
                                        authedSession.getId(),
                                        UserSession.UserType.EXAM_CENTRE_STAFF,
                                        email)
                        );
                        authedSession.setChannelExams(HFConfig.getChannel(
                                authedSession.getHfClient(), HFConfig.channelExamsId
                        ));
                        authedSession.setChannelResults(HFConfig.getChannel(
                                authedSession.getHfClient(), HFConfig.channelResultsId
                        ));
                    } else {
                        SysAdmin sysAdmin = sysAdminSerice.authenticate(email, password);
                        if (sysAdmin != null) {
                            authedSession.setUserType(UserSession.UserType.SYS_ADMIN);
                            authedSession.setId(sysAdmin.getId());
                            authedSession.setFirstName(sysAdmin.getFirstName());
                            authedSession.setSurname(sysAdmin.getSurname());
                            authedSession.setEmail(sysAdmin.getEmail());
                            authedSession.setHfClient(
                                    HFConfig.getHfClient(
                                            authedSession.getId(),
                                            UserSession.UserType.SYS_ADMIN,
                                            email)
                            );
                            authedSession.setChannelExams(HFConfig.getChannel(
                                    authedSession.getHfClient(), HFConfig.channelExamsId
                            ));
                            authedSession.setChannelResults(HFConfig.getChannel(
                                    authedSession.getHfClient(), HFConfig.channelResultsId
                            ));
                        } else {
                            return getApiResponse(startTime, "Invalid credentials", null, HttpStatus.UNAUTHORIZED);
                        }
                    }
                }
            }

            setUserSession(authedSession);
            loginSessionService.refreshSession(authedSession);

            HashMap<String, Object> userSessionMap = new HashMap<>();
            userSessionMap.put("id", getUserSession().getId());
            userSessionMap.put("firstName", getUserSession().getFirstName());
            userSessionMap.put("surname", getUserSession().getSurname());
            userSessionMap.put("email", getUserSession().getEmail());
            userSessionMap.put("userType", getUserSession().getUserType());


            logger.debug("initialised global user session: " + getUserSession().toString());
            HashMap<String, HashMap<String, Object>> result = new HashMap<>();
            result.put("userSession", userSessionMap);

            return getApiResponse(startTime, "", result, HttpStatus.OK);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    @ApiOperation(value = "Log out of account", notes = "Log out of account")
    public ResponseEntity<ApiResponse> logOut() {
        long startTime = System.currentTimeMillis();
        try {
            loginSessionService.logOutSession(getUserSession());
            return getApiResponse(startTime, "", null, HttpStatus.OK);
        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/{userType}/{userId}/two-fa/refresh", method = RequestMethod.POST)
    @ApiOperation(value = "Refresh 2FA code", notes = "Refresh 2FA code")
    public ResponseEntity<ApiResponse> refresh2FACode(@PathVariable UserSession.UserType userType,
                                                      @PathVariable Long userId) {
        long startTime = System.currentTimeMillis();
        try {
            loginSessionService.refreshSession(getUserSession());
            return getApiResponse(startTime, "", null, HttpStatus.OK);
        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/{userType}/{userId}/two-fa", method = RequestMethod.GET)
    @ApiOperation(value = "Verify 2FA Code", notes = "Verify 2FA Code")
    public ResponseEntity<ApiResponse> verify2FA(@PathVariable UserSession.UserType userType,
                                                 @PathVariable Long userId,
                                                 @RequestParam String code) {
        long startTime = System.currentTimeMillis();
        try {
            LoginSessionService.Verify2FACodeResponse response = loginSessionService.verify2FACode(
                    userType, userId, code
            );

            if (LoginSessionService.Verify2FACodeResponse.EXPIRED_CODE.equals(response)) {
                return getApiResponse(startTime, "EXPIRED_CODE", null, HttpStatus.BAD_REQUEST);
            }
            else if (LoginSessionService.Verify2FACodeResponse.INVALID_CODE.equals(response)) {
                return getApiResponse(startTime, "INVALID_CODE", null, HttpStatus.BAD_REQUEST);
            }
            else {
                return getApiResponse(startTime, "", null, HttpStatus.OK);
            }

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
