package com.example.examMgt.controller;

import com.example.examMgt.model.ApiResponse;
import com.example.examMgt.model.TxHistoryInfo;
import com.example.examMgt.model.UserSession;
import com.example.examMgt.model.ValidationResponse;
import com.example.examMgt.model.hf.Section;
import com.example.examMgt.model.hf.history.SectionHistory;
import com.example.examMgt.service.ExaminationService;
import com.example.examMgt.service.QsccService;
import com.example.examMgt.service.SectionService;
import io.swagger.annotations.ApiOperation;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;


@RestController
@RequestMapping(value = "/v1/section")
public class SectionController extends BaseController {

    private static final Logger logger = Logger.getLogger(SectionController.class);

    @Autowired
    private SectionService sectionService;

    @Autowired
    private QsccService qsccService;

    @Autowired
    private ExaminationService examinationService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    @ApiOperation(value = "Get all sections", notes = "Get all sections")
    public ResponseEntity<ApiResponse> getAllSections() {
        long startTime = System.currentTimeMillis();
        try {
            List<Section> sections = sectionService.getAllSections(getUserSession());
            if (sections == null) {
                throw new Exception("Unable to fetch all sections");
            }
            HashMap<String, List<Section>> results = new HashMap<>();
            results.put("sections", sections);
            return getApiResponse(startTime, "", results, HttpStatus.OK);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/{sectionId}/history", method = RequestMethod.GET)
    @ApiOperation(value = "Get history of section", notes = "Get history of section")
    public ResponseEntity<ApiResponse> getSectionHistory(@PathVariable String sectionId) {
        long startTime = System.currentTimeMillis();
        try {
            if (sectionId.isEmpty() ||
                    sectionService.getSectionById(getUserSession(), sectionId) == null) {
                return getApiResponse(startTime, "Invalid sectionId", null, HttpStatus.BAD_REQUEST);
            }

            List<SectionHistory> sectionHistories = sectionService.getSectionHistory(getUserSession(), sectionId);

            HashMap<String, TxHistoryInfo> txHistoryInfoHashMap = new HashMap<>();
            for (SectionHistory sectionHistory : sectionHistories) {
                String txId = sectionHistory.getTxId();
                txHistoryInfoHashMap.put(txId,
                        qsccService.getTransactionById(getUserSession().getChannelExams(), txId));
            }

            HashMap<String, Object> results = new HashMap<>();
            results.put("sectionHistory", sectionHistories);
            results.put("txHistoryInfo", txHistoryInfoHashMap);
            return getApiResponse(startTime, "", results, HttpStatus.OK);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/{sectionId}", method = RequestMethod.DELETE)
    @ApiOperation(value = "Delete section", notes = "Delete section")
    public ResponseEntity<ApiResponse> deleteSection(@PathVariable String sectionId) {
        long startTime = System.currentTimeMillis();
        try {
            if (sectionService.getSectionById(getUserSession(), sectionId) == null) {
                return getApiResponse(startTime, "Invalid sectionid", null, HttpStatus.BAD_REQUEST);
            }

            if (UserSession.UserType.SYS_ADMIN.equals(getUserSession().getUserType())) {
                return getApiResponse(startTime, "SYS_ADMIN_ERROR", null, HttpStatus.BAD_REQUEST);
            }

            if (!sectionService.deleteSection(getUserSession(), sectionId)) {
                throw new Exception("Unable to delete section");
            }
            return getApiResponse(startTime, "", null, HttpStatus.OK);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/{sectionId}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Update section", notes = "Update section")
    public ResponseEntity<ApiResponse> updateSection(@PathVariable String sectionId,
                                                     @RequestBody Section updatedSection) {
        long startTime = System.currentTimeMillis();
        try {
            if (sectionId.isEmpty() ||
                    sectionService.getSectionById(getUserSession(), sectionId) == null) {
                return getApiResponse(startTime, "Invalid sectionId", null, HttpStatus.BAD_REQUEST);
            }

            if (UserSession.UserType.SYS_ADMIN.equals(getUserSession().getUserType())) {
                return getApiResponse(startTime, "SYS_ADMIN_ERROR", null, HttpStatus.BAD_REQUEST);
            }

            ValidationResponse validationResponse = sectionService.validateSection(updatedSection);
            if (!validationResponse.isValid()) {
                return getApiResponse(startTime, validationResponse.getMessage(), null, HttpStatus.BAD_REQUEST);
            }

            updatedSection.setId(sectionId);
            if (!sectionService.updateSection(getUserSession(), updatedSection)) {
                throw new Exception("Unable to update section. Unexpected error");
            }
            return getApiResponse(startTime, "", null, HttpStatus.OK);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Create section", notes = "Create section")
    public ResponseEntity<ApiResponse> createSection(@RequestBody Section section) {
        long startTime = System.currentTimeMillis();
        try {
            ValidationResponse validationResponse = sectionService.validateSection(section);
            if (!validationResponse.isValid()) {
                return getApiResponse(startTime, validationResponse.getMessage(), null, HttpStatus.BAD_REQUEST);
            }

            examinationService.updateDisplayIndexesBeforeCreate(getUserSession(), section.getExamId(), section.getDisplayIndex());

            Section createdSection = sectionService.createSection(getUserSession(), section);
            if (createdSection == null) {
                throw new Exception("Unable to create section: " + createdSection + " . Unexpected error");
            }
            HashMap<String, Section> result = new HashMap<>();
            result.put("section", section);
            return getApiResponse(startTime, "", result, HttpStatus.CREATED);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
