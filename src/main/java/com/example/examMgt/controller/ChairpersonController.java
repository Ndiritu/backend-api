package com.example.examMgt.controller;

import com.example.examMgt.model.ApiResponse;
import com.example.examMgt.model.UserSession;
import com.example.examMgt.model.hf.ExamApproval;
import com.example.examMgt.model.viewModel.ExaminationView;
import com.example.examMgt.service.ChairpersonService;
import com.example.examMgt.service.ExamApprovalService;
import com.example.examMgt.service.ExaminationService;
import io.swagger.annotations.ApiOperation;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping(value = "/v1/chairperson")
public class ChairpersonController extends BaseController {

    private static final Logger logger = Logger.getLogger(ChairpersonController.class);

    @Autowired
    private ChairpersonService chairpersonService;

    @Autowired
    private ExamApprovalService examApprovalService;

    @Autowired
    private ExaminationService examinationService;


    @RequestMapping(value = "/{chairpersonId}/exams/approval", method = RequestMethod.GET)
    @ApiOperation(value = "Get exams by approval status", notes = "Get exams by approval status")
    public ResponseEntity<ApiResponse> getExamsByApprovalStatus(
            @PathVariable Long chairpersonId,
            @RequestParam(defaultValue = "false") Boolean status
    ) {
        long startTime = System.currentTimeMillis();
        try {
            if (chairpersonId == null
                    || chairpersonService.getChairpersonById(chairpersonId) == null) {
                return getApiResponse(startTime, "Invalid chairpersonId", null, HttpStatus.BAD_REQUEST);
            }

            List<ExamApproval> examApprovals = examApprovalService.getExamsByApprovalStatus(getUserSession(), status);
            if (examApprovals == null) {
                throw new Exception("Unexpected error occurred");
            }
            List<ExaminationView> examinationViews = examinationService.convertApprovalsToExamViews(getUserSession(), examApprovals);
            if (examinationViews == null) {
                throw new Exception("Unexpected error occurred");
            }

            HashMap<String, List<ExaminationView>> result = new HashMap<>();
            result.put("examinations", examinationViews);

            return getApiResponse(startTime, "", result, HttpStatus.OK);
        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
