package com.example.examMgt.controller;

import com.example.examMgt.model.ApiResponse;
import com.example.examMgt.model.TxHistoryInfo;
import com.example.examMgt.model.UserSession;
import com.example.examMgt.model.ValidationResponse;
import com.example.examMgt.model.db.CourseSchedule;
import com.example.examMgt.model.db.ExamPeriod;
import com.example.examMgt.model.db.ExternalExaminer;
import com.example.examMgt.model.hf.*;
import com.example.examMgt.model.hf.history.ExaminationHistory;
import com.example.examMgt.model.viewModel.*;
import com.example.examMgt.service.*;
import io.swagger.annotations.ApiOperation;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping(value = "/v1/examination")
public class ExaminationController extends BaseController {

    private static final Logger logger = Logger.getLogger(ExaminationController.class);

    @Autowired private ExaminationService examinationService;
    @Autowired private CourseService courseService;
    @Autowired private QuestionService questionService;
    @Autowired private SectionService sectionService;
    @Autowired private InstructionService instructionService;
    @Autowired private CommentService commentService;
    @Autowired private ItemModerationService itemModerationService;
    @Autowired private ExamApprovalService examApprovalService;
    @Autowired private ExternalExaminerService externalExaminerService;
    @Autowired private ChairpersonService chairpersonService;
    @Autowired private QsccService qsccService;
    @Autowired private ExamGenerationService examGenerationService;
    @Autowired private ExamPrintingService examPrintingService;
    @Autowired private ExamPeriodService examPeriodService;
    @Autowired private MailService mailService;
    @Autowired private NotificationService notificationService;

    //todo: getExamsByModerationStatus??

    @RequestMapping(value = "/{examId}/sendForModeration", method = RequestMethod.POST)
    @ApiOperation(value = "Send exam for moderation", notes = "Send exam for moderation")
    public ResponseEntity<ApiResponse> sendForModeration(@PathVariable String examId) {
        long startTime = System.currentTimeMillis();
        try {
            Examination examination = examinationService.getExaminationById(getUserSession(), examId);
            List<Examination> examinations = new ArrayList<>();
            examinations.add(examination);

            List<ExaminationView> examinationViews = examinationService.convertExamsToExamViews(
                    getUserSession(), examinations
            );

            String extExaminerFirstName = examinationViews.get(0).getModerationDetails().getExtExaminerFirstName();
            String examName = examination.getName();
            String courseCode = String.valueOf(examinationViews.get(0).getCourseCode());
            String courseName = examinationViews.get(0).getCourseName();

            String to = "philipndiritu@gmail.com";
            String subject = "SCI EMS | Exam Ready for Moderation";
            StringBuilder text = new StringBuilder();
            text.append("Dear ");
            text.append(extExaminerFirstName);
            text.append(",\n\nAn examination is now ready for moderation.\n\nExam Name: ");
            text.append(examName);
            text.append("\nCourse: ");
            text.append(courseCode);
            text.append(" - ");
            text.append(courseName);
            text.append("\n\nRegards,\nSCI Exam Management System");

            mailService.sendMail(to, subject, text.toString());

            return getApiResponse(startTime, "", null, HttpStatus.OK);


        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @RequestMapping(value = "/{examId}/print", method = RequestMethod.POST)
    @ApiOperation(value = "Print exam", notes = "Print exam")
    public ResponseEntity<ApiResponse> printExam(@PathVariable String examId) {
        long startTime = System.currentTimeMillis();
        try {
            Examination exam = examinationService.getExaminationById(getUserSession(), examId);
            if (exam == null) {
                return getApiResponse(startTime, "Invalid examId", null, HttpStatus.BAD_REQUEST);
            }

            if (!examPrintingService.printExam(getUserSession(), exam)) {
                throw new Exception("Unable to print exam");
            }
            return getApiResponse(startTime, "", null, HttpStatus.OK);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/{examId}/history", method = RequestMethod.GET)
    @ApiOperation(value = "Get exam history", notes = "Get exam history")
    public ResponseEntity<ApiResponse> getExamHistory(@PathVariable String examId) {
        long startTime = System.currentTimeMillis();
        try {
            if (examinationService.getExaminationById(getUserSession(), examId) == null) {
                return getApiResponse(startTime, "Invalid examId" ,null, HttpStatus.BAD_REQUEST);
            }

            List<ExaminationHistory> examHistories = examinationService.getHistoryById(getUserSession(), examId);
            List<ExaminationHistoryView> examinationHistoryViews =
                    examinationService.convertExamHistoriesToExamHistoryViews(getUserSession(), examHistories);
            //todo: add txhistory info to history view
            HashMap<String, TxHistoryInfo> txHistoryInfos = new HashMap<>();
            for (ExaminationHistoryView examinationHistoryView : examinationHistoryViews) {
                String txId = examinationHistoryView.getTxId();
                txHistoryInfos.put(txId,
                        qsccService.getTransactionById(getUserSession().getChannelExams(), txId));
            }

            HashMap<String, Object> results = new HashMap<>();
            results.put("examHistory", examinationHistoryViews);
            results.put("txHistoryInfo", txHistoryInfos);
            return getApiResponse(startTime, "", results, HttpStatus.OK);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/{examId}/content", method = RequestMethod.GET)
    @ApiOperation(value = "Get exam content sorted in asc order by display index",
                    notes = "Get exam content sorted in asc order by display index")
    public ResponseEntity<ApiResponse> getExamContent(@PathVariable String examId) {
        long startTime = System.currentTimeMillis();
        try {
            if (examinationService.getExaminationById(getUserSession(), examId) == null) {
                return getApiResponse(startTime, "Invalid examId", null, HttpStatus.BAD_REQUEST);
            }

            //todo: add threads to make this a little faster
            List<Object> content = examinationService.getExamContent(getUserSession(), examId);
            HashMap<String, List<Object>> results = new HashMap<>();
            results.put("content", content);
            return getApiResponse(startTime, "", results, HttpStatus.OK);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/{examId}/sections", method = RequestMethod.GET)
    @ApiOperation(value = "Get sections by examId", notes = "Get sections by examId")
    public ResponseEntity<ApiResponse> getSectionsByExamId(@PathVariable String examId) {
        long startTime = System.currentTimeMillis();
        try {
            if (examinationService.getExaminationById(getUserSession(), examId) == null) {
                return getApiResponse(startTime, "Invalid examId", null, HttpStatus.BAD_REQUEST);
            }

            List<Section> sections = sectionService.getSectionsByExamId(getUserSession(), examId);
            List<SectionView> sectionViews = sectionService.convertToSectionViews(getUserSession(), sections);
            HashMap<String, List<SectionView>> results = new HashMap<>();
            results.put("sections", sectionViews);
            return getApiResponse(startTime, "", results, HttpStatus.OK);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/{examId}/questions", method = RequestMethod.GET)
    @ApiOperation(value = "Get questions by examId", notes = "Get questions by examId")
    public ResponseEntity<ApiResponse> getQuestionsByExamId(@PathVariable String examId) {
        long startTime = System.currentTimeMillis();
        try {
            if (examinationService.getExaminationById(getUserSession(), examId) == null) {
                return getApiResponse(startTime, "Invalid examid", null, HttpStatus.BAD_REQUEST);
            }

            List<Question> questions = questionService.getQuestionsByExamId(getUserSession(), examId);
            List<QuestionView> questionViews = questionService.convertToQuestionViews(getUserSession(), questions);
            HashMap<String, List<QuestionView>> results = new HashMap<>();
            results.put("questions", questionViews);
            return getApiResponse(startTime, "", results, HttpStatus.OK);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/{examId}/instructions", method = RequestMethod.GET)
    @ApiOperation(value = "Get instructions by examId", notes = "Get instructions by examId")
    public ResponseEntity<ApiResponse> getExamInstructions(@PathVariable String examId) {
        long startTime = System.currentTimeMillis();
        try {
            if (examinationService.getExaminationById(getUserSession(), examId) == null) {
                return getApiResponse(startTime, "Invalid examId", null, HttpStatus.BAD_REQUEST);
            }
            List<Instruction> instructions = instructionService.getInstructionsByExamId(getUserSession(), examId);
            List<InstructionView> instructionViews = instructionService.convertToInstructionViews(
                    getUserSession(), instructions
            );
            HashMap<String, List<InstructionView>> results = new HashMap<>();
            results.put("instructions", instructionViews);
            return getApiResponse(startTime, "", results, HttpStatus.OK);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/{examId}", method = RequestMethod.GET)
    @ApiOperation(value = "Get exam by id", notes = "Get exam by id")
    public ResponseEntity<ApiResponse> getExamById(@PathVariable String examId) {
        long startTime = System.currentTimeMillis();
        try {
            Examination exam = examinationService.getExaminationById(getUserSession(), examId);
            if (exam ==  null) {
                return getApiResponse(startTime, "Invalid examId", null, HttpStatus.BAD_REQUEST);
            }

            List<Examination> exams = new ArrayList<>();
            exams.add(exam);
            List<ExaminationView> examViews = examinationService.convertExamsToExamViews(getUserSession(), exams);

            HashMap<String, Object> result = new HashMap<>();
            result.put("exam", examViews.get(0));
            return getApiResponse(startTime, "", result, HttpStatus.OK);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/generation", method = RequestMethod.GET)
    @ApiOperation(value = "Get all exam generations", notes = "Get all exam generations")
    public ResponseEntity<ApiResponse> getAllExamGenerations() {
        long startTime = System.currentTimeMillis();
        try {
            List<ExamGeneration> examGenerations = examGenerationService.getAllExamGenerations(getUserSession());
            HashMap<String, List<ExamGeneration>> result = new HashMap<>();
            result.put("examGenerations", examGenerations);

            return getApiResponse(startTime, "", result, HttpStatus.OK);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/{examId}/collection", method = RequestMethod.GET)
    @ApiOperation(value = "Set exam collection details", notes = "Set exam collection details")
    public ResponseEntity<ApiResponse> setExamCollectionDetails(@PathVariable String examId,
                                                                @RequestParam Boolean status,
                                                                @RequestParam Integer numCopies) {
        long startTime = System.currentTimeMillis();
        try {
            Examination exam = examinationService.getExaminationById(getUserSession(), examId);
            if (exam == null) {
                return getApiResponse(startTime, "Invalid examId", null, HttpStatus.BAD_REQUEST);
            }

            if (UserSession.UserType.SYS_ADMIN.equals(getUserSession().getUserType())) {
                return getApiResponse(startTime, "SYS_ADMIN_ERROR", null, HttpStatus.BAD_REQUEST);
            }

            if (!UserSession.UserType.LECTURER.equals(getUserSession().getUserType())) {
                return getApiResponse(startTime, "Unauthorised user type", null, HttpStatus.BAD_REQUEST);
            }

            if (getUserSession().getId() != exam.getCreatedByLecturerId()) {
                return getApiResponse(startTime, "Unauthorised user id", null, HttpStatus.BAD_REQUEST);
            }

            ExamGeneration examGeneration = examGenerationService.getExamGenerationByExamId(getUserSession(), examId);
            examGeneration.setLecturerId(exam.getCreatedByLecturerId());
            examGeneration.setLecturerStatus(status);
            examGeneration.setLecturerNumCopies(numCopies);

            if (!examGenerationService.updateExamGeneration(getUserSession(), examGeneration)) {
                throw new Exception("Could not set collection info");
            }

            return getApiResponse(startTime, "", null, HttpStatus.OK);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/{examId}/generation", method = RequestMethod.GET)
    @ApiOperation(value = "Set exam generation details", notes = "Set exam generation details")
    public ResponseEntity<ApiResponse> setExamGenerationDetails(@PathVariable String examId,
                                                                @RequestParam Boolean status,
                                                                @RequestParam Integer numCopies) {
        long startTime = System.currentTimeMillis();
        try {
            Examination exam = examinationService.getExaminationById(getUserSession(), examId);
            if (exam == null) {
                return getApiResponse(startTime, "Invalid examId", null, HttpStatus.BAD_REQUEST);
            }

//            if (UserSession.UserType.SYS_ADMIN.equals(getUserSession().getUserType())) {
//                return getApiResponse(startTime, "SYS_ADMIN_ERROR", null, HttpStatus.BAD_REQUEST);
//            }

            if (!UserSession.UserType.EXAM_CENTRE_STAFF.equals(getUserSession().getUserType())) {
                return getApiResponse(startTime, "Unauthorised user type", null, HttpStatus.BAD_REQUEST);
            }

            ExamGeneration examGeneration = new ExamGeneration();
            examGeneration.setExamId(exam.getId());
            examGeneration.setExamCentreStaffId(getUserSession().getId());
            examGeneration.setExamCentreStatus(status);
            examGeneration.setExamCentreNumCopies(numCopies);
            examGeneration.setLecturerId(exam.getCreatedByLecturerId());
            examGeneration.setLecturerStatus(false);
            examGeneration.setLecturerNumCopies(0);

            ExamGeneration createdExamGeneration = examGenerationService.createExamGeneration(
                    getUserSession(), examGeneration);
            if (createdExamGeneration == null) {
                throw new Exception("Unable to create exam generation");
            }

            notificationService.sendExamGeneratedNotification(getUserSession(), examId);

            HashMap<String, ExamGeneration> result = new HashMap<>();
            result.put("examGeneration", createdExamGeneration);
            return getApiResponse(startTime, "", result, HttpStatus.OK);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/{examId}/moderation", method = RequestMethod.GET)
    @ApiOperation(value = "Set exam moderation status", notes = "Set exam moderation status")
    public ResponseEntity<ApiResponse> setModerationStatus(@PathVariable String examId,
                                                           @RequestParam Boolean status) {
        long startTime = System.currentTimeMillis();
        try {
            if (examId.isEmpty()
                    || examinationService.getExaminationById(getUserSession(), examId) == null) {
                return getApiResponse(startTime, "Invalid examId", null, HttpStatus.BAD_REQUEST);
            }

            ItemModeration itemModeration = new ItemModeration();
            itemModeration.setItemType(ItemModeration.ItemType.EXAM);
            itemModeration.setItemId(examId);
            itemModeration.setExtExaminerId(getUserSession().getId());
            itemModeration.setStatus(status);

            if (!itemModerationService.updateItemModeration(getUserSession(), itemModeration)) {
                throw new Exception("Unable to update item moderation");
            }

            ItemModeration updatedExamModeration = itemModerationService.getModerationByItem(
                    getUserSession(), ItemModeration.ItemType.EXAM, examId
            );
            if (updatedExamModeration == null) {
                throw new Exception("Unable to get moderation by item");
            }
            updatedExamModeration.setStatus(status);
            List<ItemModeration> moderations = new ArrayList<>();
            moderations.add(updatedExamModeration);
            List<ItemModerationView> moderationViews = itemModerationService.convertToItemModerationView(
                    moderations
            );

            notificationService.sendExamModeratedNotification(getUserSession(), examId);

            HashMap<String, ItemModerationView> result = new HashMap<>();
            result.put("moderation", moderationViews.get(0));
            return getApiResponse(startTime, "", result, HttpStatus.OK);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/{examId}/approval", method = RequestMethod.POST)
    @ApiOperation(value = "Set approval status", notes = "Set approval status")
    public ResponseEntity<ApiResponse> setExamApprovalStatus(@PathVariable String examId,
                                                             @RequestParam Boolean status) {
        long startTime = System.currentTimeMillis();
        try {
            if (examId.isEmpty()
                    || examinationService.getExaminationById(getUserSession(), examId) == null) {
                return getApiResponse(startTime, "Invalid examId", null, HttpStatus.BAD_REQUEST);
            }

            ExamApproval examApproval = new ExamApproval();
            examApproval.setExamId(examId);
            examApproval.setChairpersonId(getUserSession().getId());
            examApproval.setStatus(status);

            if (!examApprovalService.updateExamApproval(getUserSession(), examApproval)) {
                throw new Exception("Unable to update exam approval status");
            }

            ExamApproval updatedExamApproval = examApprovalService.getApprovalByExamId(getUserSession(), examId);
            if (updatedExamApproval == null) {
                throw new Exception("Unable to get updated approval");
            }

            updatedExamApproval.setStatus(status);
            List<ExamApproval> examApprovals = new ArrayList<>();
            examApprovals.add(updatedExamApproval);
            List<ExamApprovalView> examApprovalViews = examApprovalService.convertApprovalsToApprovalViews(
                    examApprovals);

            HashMap<String, ExamApprovalView> result = new HashMap<>();
            result.put("approval", examApprovalViews.get(0));
            return getApiResponse(startTime, "", result, HttpStatus.OK);
        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

//    @RequestMapping(value = "/moderation", method = RequestMethod.GET)
//    @ApiOperation(value = "Get exams by moderation status", notes = "Get exams by moderation status")
//    public ResponseEntity<ApiResponse> getExamsByModerationStatus(@RequestParam Boolean status) {
//        long startTime = System.currentTimeMillis();
//        try {
//            List<ItemModeration> moderations = itemModerationService.get
//
//        } catch (Exception ex) {
//            logger.error(ex.toString(), ex);
//            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
//        }
//    }

    @RequestMapping(value = "/approval", method = RequestMethod.GET)
    @ApiOperation(value = "Get exams by approval status", notes = "Get exams by approval status")
    public ResponseEntity<ApiResponse> getExamsByApprovalStatus(
            @RequestParam Boolean status
    ) {
        long startTime = System.currentTimeMillis();
        try {
            List<ExamApproval> examApprovals = examApprovalService.getExamsByApprovalStatus(
                    getUserSession(), status
            );
            if (examApprovals == null) {
                throw new Exception("Could not get exams by approval status");
            }

            List<String> examIds = new ArrayList<>();
            for (ExamApproval examApproval : examApprovals) {
                examIds.add(examApproval.getExamId());
            }
            List<Examination> exams = new ArrayList<>();
            Examination exam;
            for (String examId : examIds) {
                exam = examinationService.getExaminationById(getUserSession(), examId);
                if (exam != null) {
                    exams.add(exam);
                }
            }
            List<ExaminationView> examViews = examinationService.convertExamsToExamViews(
                    getUserSession(), exams
            );

            HashMap<String, List<ExaminationView>> results = new HashMap<>();
            results.put("examinations", examViews);
            return getApiResponse(startTime, "", results, HttpStatus.OK);
        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    @ApiOperation(value = "List all examinations", notes = "List all examinations")
    public ResponseEntity<ApiResponse> getAllExaminations() {
        long startTime = System.currentTimeMillis();
        try {
            if (getUserSession() == null) {
                throw new Exception("user session is null");
            } else {
                logger.debug("userSession: " + getUserSession().toString());
            }
            List<Examination> exams = examinationService.getAllExaminations(getUserSession());
            if (exams == null) {
                throw new Exception("Unexpected error occurred");
            }
            List<ExaminationView> examViews = examinationService.convertExamsToExamViews(getUserSession(), exams);
            HashMap<String, List<ExaminationView>> results = new HashMap<>();
            results.put("examinations", examViews);
            return getApiResponse(startTime, "", results, HttpStatus.OK);
        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/{examId}", method = RequestMethod.DELETE)
    @ApiOperation(value = "Delete examination", notes = "Delete examination")
    public ResponseEntity<ApiResponse> deleteExamination(@PathVariable String examId) {
        long startTime = System.currentTimeMillis();
        try {
            if (examinationService.getExaminationById(getUserSession(), examId) == null) {
                return getApiResponse(startTime, "Invalid examId", null, HttpStatus.BAD_REQUEST);
            }

            if (UserSession.UserType.SYS_ADMIN.equals(getUserSession().getUserType())) {
                return getApiResponse(startTime, "SYS_ADMIN_ERROR", null, HttpStatus.BAD_REQUEST);
            }

            //todo: should return updated object instead
            if (!examinationService.deleteExamination(getUserSession(), examId)) {
                throw new Exception("Unable to delete exam");
            }
//            examinationService.deleteExamCreated(examId);

            return getApiResponse(startTime, "", null, HttpStatus.OK);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/{examId}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Update examination", notes = "Update examination")
    public ResponseEntity<ApiResponse> updateExamination(@PathVariable String examId,
                                                         @RequestBody Examination examination) {
        long startTime = System.currentTimeMillis();
        try {
            if (examinationService.getExaminationById(getUserSession(), examId) == null) {
                return getApiResponse(startTime, "Invalid examId", null, HttpStatus.BAD_REQUEST);
            }

            // if (UserSession.UserType.SYS_ADMIN.equals(getUserSession().getUserType())) {
            //     return getApiResponse(startTime, "SYS_ADMIN_ERROR", null, HttpStatus.BAD_REQUEST);
            // }

            ValidationResponse validationResponse = examinationService.validateExamination(examination);
            if (!validationResponse.isValid()) {
                return getApiResponse(startTime, validationResponse.getMessage(), null, HttpStatus.BAD_REQUEST);
            }

            examination.setId(examId);
            //todo: Should return updated object instead
            if (!examinationService.updateExamination(getUserSession(), examination)) {
                throw new Exception("Unable to update exam");
            }
            return getApiResponse(startTime, "", null, HttpStatus.OK);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Create examination", notes = "Create article")
    public ResponseEntity<ApiResponse> createExamination(@RequestBody Examination examination) {
        long startTime = System.currentTimeMillis();
        try {
            ValidationResponse validationResponse = examinationService.validateExamination(examination);
            if (!validationResponse.isValid()) {
                return getApiResponse(startTime, validationResponse.getMessage(), null, HttpStatus.BAD_REQUEST);
            }

//            CourseSchedule courseSchedule = courseService.getCourseScheduleByExamAndPeriod(
//                    examination.getId(), examination.getCourseId()
//            );
//
//            examination.setExamDate(courseSchedule.getExamDate().getTime());
//            examination.setStartTime(courseSchedule.getStartTime().getTime());
//            examination.setDuration(
//                    courseSchedule.getStartTime().getTime() - courseSchedule.getEndTime().getTime()
//            );

            Examination createdExamination = examinationService.createExamination(getUserSession(), examination);
            if (createdExamination == null) {
                throw new Exception("An error occurred while creating examination: " + examination);
            }

//            examinationService.insertIntoExamsCreated(examPeriodId, createdExamination.getId(), createdExamination.getCourseId());

            //get ext examiner assigned for that course
            ExternalExaminer externalExaminer = externalExaminerService.getExtExaminerModeratingCourse(
                    createdExamination.getCourseId()
            );
            //set item moderation to false
            ItemModeration itemModeration = new ItemModeration();
            itemModeration.setItemType(ItemModeration.ItemType.EXAM);
            itemModeration.setItemId(createdExamination.getId());
            itemModeration.setExtExaminerId(externalExaminer.getId());
            itemModeration.setStatus(false);

            if (itemModerationService.createItemModeration(getUserSession(), itemModeration) == null) {
                throw new Exception("Unable to create item moderation");
            }
            //get current chairperson
            ChairpersonView currentChairperson = chairpersonService.getCurrentChairperson();
            //set exam approval to false
            ExamApproval examApproval = new ExamApproval();
            examApproval.setExamId(createdExamination.getId());
            examApproval.setChairpersonId(currentChairperson.getId());
            examApproval.setStatus(false);

            if (examApprovalService.createExamApproval(getUserSession(), examApproval) == null) {
                throw new Exception("Unable to create exam approval");
            }

            HashMap<String, Examination> result = new HashMap<>();
            result.put("examination", createdExamination);

            return getApiResponse(startTime, "", result, HttpStatus.CREATED);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


//    @RequestMapping(value = "/{examId}/period", method = RequestMethod.GET)
//    @ApiOperation(value = "Get exam period by exam id", notes = "Get exam period by exam id")
//    public ResponseEntity<ApiResponse> getExamPeriodByExamId(@PathVariable String examId) {
//        long startTime = System.currentTimeMillis();
//        try {
//            ExamPeriod examPeriod = examPeriodService.getExamPeriodByExamId(examId);
//            HashMap<String, ExamPeriod> results = new HashMap<>();
//            results.put("examPeriod", examPeriod);
//
//            return getApiResponse(startTime, "", results, HttpStatus.OK);
//
//        } catch (Exception ex) {
//            logger.error(ex.toString(), ex);
//            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
//        }
//    }
//
//
//    @RequestMapping(value = "/available-exam-periods", method = RequestMethod.GET)
//    @ApiOperation(value =  "Get available exam periods by courseID", notes = "Get available exam periods by courseID")
//    public ResponseEntity<ApiResponse> getAvailableExamPeriodsByCourseId(@RequestParam Long courseId) {
//        long startTime = System.currentTimeMillis();
//        try {
//            List<ExamPeriod> examPeriods = examPeriodService.getAvailableExamPeriodsByCourseId(courseId);
//
//            HashMap<String, List<ExamPeriod>> results = new HashMap<>();
//            results.put("examPeriods", examPeriods);
//            return getApiResponse(startTime, "", results, HttpStatus.OK);
//
//        } catch (Exception ex) {
//            logger.error(ex.toString(), ex);
//            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
//        }
//    }
//
//
//    @RequestMapping(value = "/all-exam-periods", method = RequestMethod.GET)
//    @ApiOperation(value = "Get all exam periods", notes = "Get all exam periods")
//    public ResponseEntity<ApiResponse> getAllExamPeriods() {
//        long startTime = System.currentTimeMillis();
//        try {
//            List<ExamPeriod> examPeriods = examPeriodService.getAllExamPeriods();
//            HashMap<String, List<ExamPeriod>> results = new HashMap<>();
//            results.put("examPeriods", examPeriods);
//            return getApiResponse(startTime, "", results, HttpStatus.OK);
//
//        } catch (Exception ex) {
//            logger.error(ex.toString(), ex);
//            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
//        }
//    }
//
//    @RequestMapping(value = "/by-exam-period", method = RequestMethod.GET)
//    @ApiOperation(value = "Get exams by exam period", notes = "Get exams by exam period")
//    public ResponseEntity<ApiResponse> getExamsByExamPeriod(@RequestParam Long periodId) {
//        long startTime = System.currentTimeMillis();
//        try {
//            List<String> examIds = examinationService.getExamsByExamPeriod(periodId);
//            List<Examination> examinations = new ArrayList<>();
//            for (String id : examIds) {
//                Examination examination = examinationService.getExaminationById(getUserSession(), id);
//                examinations.add(examination);
//            }
//
//            List<ExaminationView> examinationViews = examinationService.convertExamsToExamViews(
//                    getUserSession(), examinations
//            );
//
//            HashMap<String, List<ExaminationView>> results = new HashMap<>();
//            results.put("examinations", examinationViews);
//            return getApiResponse(startTime, "", results, HttpStatus.OK);
//
//        } catch (Exception ex) {
//            logger.error(ex.toString(), ex);
//            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
//        }
//    }

//    @RequestMapping(value = "/exists", method = RequestMethod.GET)
//    @ApiOperation(value = "Check if exam for period already exists", notes = "Check if exam for period already exists")
//    public ResponseEntity<ApiResponse> checkExamExists(@RequestParam Long courseId,
//                                                       @RequestParam Long periodId) {
//        long startTime = System.currentTimeMillis();
//        try {
//            boolean status = examPeriodService.examAlreadyCreatedForPeriod(courseId, periodId);
//            HashMap<String, Boolean> results = new HashMap<>();
//            results.put("status", status);
//            return getApiResponse(startTime, "", results, HttpStatus.OK);
//
//        } catch (Exception ex) {
//            logger.error(ex.toString(), ex);
//            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
//        }
//    }

}
