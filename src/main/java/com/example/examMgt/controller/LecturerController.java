package com.example.examMgt.controller;

import com.example.examMgt.model.ApiResponse;
import com.example.examMgt.model.UserSession;
import com.example.examMgt.model.db.Course;
import com.example.examMgt.model.hf.Examination;
import com.example.examMgt.model.viewModel.ExaminationView;
import com.example.examMgt.service.CourseService;
import com.example.examMgt.service.ExaminationService;
import com.example.examMgt.service.LecturerService;
import io.swagger.annotations.ApiOperation;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping(value = "/v1/lecturer")
public class LecturerController extends BaseController {

    private static final Logger logger = Logger.getLogger(LecturerController.class);

    @Autowired
    private LecturerService lecturerService;

    @Autowired
    private ExaminationService examinationService;

    @Autowired
    private CourseService courseService;

    @RequestMapping(value = "/{lecturerId}/courses", method = RequestMethod.GET)
    @ApiOperation(value = "Get courses taught by lecturer", notes = "Get courses taught by lecturer")
    public ResponseEntity<ApiResponse> getCoursesByLecturer(@PathVariable Long lecturerId) {
        long startTime = System.currentTimeMillis();
        try {
            if (lecturerService.getLecturerById(lecturerId) == null) {
                return getApiResponse(startTime, "Invalid lecturerId", null, HttpStatus.BAD_REQUEST);
            }

            List<Course> courses = courseService.getCoursesTaughtByLec(lecturerId);
            HashMap<String, List<Course>> results = new HashMap<>();
            results.put("courses", courses);
            return getApiResponse(startTime, "", results, HttpStatus.OK);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/{lecturerId}/exams", method = RequestMethod.GET)
    @ApiOperation(value = "Get exams created by lecturer", notes = "Get exams created by lecturer")
    public ResponseEntity<ApiResponse> getExamsByLecturer(@PathVariable Long lecturerId) {
        long starTime = System.currentTimeMillis();
        try {
            if (lecturerId == null) {
                return getApiResponse(starTime, "Invalid lecturerId", null, HttpStatus.BAD_REQUEST);
            }

            if (lecturerService.getLecturerById(lecturerId) == null) {
                return getApiResponse(starTime, "LecturerId does not exist", null, HttpStatus.BAD_REQUEST);
            }
            List<Examination> exams = examinationService.getExamsByLecturerId(getUserSession(), lecturerId);
            if (exams == null) {
                throw new Exception("An unexpected error occurred");
            }
            List<ExaminationView> examViews = examinationService.convertExamsToExamViews(getUserSession(), exams);
            HashMap<String, List<ExaminationView>> results = new HashMap<>();
            results.put("examinations", examViews);

            return getApiResponse(starTime, "", results, HttpStatus.OK);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(starTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
