package com.example.examMgt.controller;

import com.example.examMgt.model.ApiResponse;
import com.example.examMgt.model.UserSession;
import com.example.examMgt.model.db.Course;
import com.example.examMgt.model.hf.Examination;
import com.example.examMgt.model.viewModel.ExaminationView;
import com.example.examMgt.service.CourseService;
import com.example.examMgt.service.ExaminationService;
import io.swagger.annotations.ApiOperation;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping(value = "/v1/course")
public class CourseController extends BaseController {

    private static final Logger logger = Logger.getLogger(CourseController.class);

    @Autowired
    private CourseService courseService;

    @Autowired
    private ExaminationService examinationService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    @ApiOperation(value = "Get all courses", notes = "Get all courses")
    public ResponseEntity<ApiResponse> getAllCourses() {
        long startTime = System.currentTimeMillis();
        try {
            List<Course> courses = courseService.getAllCourses();
            HashMap<String, List<Course>> result = new HashMap<>();
            result.put("courses", courses);
            return getApiResponse(startTime, "", result, HttpStatus.OK);
        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/{courseId}/exams", method = RequestMethod.GET)
    @ApiOperation(value = "Get examination by courseId", notes = "Get examination by courseId")
    public ResponseEntity<ApiResponse> getExaminationsByCourseId(@PathVariable Long courseId) {
        long startTime = System.currentTimeMillis();
        try {
            if (courseId == null ||
                    courseService.getCourseById(courseId) == null) {
                return getApiResponse(startTime, "Invalid courseId", null, HttpStatus.BAD_REQUEST);
            }

            List<Examination> exams = examinationService.getExamsByCourseId(getUserSession(), courseId);
            if (exams == null) {
                throw new Exception("Unexpected error occured");
            }
            List<ExaminationView> examViews = examinationService.convertExamsToExamViews(getUserSession(), exams);
            HashMap<String, List<ExaminationView>> result = new HashMap<>();
            result.put("examinations", examViews);

            return getApiResponse(startTime, "", result, HttpStatus.OK);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
