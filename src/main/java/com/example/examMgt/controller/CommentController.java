package com.example.examMgt.controller;

import com.example.examMgt.model.ApiResponse;
import com.example.examMgt.model.UserSession;
import com.example.examMgt.model.ValidationResponse;
import com.example.examMgt.model.hf.*;
import com.example.examMgt.model.hf.history.CommentHistory;
import com.example.examMgt.model.viewModel.CommentHistoryView;
import com.example.examMgt.model.viewModel.CommentView;
import com.example.examMgt.service.*;
import io.swagger.annotations.ApiOperation;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping(value = "v1/comment")
public class CommentController extends BaseController {

    private static final Logger logger = Logger.getLogger(CommentController.class);

    @Autowired
    private CommentService commentService;

    @Autowired
    private SectionService sectionService;

    @Autowired
    private InstructionService instructionService;

    @Autowired
    private QuestionService questionService;

    @Autowired
    private LecturerService lecturerService;

    @Autowired
    private ExternalExaminerService externalExaminerService;

    //todo: Get comments by recipient?
    //todo: Get comments by sender?

    @RequestMapping(value = "/item", method = RequestMethod.GET)
    @ApiOperation(value = "Get comments by item", notes = "Get comments by item")
    public ResponseEntity<ApiResponse> getCommentsByItem(
        @RequestParam(name = "itemId") String itemId,
        @RequestParam(name = "itemType") ItemModeration.ItemType itemType
    ) {
        long startTime = System.currentTimeMillis();
        try {
            if (itemType.equals(ItemModeration.ItemType.SECTION)) {
                Section section = sectionService.getSectionById(getUserSession(), itemId);
                if (section == null) {
                    return getApiResponse(startTime, "Invalid sectionId", null, HttpStatus.BAD_REQUEST);
                }
            }
            if (itemType.equals(ItemModeration.ItemType.INSTRUCTION)) {
                Instruction instruction = instructionService.getInstructionById(getUserSession(), itemId);
                if (instruction == null) {
                    return getApiResponse(startTime, "Invalid instructionId", null, HttpStatus.BAD_REQUEST);
                }
            }
            if (itemType.equals(ItemModeration.ItemType.QUESTION)) {
                Question question = questionService.getQuestionById(getUserSession(), itemId);
                if (question == null) {
                    return getApiResponse(startTime, "Invalid questionId", null, HttpStatus.BAD_REQUEST);
                }
            }

            List<Comment> comments = commentService.getCommentsByItem(getUserSession(), itemType, itemId);
            List<CommentView> commentViews = commentService.convertToCommentView(comments);

            HashMap<String, List<CommentView>> results = new HashMap<>();
            results.put("comments", commentViews);

            return getApiResponse(startTime, "", results, HttpStatus.OK);
        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    @ApiOperation(value = "Create comment", notes = "Create comment")
    public ResponseEntity<ApiResponse> createComment(@RequestBody Comment comment) {
        long startTime = System.currentTimeMillis();
        try {
            ValidationResponse validationResponse = commentService.validateComment(comment);
            if (!validationResponse.isValid()) {
                return getApiResponse(startTime, validationResponse.getMessage(), null, HttpStatus.BAD_REQUEST);
            }

            Comment createdComment = commentService.createComment(getUserSession(), comment);
            if (createdComment == null) {
                throw new Exception("Unable to create comment. Unexpected error");
            }
            List<Comment> comments = new ArrayList<>();
            comments.add(createdComment);
            List<CommentView> commentViews = commentService.convertToCommentView(comments);

            HashMap<String, CommentView> results = new HashMap<>();
            results.put("comment", commentViews.get(0));

            return getApiResponse(startTime, "", results, HttpStatus.CREATED);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/{commentId}", method = RequestMethod.DELETE)
    @ApiOperation(value = "Delete comment", notes = "Delete comment")
    public ResponseEntity<ApiResponse> deleteComment(@PathVariable String commentId) {
        long startTime = System.currentTimeMillis();
        try {
            Comment comment = commentService.getCommentById(getUserSession(), commentId);
            if (comment == null) {
                return getApiResponse(startTime, "Invalid commentId", null, HttpStatus.BAD_REQUEST);
            }
            if (getUserSession().getId() != comment.getSenderId()) {
                return getApiResponse(startTime, "User unauthorised to delete comment", null, HttpStatus.BAD_REQUEST);
            }

            if (commentService.deleteComment(getUserSession(), commentId)) {
                return getApiResponse(startTime, "", null, HttpStatus.OK);
            }
            throw new Exception("Unable to delete comment. Unexpected error");

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/{commentId}/history", method = RequestMethod.GET)
    @ApiOperation(value = "Get comment history", notes = "Get comment history")
    public ResponseEntity<ApiResponse> getCommentHistory(@PathVariable String commentId) {
        long startTime = System.currentTimeMillis();
        try {
            if (commentService.getCommentById(getUserSession(), commentId) == null) {
                return getApiResponse(startTime, "Invalid commentId", null, HttpStatus.BAD_REQUEST);
            }

            List<CommentHistory> commentHistoryList = commentService.getCommentHistoryById(
                    getUserSession(), commentId
            );
            if (commentHistoryList == null) {
                throw new Exception("Unexpected error occurred");
            }

            List<CommentHistoryView> commentHistoryViews =
                    commentService.convertCommentHistoryToCommentHistoryView(commentHistoryList);
            HashMap<String, List<CommentHistoryView>> results = new HashMap<>();
            results.put("commentHistories", commentHistoryViews);

            return getApiResponse(startTime, "", results, HttpStatus.OK);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    @ApiOperation(value = "Get all comments", notes = "Get all comments")
    public ResponseEntity<ApiResponse> getAllComments() {
        long startTime = System.currentTimeMillis();
        try {
            List<Comment> comments = commentService.getAllComments(getUserSession());
            if (comments == null) {
                throw new Exception("Unexpected error");
            }
            List<CommentView> commentViews = commentService.convertToCommentView(comments);
            HashMap<String, List<CommentView>> results = new HashMap<>();
            results.put("comments", commentViews);

            return getApiResponse(startTime, "", results, HttpStatus.OK);
        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
