package com.example.examMgt.controller;

import com.example.examMgt.model.ApiResponse;
import com.example.examMgt.model.TxHistoryInfo;
import com.example.examMgt.model.UserSession;
import com.example.examMgt.model.ValidationResponse;
import com.example.examMgt.model.db.ExternalExaminer;
import com.example.examMgt.model.hf.Comment;
import com.example.examMgt.model.hf.Examination;
import com.example.examMgt.model.hf.Instruction;
import com.example.examMgt.model.hf.ItemModeration;
import com.example.examMgt.model.hf.history.InstructionHistory;
import com.example.examMgt.model.viewModel.CommentView;
import com.example.examMgt.model.viewModel.ItemModerationView;
import com.example.examMgt.service.*;
import io.swagger.annotations.ApiOperation;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping(value = "v1/instruction")
public class InstructionController extends BaseController {

    private static final Logger logger = Logger.getLogger(InstructionController.class);

    @Autowired private InstructionService instructionService;
    @Autowired private CommentService commentService;
    @Autowired private ExternalExaminerService externalExaminerService;
    @Autowired private ExaminationService examinationService;
    @Autowired private ItemModerationService itemModerationService;
    @Autowired private QsccService qsccService;

    @RequestMapping(value = "/{instructionId}/moderation", method = RequestMethod.GET)
    @ApiOperation(value = "Set instruction moderation" , notes = "Set instruction moderation")
    public ResponseEntity<ApiResponse> setInstructionModeration(@PathVariable String instructionId,
                                                                @RequestParam Boolean status) {
        long startTime = System.currentTimeMillis();
        try {
            if (instructionId.isEmpty()
                    || instructionService.getInstructionById(getUserSession(), instructionId) == null) {
                return getApiResponse(startTime, "invalid instruction id", null, HttpStatus.BAD_REQUEST);
            }

            if (UserSession.UserType.SYS_ADMIN.equals(getUserSession().getUserType())) {
                return getApiResponse(startTime, "SYS_ADMIN_ERROR", null, HttpStatus.BAD_REQUEST);
            }

            ItemModeration itemModeration = new ItemModeration();
            itemModeration.setItemType(ItemModeration.ItemType.INSTRUCTION);
            itemModeration.setItemId(instructionId);
            itemModeration.setExtExaminerId(getUserSession().getId());
            itemModeration.setStatus(status);

            //todo: return object instead of boolean
            if (!itemModerationService.updateItemModeration(getUserSession(), itemModeration)) {
                throw new Exception("unable to update item moderation");
            }

            ItemModeration updatedItemModeration = itemModerationService.getModerationByItem(
                    getUserSession(), ItemModeration.ItemType.INSTRUCTION, instructionId);
            updatedItemModeration.setStatus(status);

            List<ItemModeration> moderations = new ArrayList<>();
            moderations.add(updatedItemModeration);
            List<ItemModerationView> moderationViews = itemModerationService.convertToItemModerationView(
                    moderations);

            HashMap<String, ItemModerationView> result = new HashMap<>();
            result.put("itemModeration", moderationViews.get(0));
            return getApiResponse(startTime, "", result, HttpStatus.OK);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/{instructionId}/history", method = RequestMethod.GET)
    @ApiOperation(value = "Get instruction history", notes = "Get instruction history")
    public ResponseEntity<ApiResponse> getInstructionHistory(@PathVariable String instructionId) {
        long startTime = System.currentTimeMillis();
        try {
            if (instructionService.getInstructionById(getUserSession(), instructionId) == null) {
                return getApiResponse(startTime, "Invalid instructionId", null, HttpStatus.BAD_REQUEST);
            }

            List<InstructionHistory> instructionHistories = instructionService.getInstructionHistoryById(
                    getUserSession(), instructionId);

            HashMap<String, TxHistoryInfo> txHistoryInfoHashMap = new HashMap<>();
            for (InstructionHistory instructionHistory : instructionHistories) {
                String txId = instructionHistory.getTxId();
                txHistoryInfoHashMap.put(txId,
                        qsccService.getTransactionById(getUserSession().getChannelExams(), txId));

            }

            HashMap<String, Object> result = new HashMap<>();
            result.put("instructionHistory", instructionHistories);
            result.put("txHistoryInfo", txHistoryInfoHashMap);
            return getApiResponse(startTime, "", result, HttpStatus.OK);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/{instructionId}/comments", method = RequestMethod.GET)
    @ApiOperation(value = "Get comments by instruction", notes = "Get comments by instruction")
    public ResponseEntity<ApiResponse> getCommentsByInstruction(@PathVariable String instructionId) {
        long startTime = System.currentTimeMillis();
        try {
            if (instructionService.getInstructionById(getUserSession(), instructionId) == null) {
                return getApiResponse(startTime, "Invalid instructionId", null, HttpStatus.BAD_REQUEST);
            }

            List<Comment> comments = commentService.getCommentsByItem(
                    getUserSession(), ItemModeration.ItemType.INSTRUCTION, instructionId);
            List<CommentView> commentViews = commentService.convertToCommentView(comments);

            HashMap<String, List<CommentView>> result = new HashMap<>();
            result.put("comments", commentViews);
            return getApiResponse(startTime, "", result, HttpStatus.OK);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/{instructionId}/comment", method = RequestMethod.POST)
    @ApiOperation(value = "Add comment to instruction", notes = "Add comment to instruction")
    public ResponseEntity<ApiResponse> addCommentToInstruction(@PathVariable String instructionId,
                                                               @RequestBody Comment comment) {
        long startTime = System.currentTimeMillis();
        try {
            Instruction instruction = instructionService.getInstructionById(getUserSession(), instructionId);
            if (instruction == null) {
                return getApiResponse(startTime, "Invalid instructionId", null, HttpStatus.BAD_REQUEST);
            }

            ValidationResponse validationResponse = commentService.validateComment(comment);
            if (!validationResponse.isValid()) {
                return getApiResponse(startTime, validationResponse.getMessage(), null, HttpStatus.BAD_REQUEST);
            }

            String examId = instruction.getExamId();
            Long recipientId = null;
            if (Comment.RecipientType.valueOf(comment.getRecipientType())
                    == Comment.RecipientType.EXT_EXAMINER) {
                ItemModeration itemModeration = itemModerationService.getModerationByItem(
                        getUserSession(), ItemModeration.ItemType.EXAM, examId
                );
                if (itemModeration == null) {
                    throw new Exception("Could not get item Moderation");
                }
                recipientId = itemModeration.getExtExaminerId();
            }

            if (Comment.RecipientType.valueOf(comment.getRecipientType())
                    == Comment.RecipientType.LECTURER) {
                Examination exam = examinationService.getExaminationById(getUserSession(), examId);
                recipientId = exam.getCreatedByLecturerId();
            }

            if (recipientId == null) {
                throw new Exception("Could not find recipientId");
            }

            //todo: set senderType to usersession type
            comment.setSenderId(getUserSession().getId());
            comment.setSenderType(Comment.SenderType.LECTURER);
            comment.setRecipientId(recipientId);
            comment.setItemType(ItemModeration.ItemType.INSTRUCTION);
            comment.setItemId(instructionId);
            comment.setExamId(examId);

            Comment createdComment = commentService.createComment(getUserSession(), comment);
            if (createdComment == null) {
                throw new Exception("Unable to create comment");
            }
            List<Comment> comments = new ArrayList<>();
            comments.add(createdComment);
            List<CommentView> commentViews = commentService.convertToCommentView(comments);

            HashMap<String, List<CommentView>> result = new HashMap<>();
            result.put("comment", commentViews);
            return getApiResponse(startTime, "", result, HttpStatus.CREATED);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/{instructionId}", method = RequestMethod.DELETE)
    @ApiOperation(value = "Delete instruction", notes = "Delete instruction")
    public ResponseEntity<ApiResponse> deleteInstruction(@PathVariable String instructionId) {
        long startTime = System.currentTimeMillis();
        try {
            Instruction instruction = instructionService.getInstructionById(getUserSession(), instructionId);
            if (instruction == null) {
                return getApiResponse(startTime, "Invalid instructionId", null, HttpStatus.BAD_REQUEST);
            }

            if (UserSession.UserType.SYS_ADMIN.equals(getUserSession().getUserType())) {
                return getApiResponse(startTime, "SYS_ADMIN_ERROR", null, HttpStatus.BAD_REQUEST);
            }

            if (!instructionService.deleteInstruction(getUserSession(), instructionId)) {
                throw new Exception("Unable to delete instruction");
            }
            return getApiResponse(startTime, "", null, HttpStatus.OK);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/{instructionId}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Update instruction", notes = "Update instruction")
    public ResponseEntity<ApiResponse> updateInstruction(@PathVariable String instructionId,
                                                         @RequestBody Instruction instruction) {
        long startTime = System.currentTimeMillis();
        try {
            if (instructionId.isEmpty()
                    ||instructionService.getInstructionById(getUserSession(), instructionId) == null) {
                return getApiResponse(startTime, "Invalid instruction id", null, HttpStatus.BAD_REQUEST);
            }

            // if (UserSession.UserType.SYS_ADMIN.equals(getUserSession().getUserType())) {
            //     return getApiResponse(startTime, "SYS_ADMIN_ERROR", null, HttpStatus.BAD_REQUEST);
            // }

            ValidationResponse validationResponse = instructionService.validateInstruction(instruction);
            if (!validationResponse.isValid()) {
                return getApiResponse(startTime, validationResponse.getMessage(), null, HttpStatus.BAD_REQUEST);
            }

            instruction.setId(instructionId);
            if (!instructionService.updateInstruction(getUserSession(), instruction)) {
                throw new Exception("Unable to update instruction. Unexpected error");
            }

            return getApiResponse(startTime, "", null, HttpStatus.OK);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Create instruction", notes = "Create instruction")
    public ResponseEntity<ApiResponse> createInstruction(@RequestBody Instruction instruction) {
        long startTime = System.currentTimeMillis();
        try {
            ValidationResponse validationResponse =instructionService.validateInstruction(instruction);
            if (!validationResponse.isValid()) {
                return getApiResponse(startTime, validationResponse.getMessage(), null, HttpStatus.BAD_REQUEST);
            }

            examinationService.updateDisplayIndexesBeforeCreate(getUserSession(),
                    instruction.getExamId(),
                    instruction.getDisplayIndex()
            );

            Instruction createdInstruction = instructionService.createInstruction(getUserSession(), instruction);
            if (createdInstruction == null) {
                throw new Exception("Unable to create instruction. Unexpected error");
            }

            Examination exam = examinationService.getExaminationById(getUserSession(), createdInstruction.getExamId());
            if (exam == null) {
                throw new Exception("could not find exam matching id");
            }
            ExternalExaminer externalExaminer = externalExaminerService.getExtExaminerModeratingCourse(
                    exam.getCourseId());
            if (externalExaminer == null) {
                throw new Exception("could not find external examiner moderating course");
            }

            ItemModeration itemModeration = new ItemModeration();
            itemModeration.setItemType(ItemModeration.ItemType.INSTRUCTION);
            itemModeration.setItemId(createdInstruction.getId());
            itemModeration.setExtExaminerId(externalExaminer.getId());
            itemModeration.setStatus(false);

            if (itemModerationService.createItemModeration(getUserSession(), itemModeration) == null) {
                throw new Exception("Unable to create item moderation");
            }

            HashMap<String, Instruction> results = new HashMap<>();
            results.put("instruction", createdInstruction);
            return getApiResponse(startTime, "", results, HttpStatus.CREATED);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
