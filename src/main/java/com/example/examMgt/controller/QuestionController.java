package com.example.examMgt.controller;

import com.example.examMgt.model.ApiResponse;
import com.example.examMgt.model.TxHistoryInfo;
import com.example.examMgt.model.UserSession;
import com.example.examMgt.model.ValidationResponse;
import com.example.examMgt.model.db.ExternalExaminer;
import com.example.examMgt.model.hf.Comment;
import com.example.examMgt.model.hf.Examination;
import com.example.examMgt.model.hf.ItemModeration;
import com.example.examMgt.model.hf.Question;
import com.example.examMgt.model.hf.history.QuestionHistory;
import com.example.examMgt.model.viewModel.CommentView;
import com.example.examMgt.model.viewModel.ItemModerationView;
import com.example.examMgt.model.viewModel.QuestionView;
import com.example.examMgt.service.*;
import io.swagger.annotations.ApiOperation;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping(value = "/v1/question")
public class QuestionController extends BaseController {

    private static final Logger logger = Logger.getLogger(QuestionController.class);

    @Autowired private QuestionService questionService;
    @Autowired private ExaminationService examinationService;
    @Autowired private ExternalExaminerService externalExaminerService;
    @Autowired private ItemModerationService itemModerationService;
    @Autowired private CommentService commentService;
    @Autowired private QsccService qsccService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    @ApiOperation(value = "Get all questions", notes = "Get all questions")
    public ResponseEntity<ApiResponse> getAllQuestions() {
        long startTime = System.currentTimeMillis();
        try {
            List<Question> questions = questionService.getAllQuestions(getUserSession());
            if (questions == null) {
                throw new Exception("Unable to fetch questions");
            }
            HashMap<String, List<Question>> results = new HashMap<>();
            results.put("questions", questions);
            return getApiResponse(startTime, "", results, HttpStatus.OK);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/{questionId}/moderation", method = RequestMethod.GET)
    @ApiOperation(value = "Set question moderation", notes = "Set question moderation")
    public ResponseEntity<ApiResponse> setQuestionModeration(@PathVariable String questionId,
                                                             @RequestParam Boolean status) {
        long startTime = System.currentTimeMillis();
        try {
            if (questionService.getQuestionById(getUserSession(), questionId) == null) {
                return getApiResponse(startTime, "Invalid questionid", null, HttpStatus.BAD_REQUEST);
            }

            if (UserSession.UserType.SYS_ADMIN.equals(getUserSession().getUserType())) {
                return getApiResponse(startTime, "SYS_ADMIN_ERROR", null, HttpStatus.BAD_REQUEST);
            }

            ItemModeration itemModeration = new ItemModeration();
            itemModeration.setItemType(ItemModeration.ItemType.QUESTION);
            itemModeration.setItemId(questionId);
            itemModeration.setExtExaminerId(getUserSession().getId());
            itemModeration.setStatus(status);

            if (!itemModerationService.updateItemModeration(getUserSession(), itemModeration)) {
                throw new Exception("Unable to update item moderation");
            }

            ItemModeration updatedItemModeration = itemModerationService.getModerationByItem(
                    getUserSession(), ItemModeration.ItemType.QUESTION, questionId);
            if (updatedItemModeration == null) {
                throw new Exception("Unable to get moderation by item");
            }
            updatedItemModeration.setStatus(status);

            List<ItemModeration> moderations = new ArrayList<>();
            moderations.add(updatedItemModeration);
            List<ItemModerationView> moderationViews = itemModerationService.convertToItemModerationView(moderations);

            HashMap<String, ItemModerationView> result = new HashMap<>();
            result.put("itemModeration", moderationViews.get(0));
            return getApiResponse(startTime, "", result, HttpStatus.OK);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/{questionId}/comments", method = RequestMethod.GET)
    @ApiOperation(value = "Get comments by questionId", notes = "Get comments by questionId")
    public ResponseEntity<ApiResponse> getCommentsByQuestionId(@PathVariable String questionId) {
        long startTime = System.currentTimeMillis();
        try {
            if (questionId.isEmpty()
                    || questionService.getQuestionById(getUserSession(), questionId) == null) {
                return getApiResponse(startTime, "invalid questionId", null, HttpStatus.BAD_REQUEST);
            }

            List<Comment> comments = commentService.getCommentsByItem(
                    getUserSession(), ItemModeration.ItemType.QUESTION, questionId);
            List<CommentView> commentViews = commentService.convertToCommentView(comments);

            HashMap<String, CommentView> results = new HashMap<>();
            results.put("comments", commentViews.get(0));
            return getApiResponse(startTime, "", results, HttpStatus.OK);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/{questionId}/comment", method = RequestMethod.POST)
    @ApiOperation(value = "Add comment to question" , notes = "Add comment to question")
    public ResponseEntity<ApiResponse> addCommentToQuestion(@PathVariable String questionId,
                                                            @RequestBody Comment comment) {
        long startTime = System.currentTimeMillis();
        try {
            Question question = questionService.getQuestionById(getUserSession(), questionId);
            if (question == null) {
                return getApiResponse(startTime, "Invalid questionId", null, HttpStatus.BAD_REQUEST);
            }

            ValidationResponse validationResponse = commentService.validateComment(comment);
            if (!validationResponse.isValid()) {
                return getApiResponse(startTime, validationResponse.getMessage(), null, HttpStatus.BAD_REQUEST);
            }

            String examId = question.getExamId();
            Long recipientId = null;
            if (Comment.RecipientType.valueOf(comment.getRecipientType())
                    == Comment.RecipientType.EXT_EXAMINER) {
                ItemModeration itemModeration = itemModerationService.getModerationByItem(
                        getUserSession(), ItemModeration.ItemType.EXAM, examId
                );
                if (itemModeration == null) {
                    throw new Exception("Could not get item Moderation");
                }
                recipientId = itemModeration.getExtExaminerId();
            }

            if (Comment.RecipientType.valueOf(comment.getRecipientType())
                    == Comment.RecipientType.LECTURER) {
                Examination exam = examinationService.getExaminationById(getUserSession(), examId);
                recipientId = exam.getCreatedByLecturerId();
            }

            if (recipientId == null) {
                throw new Exception("Could not find recipientId");
            }

            //todo: set senderType to usersession type
            comment.setSenderType(Comment.SenderType.LECTURER);
            comment.setSenderId(getUserSession().getId());
            comment.setRecipientId(recipientId);
            comment.setItemType(ItemModeration.ItemType.QUESTION);
            comment.setItemId(questionId);
            comment.setExamId(examId);

            Comment createdComment = commentService.createComment(getUserSession(), comment);
            if (createdComment == null) {
                throw new Exception("unable to create comment");
            }
            List<Comment> comments = new ArrayList<>();
            comments.add(createdComment);
            List<CommentView> commentViews = commentService.convertToCommentView(comments);

            HashMap<String, List<CommentView>> result = new HashMap<>();
            result.put("comment", commentViews);
            return getApiResponse(startTime, "" , result, HttpStatus.CREATED);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/{questionId}/history", method = RequestMethod.GET)
    @ApiOperation(value = "Get question history", notes = "Get question history")
    public ResponseEntity<ApiResponse> getQuestionHistory(@PathVariable String questionId) {
        long startTime = System.currentTimeMillis();
        try {
            if (questionId.isEmpty() ||
                    questionService.getQuestionById(getUserSession(), questionId) == null) {
                return getApiResponse(startTime, "Invalid questionId", null, HttpStatus.BAD_REQUEST);
            }

            List<QuestionHistory> questionHistoryList = questionService.getQuestionHistory(
                    getUserSession(), questionId);

            HashMap<String, TxHistoryInfo> txHistoryInfos = new HashMap<>();
            for (QuestionHistory questionHistory : questionHistoryList) {
                String txId = questionHistory.getTxId();
                txHistoryInfos.put(txId, qsccService.getTransactionById(
                        getUserSession().getChannelExams(),
                        txId
                ));
            }

            HashMap<String, Object> results = new HashMap<>();
            results.put("questionHistory", questionHistoryList);
            results.put("txHistoryInfo", txHistoryInfos);
            return getApiResponse(startTime, "", results, HttpStatus.OK);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/{questionId}", method = RequestMethod.DELETE)
    @ApiOperation(value = "Delete question", notes = "Delete question")
    public ResponseEntity<ApiResponse> deleteQuestion(@PathVariable String questionId) {
        long startTime = System.currentTimeMillis();
        try {
            Question question = questionService.getQuestionById(getUserSession(), questionId);
            if (questionId.isEmpty()
                    || question == null) {
                return getApiResponse(startTime, "Invalid questionId", null, HttpStatus.BAD_REQUEST);
            }

            if (UserSession.UserType.SYS_ADMIN.equals(getUserSession().getUserType())) {
                return getApiResponse(startTime, "SYS_ADMIN_ERROR", null, HttpStatus.BAD_REQUEST);
            }

            if (Question.QuestionType.MAIN_QUESTION
                    == Question.QuestionType.valueOf(question.getType())) {
                questionService.deleteNestedItems(getUserSession(), question);
            }

            if (!questionService.deleteQuestion(getUserSession(), questionId)) {
                throw new Exception("Unable to delete question");
            }
            return getApiResponse(startTime, "", null, HttpStatus.OK);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/{questionId}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Update question", notes = "Update question")
    public ResponseEntity<ApiResponse> updateQuestion(@PathVariable String questionId,
                                                      @RequestBody Question question) {
        long startTime = System.currentTimeMillis();
        try {
            Question existingQuestion = questionService.getQuestionById(getUserSession(), questionId);
            if (questionId.isEmpty() || existingQuestion == null) {
                return getApiResponse(startTime, "Invalid questionId", null, HttpStatus.BAD_REQUEST);
            }

//            if (UserSession.UserType.SYS_ADMIN.equals(getUserSession().getUserType())) {
//                return getApiResponse(startTime, "SYS_ADMIN_ERROR", null, HttpStatus.BAD_REQUEST);
//            }

            // ItemModeration itemModeration = itemModerationService.getModerationByItem(
            //         getUserSession(), ItemModeration.ItemType.QUESTION, questionId);
            // if (itemModeration.isStatus()) {
            //     return getApiResponse(startTime, "Cannot update a successfully moderated qstn",
            //             null, HttpStatus.BAD_REQUEST);
            // }

            ValidationResponse validationResponse = questionService.validateQuestion(question);
            if (!validationResponse.isValid()) {
                return getApiResponse(startTime, validationResponse.getMessage(), null, HttpStatus.BAD_REQUEST);
            }

            question.setId(questionId);
            if (!questionService.updateQuestion(getUserSession(), question)) {
                throw new Exception("could not update question");
            }
            return getApiResponse(startTime, "", null, HttpStatus.OK);
        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/{questionId}", method = RequestMethod.GET)
    @ApiOperation(value = "Get question by id", notes = "Get question by id")
    public ResponseEntity<ApiResponse> getQuestionById(@PathVariable String questionId) {
        long startTime = System.currentTimeMillis();
        try {
            if (questionId.isEmpty()) {
                return getApiResponse(startTime, "QuestionId cannot be empty", null, HttpStatus.BAD_REQUEST);
            }

            Question question = questionService.getQuestionById(getUserSession(), questionId);
            if (question == null) {
                return getApiResponse(startTime, "Invalid questionId", null, HttpStatus.BAD_REQUEST);
            }
            List<Question> questions = new ArrayList<>();
            questions.add(question);
            List<QuestionView> questionViews = questionService.convertToQuestionViews(getUserSession(), questions);

            HashMap<String, QuestionView> result = new HashMap<>();
            result.put("question", questionViews.get(0));
            return getApiResponse(startTime, "", result, HttpStatus.OK);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation(value = "Create question", notes = "Create question")
    public ResponseEntity<ApiResponse> createQuestion(@RequestBody Question question) {
        long startTime = System.currentTimeMillis();
        try {
            ValidationResponse validationResponse = questionService.validateQuestion(question);
            if (!validationResponse.isValid()) {
                return getApiResponse(startTime, validationResponse.getMessage(), "", HttpStatus.BAD_REQUEST);
            }

            examinationService.updateDisplayIndexesBeforeCreate(getUserSession(), question.getExamId(), question.getDisplayIndex());

            Question createdQstn = questionService.createQuestion(getUserSession(), question);
            if (createdQstn == null) {
                throw new Exception("Could not create qstn. Unexpected error occurred");
            }

            Examination exam = examinationService.getExaminationById(getUserSession(), createdQstn.getExamId());
            if (exam == null) {
                throw new Exception("could not find matching examination");
            }
            ExternalExaminer externalExaminer = externalExaminerService.getExtExaminerModeratingCourse(
                    exam.getCourseId());

            ItemModeration itemModeration = new ItemModeration();
            itemModeration.setItemType(ItemModeration.ItemType.QUESTION);
            itemModeration.setItemId(createdQstn.getId());
            itemModeration.setExtExaminerId(externalExaminer.getId());
            itemModeration.setStatus(false);

            if (itemModerationService.createItemModeration(getUserSession(), itemModeration) == null) {
                throw new Exception("Unable to create item moderation");
            }

            HashMap<String, Question> result = new HashMap<>();
            result.put("question", question);
            return getApiResponse(startTime, "", result, HttpStatus.CREATED);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), "", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
