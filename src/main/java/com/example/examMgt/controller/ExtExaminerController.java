package com.example.examMgt.controller;

import com.example.examMgt.model.ApiResponse;
import com.example.examMgt.model.db.Course;
import com.example.examMgt.model.hf.Examination;
import com.example.examMgt.model.hf.ItemModeration;
import com.example.examMgt.model.viewModel.ExaminationView;
import com.example.examMgt.service.CourseService;
import com.example.examMgt.service.ExaminationService;
import com.example.examMgt.service.ExternalExaminerService;
import com.example.examMgt.service.ItemModerationService;
import io.swagger.annotations.ApiOperation;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping(value = "/v1/ext-examiner")
public class ExtExaminerController extends BaseController {

    private static Logger logger = Logger.getLogger(ExtExaminerController.class);

    @Autowired private CourseService courseService;

    @Autowired private ExternalExaminerService externalExaminerService;

    @Autowired private ItemModerationService itemModerationService;

    @Autowired private ExaminationService examinationService;

    @RequestMapping(value = "/{extExaminerId}/courses", method = RequestMethod.GET)
    @ApiOperation(value = "Get courses by ext examiner", notes = "Get courses by ext examiner")
    public ResponseEntity<ApiResponse> getCoursesByExtExaminer(@PathVariable Long extExaminerId) {
        long startTime = System.currentTimeMillis();

        try {
            if (externalExaminerService.getExternalExaminerById(extExaminerId) == null) {
                return getApiResponse(startTime, "Invalid ext examiner id", null, HttpStatus.BAD_REQUEST);
            }

            List<Course> courses = courseService.getCoursesByExtExaminer(extExaminerId);
            HashMap<String, List<Course>> results = new HashMap<>();
            results.put("courses", courses);
            return getApiResponse(startTime, "", results, HttpStatus.OK);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/{extExaminerId}/exams", method = RequestMethod.GET)
    @ApiOperation(value = "Get exams by ext examiner", notes = "Get exams by ext examiner")
    public ResponseEntity<ApiResponse> getExamsByExtExaminer(@PathVariable Long extExaminerId) {
        long startTime = System.currentTimeMillis();
        try {
            if (externalExaminerService.getExternalExaminerById(extExaminerId) == null) {
                return getApiResponse(startTime, "Invalid extExaminerId", null, HttpStatus.BAD_REQUEST);
            }

            List<ItemModeration> itemModerations = itemModerationService.getModerationsByExtExaminerId(getUserSession());
            List<String> examIds = new ArrayList<>();
            for (ItemModeration itemModeration : itemModerations) {
                if (ItemModeration.ItemType.EXAM == ItemModeration.ItemType.valueOf(itemModeration.getItemType())) {
                    examIds.add(itemModeration.getItemId());
                }
            }

            List<Examination> examinations = new ArrayList<>();
            for (String examId : examIds) {
                Examination examination = examinationService.getExaminationById(getUserSession(), examId);
                examinations.add(examination);
            }

            List<ExaminationView> examinationViews = examinationService.convertExamsToExamViews(
                    getUserSession(), examinations
            );

            HashMap<String, List<ExaminationView>> results = new HashMap<>();
            results.put("examinations", examinationViews);

            return getApiResponse(startTime, "", results, HttpStatus.OK);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return getApiResponse(startTime, ex.toString(), null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}
