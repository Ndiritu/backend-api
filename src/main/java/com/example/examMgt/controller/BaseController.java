package com.example.examMgt.controller;

import com.example.examMgt.model.ApiResponse;
import com.example.examMgt.model.ResponseMetadata;
import com.example.examMgt.model.UserSession;
import org.apache.log4j.Logger;;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class BaseController {

    private static final Logger logger = Logger.getLogger(BaseController.class);

    private static UserSession userSession;

    protected static void setUserSession(UserSession authedSession) {
        userSession = authedSession;
    }

    protected static UserSession getUserSession() {
        return userSession;
    }

    public ResponseEntity<ApiResponse> getApiResponse(long startTime, String message, Object data, HttpStatus statusCode) {
        ResponseMetadata responseMetadata = new ResponseMetadata();
        responseMetadata.setQueryTime(System.currentTimeMillis() - startTime);
        responseMetadata.setMessage(message);

        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setMetadata(responseMetadata);
        apiResponse.setData(data);

        return new ResponseEntity<ApiResponse>(apiResponse, statusCode);
    }
}
