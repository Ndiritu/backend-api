package com.example.examMgt.model.hf;

public class ItemModeration {

    public enum ItemModFunction {
        CREATE,
        UPDATE,
        GET_BY_ITEM,
        GET_BY_EXT_EXAMINER,
        GET_HISTORY_BY_ITEM,
        GET_ALL
    }

    public enum ItemType {
        EXAM, QUESTION, INSTRUCTION, SECTION
    }

    private String itemType;
    private String itemId;
    private long extExaminerId;
    private boolean status;
    private long createDate;
    private long lastUpdate;
    private String docType;

    public ItemModeration() {}

    public ItemModeration(String itemType, String itemId, long extExaminerId, boolean status, long createDate, long lastUpdate, String docType) {
        this.itemType = itemType;
        this.itemId = itemId;
        this.extExaminerId = extExaminerId;
        this.status = status;
        this.createDate = createDate;
        this.lastUpdate = lastUpdate;
        this.docType = docType;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType.toString();
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public long getExtExaminerId() {
        return extExaminerId;
    }

    public void setExtExaminerId(long extExaminerId) {
        this.extExaminerId = extExaminerId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public long getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    @Override
    public String toString() {
        return "ItemModeration {" +
                "itemType=" + itemType + ", " +
                "itemId=" + itemId + ", " +
                "extExaminerId=" + extExaminerId + ", " +
                "status=" + status + ", " +
                "createDate=" + createDate + ", " +
                "lastUpdate=" + lastUpdate + ", " +
                "docType=" + docType +
                "}";
    }
}
