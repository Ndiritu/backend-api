package com.example.examMgt.model.hf;

import java.sql.Timestamp;

public class Section extends ExamItem {

    public enum SectionFunction {
        CREATE,
        UPDATE,
        GET_BY_EXAM_ID,
        GET_BY_ID,
        GET_ALL,
        DELETE,
        GET_HISTORY
    }

    private String name;

    public Section(String id, String examId, int displayIndex, long createDate, long lastUpdate, boolean deleted, long deleteDate, String docType, String name) {
        super(id, examId, displayIndex, createDate, lastUpdate, deleted, deleteDate, docType);
        this.name = name;
    }

    public Section() {
        super();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Section {" +
                "id=" + id + ", " +
                "examId=" + examId + ", " +
                "displayIndex=" + displayIndex + ", " +
                "name=" + name + ", " +
                "createDate=" + new Timestamp(createDate).toString() + ", " +
                "lastUpdate=" + new Timestamp(lastUpdate).toString() + ", " +
                "deleted=" + deleted + ", " +
                "deleteDate=" + new Timestamp(deleteDate).toString() + ", " +
                "docType=" + docType +
                "}";
    }
}
