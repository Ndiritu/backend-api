package com.example.examMgt.model.hf;

import java.sql.Timestamp;

public class Question extends ExamItem {

    public enum QuestionType {
        MAIN_QUESTION, SUB_QUESTION
    }

    public enum QuestionFunction {
        CREATE,
        UPDATE,
        GET_BY_ID,
        GET_BY_EXAM_ID,
        GET_ALL,
        GET_HISTORY,
        DELETE
    }

    private String type;
    private String text;
    private int marks;

    public Question(String id, String examId, int displayIndex, long createDate, long lastUpdate, boolean deleted, long deleteDate, String docType, String type, String text, int marks) {
        super(id, examId, displayIndex, createDate, lastUpdate, deleted, deleteDate, docType);
        this.type = type;
        this.text = text;
        this.marks = marks;
    }

    public Question() {
        super();
    }

    public String getType() {
        return type;
    }

    public void setType(QuestionType type) {
        this.type = String.valueOf(type);
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getMarks() {
        return marks;
    }

    public void setMarks(int marks) {
        this.marks = marks;
    }

    @Override
    public String toString() {
        return "Question {" +
                "id=" + id + ", " +
                "examId=" + examId + ", " +
                "displayIndex=" + displayIndex + ", " +
                "type=" + type + ", " +
                "text=" + text + ", " +
                "marks=" + marks + ", " +
                "createDate=" + new Timestamp(createDate).toString() + ", " +
                "lastUpdate=" + new Timestamp(lastUpdate).toString() + ", " +
                "deleted=" + deleted + ", " +
                "deleteDate=" + new Timestamp(deleteDate).toString() + ", " +
                "docType=" + docType +
                "}";
    }
}
