package com.example.examMgt.model.hf;


import java.sql.Timestamp;

public class Examination {

    public enum ExaminationType {
        ORDINARY, SUPPLEMENTARY, SPECIAL
    }

    public enum ExaminationFunction {
        CREATE,
        GET_ALL,
        GET_BY_COURSE,
        GET_BY_LECTURER,
        GET_BY_ID,
        UPDATE,
        GET_HISTORY,
        DELETE
    }

    public enum NumberingFormat {
        NUMERICAL, ALPHABETICAL, ROMAN_NUMERALS
    }

    private String id;
    private String name;
    private String examinationType;
    private long courseId;
    private long createdByLecturerId;
    private long universityId;
    private long examDate;
    private long startTime;
    private long duration;
    private String primaryNumberingFormat;
    private String secondaryNumberingFormat;
    private long createDate;
    private long lastUpdate;
    private boolean deleted;
    private long deleteDate;
    private String docType;

    public Examination(String id, String name, String examinationType, long courseId, long createdByLecturerId, long universityId, long examDate, long startTime, long duration, String primaryNumberingFormat, String secondaryNumberingFormat, long createDate, long lastUpdate, boolean deleted, long deleteDate, String docType) {
        this.id = id;
        this.name = name;
        this.examinationType = examinationType;
        this.courseId = courseId;
        this.createdByLecturerId = createdByLecturerId;
        this.universityId = universityId;
        this.examDate = examDate;
        this.startTime = startTime;
        this.duration = duration;
        this.primaryNumberingFormat = primaryNumberingFormat;
        this.secondaryNumberingFormat = secondaryNumberingFormat;
        this.createDate = createDate;
        this.lastUpdate = lastUpdate;
        this.deleted = deleted;
        this.deleteDate = deleteDate;
        this.docType = docType;
    }

    public Examination() {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExaminationType() {
        return examinationType;
    }

    public void setExaminationType(ExaminationType examinationType) {
        this.examinationType = String.valueOf(examinationType);
    }

    public long getCourseId() {
        return courseId;
    }

    public void setCourseId(long courseId) {
        this.courseId = courseId;
    }

    public long getCreatedByLecturerId() {
        return createdByLecturerId;
    }

    public void setCreatedByLecturerId(long createdByLecturerId) {
        this.createdByLecturerId = createdByLecturerId;
    }


    public long getUniversityId() {
        return universityId;
    }

    public void setUniversityId(long universityId) {
        this.universityId = universityId;
    }

    public long getExamDate() {
        return examDate;
    }

    public void setExamDate(long examDate) {
        this.examDate = examDate;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public String getPrimaryNumberingFormat() {
        return primaryNumberingFormat;
    }

    public void setPrimaryNumberingFormat(String primaryNumberingFormat) {
        this.primaryNumberingFormat = primaryNumberingFormat;
    }

    public String getSecondaryNumberingFormat() {
        return secondaryNumberingFormat;
    }

    public void setSecondaryNumberingFormat(String secondaryNumberingFormat) {
        this.secondaryNumberingFormat = secondaryNumberingFormat;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public long getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public long getDeleteDate() {
        return deleteDate;
    }

    public void setDeleteDate(long deleteDate) {
        this.deleteDate = deleteDate;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    @Override
    public String toString() {
        return "Examination{" +
                "id=" + id + ", " +
                "name=" + name + ", " +
                "examinationType=" + examinationType + ", " +
                "courseId=" + courseId + ", " +
                "createdByLecturerId=" + createdByLecturerId + ", " +
                "universityId=" + universityId + ", " +
                "examDate=" + new Timestamp(examDate).toString() + ", " +
                "startTime=" + new Timestamp(startTime).toString() + ", " +
                "duration=" + duration + ", " +
                "primaryNumberFormat=" + primaryNumberingFormat + ", " +
                "secondaryNumberFormat=" + secondaryNumberingFormat + ", " +
                "createDate=" + createDate + ", " +
                "lastUpdate=" + lastUpdate + ", " +
                "deleted=" + deleted + ", " +
                "deleteDate=" + deleteDate + ", " +
                "docType=" + docType +
                "}";
    }

}
