package com.example.examMgt.model.hf;

import java.sql.Timestamp;

public class ExamGeneration {

    public enum ExamGenerationFn {
        CREATE,
        UPDATE,
        GET_BY_EXAM_ID,
        GET_BY_EXAM_CENTRE_STAFF_ID,
        GET_ALL
    }

    private String examId;
    private long examCentreStaffId;
    private boolean examCentreStatus;
    private int examCentreNumCopies;
    private long lecturerId;
    private boolean lecturerStatus;
    private int lecturerNumCopies;
    private long createDate;
    private long lastUpdate;
    private String docType;

    public ExamGeneration(String examId, long examCentreStaffId, boolean examCentreStatus, int examCentreNumCopies, long lecturerId, boolean lecturerStatus, int lecturerNumCopies, long createDate, long lastUpdate, String docType) {
        this.examId = examId;
        this.examCentreStaffId = examCentreStaffId;
        this.examCentreStatus = examCentreStatus;
        this.examCentreNumCopies = examCentreNumCopies;
        this.lecturerId = lecturerId;
        this.lecturerStatus = lecturerStatus;
        this.lecturerNumCopies = lecturerNumCopies;
        this.createDate = createDate;
        this.lastUpdate = lastUpdate;
        this.docType = docType;
    }

    public ExamGeneration() {}

    public String getExamId() {
        return examId;
    }

    public void setExamId(String examId) {
        this.examId = examId;
    }

    public long getExamCentreStaffId() {
        return examCentreStaffId;
    }

    public void setExamCentreStaffId(long examCentreStaffId) {
        this.examCentreStaffId = examCentreStaffId;
    }

    public boolean isExamCentreStatus() {
        return examCentreStatus;
    }

    public void setExamCentreStatus(boolean examCentreStatus) {
        this.examCentreStatus = examCentreStatus;
    }

    public int getExamCentreNumCopies() {
        return examCentreNumCopies;
    }

    public void setExamCentreNumCopies(int examCentreNumCopies) {
        this.examCentreNumCopies = examCentreNumCopies;
    }

    public long getLecturerId() {
        return lecturerId;
    }

    public void setLecturerId(long lecturerId) {
        this.lecturerId = lecturerId;
    }

    public boolean isLecturerStatus() {
        return lecturerStatus;
    }

    public void setLecturerStatus(boolean lecturerStatus) {
        this.lecturerStatus = lecturerStatus;
    }

    public int getLecturerNumCopies() {
        return lecturerNumCopies;
    }

    public void setLecturerNumCopies(int lecturerNumCopies) {
        this.lecturerNumCopies = lecturerNumCopies;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public long getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    @Override
    public String toString() {
        return "ExamGeneration {" +
                "examId" + examId + ", " +
                "examCentreStaffId=" + examCentreStaffId + ", " +
                "examCentreStatus=" + examCentreStatus + ", " +
                "examCentreNumCopies=" + examCentreNumCopies + ", " +
                "lecturerId=" + lecturerId + ", " +
                "lecturerStatus=" + lecturerStatus + ", " +
                "lecturerNumCopies=" + lecturerNumCopies + ", " +
                "createDate=" + new Timestamp(createDate) + ", " +
                "lastUpdate=" + new Timestamp(lastUpdate) + ", " +
                "docType=" + docType +
                "}";
    }


}
