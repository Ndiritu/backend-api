package com.example.examMgt.model.hf.history;

import com.example.examMgt.model.hf.ItemModeration;

public class ItemModerationHistory {

    private String txId;
    private ItemModeration value;
    private long timestamp;
    private boolean isDelete;

    public ItemModerationHistory() {}

    public ItemModerationHistory(String txId, ItemModeration value, long timestamp, boolean isDelete) {
        this.txId = txId;
        this.value = value;
        this.timestamp = timestamp;
        this.isDelete = isDelete;
    }

    public String getTxId() {
        return txId;
    }

    public void setTxId(String txId) {
        this.txId = txId;
    }

    public ItemModeration getValue() {
        return value;
    }

    public void setValue(ItemModeration value) {
        this.value = value;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean delete) {
        isDelete = delete;
    }

    @Override
    public String toString() {
        return "ItemModerationHistory {" +
                "txId=" + txId + ", " +
                "value=" + value + ", " +
                "timestamp=" + timestamp + ", " +
                "isDelete=" + isDelete +
                "}";
    }
}
