package com.example.examMgt.model.hf;

public class ExamApproval {

    public enum ExamApprovalFn {
        CREATE,
        UPDATE,
        GET_EXAMS_BY_STATUS,
        GET_BY_EXAM_ID,
        GET_HISTORY_BY_EXAM_ID,
        GET_ALL
    }

    private String examId;
    private long chairpersonId;
    private boolean status;
    private long createDate;
    private long lastUpdate;
    private String docType;

    public ExamApproval(String examId, long chairpersonId, boolean status, long createDate, long lastUpdate, String docType) {
        this.examId = examId;
        this.chairpersonId = chairpersonId;
        this.status = status;
        this.createDate = createDate;
        this.lastUpdate = lastUpdate;
        this.docType = docType;
    }

    public ExamApproval() {}

    public String getExamId() {
        return examId;
    }

    public void setExamId(String examId) {
        this.examId = examId;
    }

    public long getChairpersonId() {
        return chairpersonId;
    }

    public void setChairpersonId(long chairpersonId) {
        this.chairpersonId = chairpersonId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public long getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    @Override
    public String toString() {
        return "ExamApproval {" +
                "examId=" + examId + ", " +
                "chairpersonId=" + chairpersonId + ", " +
                "status=" + status + ", " +
                "createDate=" + createDate + ", " +
                "lastUpdate=" + lastUpdate + ", " +
                "docType=" + docType +
                "}";
    }

}
