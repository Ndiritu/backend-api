package com.example.examMgt.model.hf;

public class ExamItem {

    protected String id;
    protected String examId;
    protected int displayIndex;
    protected long createDate;
    protected long lastUpdate;
    protected boolean deleted;
    protected long deleteDate;
    protected String docType;

    protected ExamItem(String id, String examId, int displayIndex, long createDate, long lastUpdate, boolean deleted, long deleteDate, String docType) {
        this.id = id;
        this.examId = examId;
        this.displayIndex = displayIndex;
        this.createDate = createDate;
        this.lastUpdate = lastUpdate;
        this.deleted = deleted;
        this.deleteDate = deleteDate;
        this.docType = docType;
    }

    protected ExamItem() {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getExamId() {
        return examId;
    }

    public void setExamId(String examId) {
        this.examId = examId;
    }

    public int getDisplayIndex() {
        return displayIndex;
    }

    public void setDisplayIndex(int displayIndex) {
        this.displayIndex = displayIndex;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public long getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public long getDeleteDate() {
        return deleteDate;
    }

    public void setDeleteDate(long deleteDate) {
        this.deleteDate = deleteDate;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }
}
