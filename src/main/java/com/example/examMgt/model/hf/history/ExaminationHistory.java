package com.example.examMgt.model.hf.history;

import com.example.examMgt.model.hf.Examination;

public class ExaminationHistory {

    private String txId;
    private Examination value;
    private long timestamp;
    private boolean isDelete;

    public ExaminationHistory() {}

    public ExaminationHistory(String txId, Examination value, long timestamp, boolean isDelete) {
        this.txId = txId;
        this.value = value;
        this.timestamp = timestamp;
        this.isDelete = isDelete;
    }

    public String getTxId() {
        return txId;
    }

    public void setTxId(String txId) {
        this.txId = txId;
    }

    public Examination getValue() {
        return value;
    }

    public void setValue(Examination value) {
        this.value = value;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean delete) {
        isDelete = delete;
    }

    @Override
    public String toString() {
        return "ExaminationHistory {" +
                "txId=" + txId + ", " +
                "value=" + value + ", " +
                "timestamp=" + timestamp + ", " +
                "isDelete=" + isDelete +
                "}";
    }
}
