package com.example.examMgt.model.hf.history;

import com.example.examMgt.model.hf.ExamApproval;

public class ExamApprovalHistory {

    private String txId;
    private ExamApproval value;
    private long timestamp;
    private boolean isDelete;

    public ExamApprovalHistory() {}

    public ExamApprovalHistory(String txId, ExamApproval value, long timestamp, boolean isDelete) {
        this.txId = txId;
        this.value = value;
        this.timestamp = timestamp;
        this.isDelete = isDelete;
    }

    public String getTxId() {
        return txId;
    }

    public void setTxId(String txId) {
        this.txId = txId;
    }

    public ExamApproval getValue() {
        return value;
    }

    public void setValue(ExamApproval value) {
        this.value = value;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean delete) {
        isDelete = delete;
    }

    @Override
    public String toString() {
        return "ExamApprovalHistory {" +
                "txId=" + txId + ", " +
                "value=" + value + ", " +
                "timestamp=" + timestamp + ", " +
                "isDelete=" + isDelete +
                "}";
    }
}
