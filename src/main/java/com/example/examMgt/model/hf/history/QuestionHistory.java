package com.example.examMgt.model.hf.history;

import com.example.examMgt.model.hf.Question;

public class QuestionHistory {

    private String txId;
    private Question value;
    private long timestamp;
    private boolean isDelete;

    public QuestionHistory() {}

    public QuestionHistory(String txId, Question value, long timestamp, boolean isDelete) {
        this.txId = txId;
        this.value = value;
        this.timestamp = timestamp;
        this.isDelete = isDelete;
    }

    public String getTxId() {
        return txId;
    }

    public void setTxId(String txId) {
        this.txId = txId;
    }

    public Question getValue() {
        return value;
    }

    public void setValue(Question value) {
        this.value = value;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean delete) {
        isDelete = delete;
    }

    @Override
    public String toString() {
        return "QuestionHistory {" +
                "txId=" + txId + ", " +
                "value=" + value + ", " +
                "timestamp=" + timestamp + ", " +
                "isDelete=" + isDelete +
                "}";
    }

}
