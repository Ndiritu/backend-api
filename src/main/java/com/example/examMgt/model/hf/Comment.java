package com.example.examMgt.model.hf;

public class Comment {

    public enum CommentFunction {
        CREATE,
        GET_BY_ITEM,
        GET_BY_EXAM,
        GET_BY_SENDER,
        GET_BY_RECIPIENT,
        DELETE,
        GET_ALL,
        GET_HISTORY,
        GET_BY_ID
    }

    public enum SenderType {
        LECTURER, EXT_EXAMINER, CHAIRPERSON, SYS_ADMIN
    }

    public enum RecipientType {
        LECTURER, EXT_EXAMINER, CHAIRPERSON, SYS_ADMIN
    }


    private String id;
    private String text;
    private String senderType;
    private long senderId;
    private String recipientType;
    private long recipientId;
    private String itemType;
    private String itemId;
    private String examId;
    private long createDate;
    private long lastUpdate;
    private boolean deleted;
    private long deleteDate;

    public Comment() {}

    public Comment(String id, String text, String senderType, long senderId, String recipientType, long recipientId, String itemType, String itemId, String examId, long createDate, long lastUpdate, boolean deleted, long deleteDate) {
        this.id = id;
        this.text = text;
        this.senderType = senderType;
        this.senderId = senderId;
        this.recipientType = recipientType;
        this.recipientId = recipientId;
        this.itemType = itemType;
        this.itemId = itemId;
        this.examId = examId;
        this.createDate = createDate;
        this.lastUpdate = lastUpdate;
        this.deleted = deleted;
        this.deleteDate = deleteDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getSenderType() {
        return senderType;
    }

    public void setSenderType(SenderType senderType) {
        this.senderType = String.valueOf(senderType);
    }

    public long getSenderId() {
        return senderId;
    }

    public void setSenderId(long senderId) {
        this.senderId = senderId;
    }

    public String getRecipientType() {
        return recipientType;
    }

    public void setRecipientType(RecipientType recipientType) {
        this.recipientType = String.valueOf(recipientType);
    }

    public long getRecipientId() {
        return recipientId;
    }

    public void setRecipientId(long recipientId) {
        this.recipientId = recipientId;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(ItemModeration.ItemType itemType) {
        this.itemType = String.valueOf(itemType);
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getExamId() {
        return examId;
    }

    public void setExamId(String examId) {
        this.examId = examId;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public long getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public long getDeleteDate() {
        return deleteDate;
    }

    public void setDeleteDate(long deleteDate) {
        this.deleteDate = deleteDate;
    }

    @Override
    public String toString() {
        return "Comment {" +
                "id=" + id + ", " +
                "text=" + text + ", " +
                "senderType=" + senderType + ", " +
                "senderId=" + senderId + ", " +
                "recipientType=" + recipientType + ", " +
                "recipientId=" + recipientId + ", " +
                "itemType=" + itemType + ", " +
                "itemId=" + itemId + ", " +
                "examId=" + examId + ", " +
                "createDate=" + createDate + ", " +
                "lastUpdate=" + lastUpdate + ", " +
                "deleted=" + deleted + ", " +
                "deleteDate=" + deleteDate +
                "}";
    }

}
