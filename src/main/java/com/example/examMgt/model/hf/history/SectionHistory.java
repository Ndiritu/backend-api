package com.example.examMgt.model.hf.history;

import com.example.examMgt.model.hf.Section;

public class SectionHistory {

    private String txId;
    private Section value;
    private long timestamp;
    private boolean isDelete;

    public SectionHistory() {}

    public SectionHistory(String txId, Section value, long timestamp, boolean isDelete) {
        this.txId = txId;
        this.value = value;
        this.timestamp = timestamp;
        this.isDelete = isDelete;
    }

    public String getTxId() {
        return txId;
    }

    public void setTxId(String txId) {
        this.txId = txId;
    }

    public Section getValue() {
        return value;
    }

    public void setValue(Section value) {
        this.value = value;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean delete) {
        isDelete = delete;
    }

    @Override
    public String toString() {
        return "SectionHistory {" +
                "txId=" + txId + ", " +
                "value=" + value + ", " +
                "timestamp=" + timestamp + ", " +
                "isDelete=" + isDelete +
                "}";
    }
}
