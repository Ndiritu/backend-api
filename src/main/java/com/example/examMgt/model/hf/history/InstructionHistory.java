package com.example.examMgt.model.hf.history;

import com.example.examMgt.model.hf.Instruction;

public class InstructionHistory {

    private String txId;
    private Instruction value;
    private long timestamp;
    private boolean isDelete;

    public InstructionHistory() {}

    public InstructionHistory(String txId, Instruction value, long timestamp, boolean isDelete) {
        this.txId = txId;
        this.value = value;
        this.timestamp = timestamp;
        this.isDelete = isDelete;
    }

    public String getTxId() {
        return txId;
    }

    public void setTxId(String txId) {
        this.txId = txId;
    }

    public Instruction getValue() {
        return value;
    }

    public void setValue(Instruction value) {
        this.value = value;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean delete) {
        isDelete = delete;
    }

    @Override
    public String toString() {
        return "InstructionHistory {" +
                "txId=" + txId + ", " +
                "value=" + value + ", " +
                "timestamp=" + timestamp + ", " +
                "isDelete=" + isDelete +
                "}";
    }

}
