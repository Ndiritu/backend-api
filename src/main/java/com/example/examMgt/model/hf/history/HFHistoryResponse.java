package com.example.examMgt.model.hf.history;

public class HFHistoryResponse {

    private String txId;
    private String value;
    private long timestamp;
    private boolean isDelete;

    public HFHistoryResponse() {}

    public String getTxId() {
        return txId;
    }

    public void setTxId(String txId) {
        this.txId = txId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean delete) {
        isDelete = delete;
    }

    @Override
    public String toString() {
        return "HFHistoryResponse {" +
                "txId=" + txId + ", " +
                "value=" + value + ", " +
                "timestamp=" + timestamp + ", " +
                "isDelete=" + isDelete +
                "}";
    }

}
