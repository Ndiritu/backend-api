package com.example.examMgt.model.hf;

import java.sql.Timestamp;

public class Instruction extends ExamItem {

    public enum InstructionFunction {
        CREATE,
        UPDATE,
        DELETE,
        GET_BY_EXAM_ID,
        GET_BY_ID,
        GET_HISTORY_BY_ID
    }

    public enum InstructionType {
        EXAM_LEVEL, SECTION_LEVEL, QUESTION_LEVEL
    }


    private String type;
    private String text;

    public Instruction() {
        super();
    }

    public Instruction(String id, String examId, int displayIndex, long createDate, long lastUpdate, boolean deleted, long deleteDate, String docType, String type, String text) {
        super(id, examId, displayIndex, createDate, lastUpdate, deleted, deleteDate, docType);
        this.type = type;
        this.text = text;
    }

    public String getType() {
        return type;
    }

    public void setType(InstructionType type) {
        this.type = String.valueOf(type);
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "Instruction {" +
                "id=" + id + ", " +
                "examId=" + examId + ", " +
                "displayIndex=" + displayIndex + ", " +
                "type=" + type + ", " +
                "text=" + text + ", " +
                "createDate=" + new Timestamp(createDate).toString() + ", " +
                "lastUpdate=" + new Timestamp(lastUpdate).toString() + ", " +
                "deleted=" + deleted + ", " +
                "deleteDate=" + deleteDate + ", " +
                "docType=" + docType +
                "}";
    }

}
