package com.example.examMgt.model.db;

import com.example.examMgt.model.Staff;

import java.sql.Timestamp;

public class SysAdmin extends Staff {

    public SysAdmin(long id, String firstName, String otherNames, String surname, String email, String password, boolean active, Timestamp createDate, Timestamp lastUpdate) {
        super(id, firstName, otherNames, surname, email, password, active, createDate, lastUpdate);
    }

    public SysAdmin() {
        super();
    }

    @Override
    public String toString() {
        return "SysAdmin {" +
                "id=" + super.getId() + ", " +
                "firstName=" + super.getFirstName() + ", " +
                "otherNames=" + super.getOtherNames() + ", " +
                "surname=" + super.getSurname() + ", " +
                "email=" + super.getEmail() + ", " +
                "password=" + super.getPassword() + ", " +
                "active=" + super.isActive() + ", " +
                "createDate=" + super.getCreateDate() + ", " +
                "lastUpdate=" + super.getLastUpdate() +
                "}";
    }
}
