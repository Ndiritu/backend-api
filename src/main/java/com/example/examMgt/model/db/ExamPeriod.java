package com.example.examMgt.model.db;

import java.sql.Timestamp;

public class ExamPeriod {

    private long id;
    private String name;
    private Timestamp startDate;
    private Timestamp createDate;
    private Timestamp lastUpdate;

    public ExamPeriod() {}

    public ExamPeriod(long id, String name, Timestamp startDate, Timestamp createDate, Timestamp lastUpdate) {
        this.id = id;
        this.name = name;
        this.startDate = startDate;
        this.createDate = createDate;
        this.lastUpdate = lastUpdate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timestamp getStartDate() {
        return startDate;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Override
    public String toString() {
        return "ExamPeriod {" +
                "id=" + id + ", " +
                "name=" + name + ", " +
                "startDate=" + startDate + ", " +
                "createDate=" + createDate + ", " +
                "lastUpdate=" + lastUpdate +
                "}";
    }

}
