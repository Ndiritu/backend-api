package com.example.examMgt.model.db;

public class University {

    private long id;
    private String name;
    private String logo;

    public University(long id, String name, String logo) {
        this.id = id;
        this.name = name;
        this.logo = logo;
    }

    public University() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    @Override
    public String toString() {
        return "University {" +
                "id=" + id + ", " +
                "name=" + name + ", " +
                "logo=" + logo +
                "}";
    }
}
