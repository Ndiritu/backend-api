package com.example.examMgt.model.db;

import com.example.examMgt.model.UserSession;

import java.sql.Timestamp;

public class LoginSession {

    private long id;
    private long userId;
    private UserSession.UserType userType;
    private String twoFACode;
    private Timestamp sessionExpiry;

    public LoginSession(long id, long userId, UserSession.UserType userType, String twoFACode, Timestamp sessionExpiry) {
        this.id = id;
        this.userId = userId;
        this.userType = userType;
        this.twoFACode = twoFACode;
        this.sessionExpiry = sessionExpiry;
    }

    public LoginSession() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public UserSession.UserType getUserType() {
        return userType;
    }

    public void setUserType(UserSession.UserType userType) {
        this.userType = userType;
    }

    public String getTwoFACode() {
        return twoFACode;
    }

    public void setTwoFACode(String twoFACode) {
        this.twoFACode = twoFACode;
    }

    public Timestamp getSessionExpiry() {
        return sessionExpiry;
    }

    public void setSessionExpiry(Timestamp sessionExpiry) {
        this.sessionExpiry = sessionExpiry;
    }

    @Override
    public String toString() {
        return "LoginSession {" +
                "id=" + id + ", " +
                "userId=" + userId + ", " +
                "userType=" + userType + ", " +
                "twoFACode=" + twoFACode + ", " +
                "sessionExpiry=" + sessionExpiry +
                "}";
    }
}
