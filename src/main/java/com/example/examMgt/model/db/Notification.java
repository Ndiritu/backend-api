package com.example.examMgt.model.db;

import com.example.examMgt.model.UserSession;

import java.sql.Timestamp;

public class Notification {

    public enum NotificationType {
        READY_FOR_MODERATION,
        EXAM_MODERATED,
        NEW_COMMENT,
        ITEM_EDITED,
        ITEM_ADDED,
        READY_FOR_PRINT,
        READY_FOR_COLLECT
    }

    private long id;
    private NotificationType type;
    private long senderId;
    private UserSession.UserType senderType;
    private long recipientId;
    private UserSession.UserType recipientType;
    private boolean seen;
    private String targetType;
    private String targetId;
    private Timestamp createDate;
    private Timestamp lastUpdate;

    public Notification() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public NotificationType getType() {
        return type;
    }

    public void setType(NotificationType type) {
        this.type = type;
    }

    public long getSenderId() {
        return senderId;
    }

    public void setSenderId(long senderId) {
        this.senderId = senderId;
    }

    public UserSession.UserType getSenderType() {
        return senderType;
    }

    public void setSenderType(UserSession.UserType senderType) {
        this.senderType = senderType;
    }

    public long getRecipientId() {
        return recipientId;
    }

    public void setRecipientId(long recipientId) {
        this.recipientId = recipientId;
    }

    public UserSession.UserType getRecipientType() {
        return recipientType;
    }

    public void setRecipientType(UserSession.UserType recipientType) {
        this.recipientType = recipientType;
    }

    public boolean isSeen() {
        return seen;
    }

    public void setSeen(boolean seen) {
        this.seen = seen;
    }

    public String getTargetType() {
        return targetType;
    }

    public void setTargetType(String targetType) {
        this.targetType = targetType;
    }

    public String getTargetId() {
        return targetId;
    }

    public void setTargetId(String targetId) {
        this.targetId = targetId;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Override
    public String toString() {
        return "Notification {" +
                "id=" + id + ", " +
                "type=" + type + ", " +
                "senderId=" + senderId + ", " +
                "senderType=" + senderType + ", " +
                "recipientId=" + recipientId + ", " +
                "recipientType=" + recipientType + ", " +
                "seen=" + seen + ", " +
                "targetType=" + targetType + ", " +
                "targetId=" + targetId + ", " +
                "createDate=" + createDate + ", " +
                "lastUpdate=" + lastUpdate +
                "}";
    }
}
