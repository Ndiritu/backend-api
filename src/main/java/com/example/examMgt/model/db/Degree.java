package com.example.examMgt.model.db;

public class Degree {

    public enum DegreeType {
        BACHELOR_OF_SCIENCE
    }

    public enum DegreeLevel {
        UNDERGRADUATE, POSTGRADUATE
    }

    private long id;
    private String name;
    private DegreeType degreeType;
    private String degreeTypePrefix;
    private DegreeLevel degreeLevel;

    public Degree(long id, String name, DegreeType degreeType, String degreeTypePrefix, DegreeLevel degreeLevel) {
        this.id = id;
        this.name = name;
        this.degreeType = degreeType;
        this.degreeTypePrefix = degreeTypePrefix;
        this.degreeLevel = degreeLevel;
    }

    public Degree() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DegreeType getDegreeType() {
        return degreeType;
    }

    public void setDegreeType(DegreeType degreeType) {
        this.degreeType = degreeType;
    }

    public String getDegreeTypePrefix() {
        return degreeTypePrefix;
    }

    public void setDegreeTypePrefix(String degreeTypePrefix) {
        this.degreeTypePrefix = degreeTypePrefix;
    }

    public DegreeLevel getDegreeLevel() {
        return degreeLevel;
    }

    public void setDegreeLevel(DegreeLevel degreeLevel) {
        this.degreeLevel = degreeLevel;
    }

    @Override
    public String toString() {
        return "Degree {" +
                "id=" + id + ", " +
                "name=" + name + ", " +
                "degreeType=" + degreeType + ", " +
                "degreeTypePrefix=" + degreeTypePrefix + ", " +
                "degreeLevel=" + degreeLevel +
                "}";
    }



}
