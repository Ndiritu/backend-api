package com.example.examMgt.model.db;

import com.example.examMgt.model.Staff;

import java.sql.Timestamp;

public class ExamCentreStaff extends Staff {

    public ExamCentreStaff(long id, String firstName, String otherNames, String surname, String email, String password, boolean active, Timestamp createDate, Timestamp lastUpdate) {
        super(id, firstName, otherNames, surname, email, password, active, createDate, lastUpdate);
    }

    public ExamCentreStaff() {
        super();
    }

    @Override
    public String toString() {
        return "ExamCentreStaff{" +
                "id=" + super.getId() + ", " +
                "firstName=" + super.getFirstName() + ", " +
                "otherNames=" + super.getOtherNames() + ", " +
                "surname=" + super.getSurname() + ", " +
                "email=" + super.getEmail() + ", " +
                "password=" + super.getPassword() + ", " +
                "active=" + super.isActive() + ", " +
                "createDate=" + super.getCreateDate() + ", " +
                "lastUpdate=" + super.getLastUpdate() +
                "}";
    }

}
