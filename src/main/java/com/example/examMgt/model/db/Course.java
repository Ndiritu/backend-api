package com.example.examMgt.model.db;

public class Course {

    public enum CourseCode {
        CSC_411, CSC_412, CSC_413, CSC_414, CSC_416, CSC_422,
        CSC_121, CSC_225, CSC_212, CSC_122, CSC_322, CSC_452, CSC_321,
        CSC_224
    }

    public enum Semester {
        FIRST_SEMESTER, SECOND_SEMESTER
    }

    private long id;
    private String name;
    private CourseCode courseCode;
    private int yearOfStudy;
    private Semester semester;
    private long degreeId;

    public Course(long id, String name, CourseCode courseCode, int yearOfStudy, Semester semester, long degreeId) {
        this.id = id;
        this.name = name;
        this.courseCode = courseCode;
        this.yearOfStudy = yearOfStudy;
        this.semester = semester;
        this.degreeId = degreeId;
    }

    public Course() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CourseCode getCourseCode() {
        return courseCode;
    }

    public void setCourseCode(CourseCode courseCode) {
        this.courseCode = courseCode;
    }

    public int getYearOfStudy() {
        return yearOfStudy;
    }

    public void setYearOfStudy(int yearOfStudy) {
        this.yearOfStudy = yearOfStudy;
    }

    public Semester getSemester() {
        return semester;
    }

    public void setSemester(Semester semester) {
        this.semester = semester;
    }

    public long getDegreeId() {
        return degreeId;
    }

    public void setDegreeId(long degreeId) {
        this.degreeId = degreeId;
    }

    @Override
    public String toString() {
        return "Course{" +
                "id=" + id + ", " +
                "name=" + name + ", " +
                "courseCode=" + courseCode + ", " +
                "yearOfStudy=" + yearOfStudy + ", " +
                "semester=" + semester + ", " +
                "degreeId=" + degreeId +
                "}";
    }
}
