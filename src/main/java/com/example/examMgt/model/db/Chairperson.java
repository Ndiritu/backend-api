package com.example.examMgt.model.db;

import java.sql.Timestamp;

public class Chairperson {

    private long id;
    private long lecturerId;
    private Timestamp startDate;
    private Timestamp endDate;

    public Chairperson() {}

    public Chairperson(long id, long lecturerId, Timestamp startDate, Timestamp endDate) {
        this.id = id;
        this.lecturerId = lecturerId;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getLecturerId() {
        return lecturerId;
    }

    public void setLecturerId(long lecturerId) {
        this.lecturerId = lecturerId;
    }

    public Timestamp getStartDate() {
        return startDate;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    public Timestamp getEndDate() {
        return endDate;
    }

    public void setEndDate(Timestamp endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "Chairperson{" +
                "id=" + id + ", " +
                "lecturerId=" + lecturerId + ", " +
                "startDate=" + startDate + ", " +
                "endDate=" + endDate + ", " +
                "}";
    }

}
