package com.example.examMgt.model.db;

import java.sql.Timestamp;

public class Student {

    private long id;
    private String firstName;
    private String otherNames;
    private String surname;
    private String registrationNumber;
    private long natlIdNumber;
    private String email;
    private String password;
    private int yearOfStudy;
    private Timestamp createDate;
    private Timestamp lastUpdate;

    public Student(long id, String firstName, String otherNames, String surname, String registrationNumber, long natlIdNumber, String email, String password, int yearOfStudy, Timestamp createDate, Timestamp lastUpdate) {
        this.id = id;
        this.firstName = firstName;
        this.otherNames = otherNames;
        this.surname = surname;
        this.registrationNumber = registrationNumber;
        this.natlIdNumber = natlIdNumber;
        this.email = email;
        this.password = password;
        this.yearOfStudy = yearOfStudy;
        this.createDate = createDate;
        this.lastUpdate = lastUpdate;
    }

    public Student() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getOtherNames() {
        return otherNames;
    }

    public void setOtherNames(String otherNames) {
        this.otherNames = otherNames;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public int getYearOfStudy() {
        return yearOfStudy;
    }

    public void setYearOfStudy(int yearOfStudy) {
        this.yearOfStudy = yearOfStudy;
    }

    public long getNatlIdNumber() {
        return natlIdNumber;
    }

    public void setNatlIdNumber(long natlIdNumber) {
        this.natlIdNumber = natlIdNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id + ", " +
                "firstName=" + firstName + ", " +
                "otherNames=" + otherNames + ", " +
                "surname=" + surname + ", " +
                "registrationNumber=" + registrationNumber + ", " +
                "natlIdNumber=" + natlIdNumber + ", " +
                "email=" + email + ", " +
                "password=" + password + ", " +
                "yearOfStudy=" + yearOfStudy + ", " +
                "createDate=" + createDate + ", " +
                "lastUpdate=" + lastUpdate +
                "}";
    }
}
