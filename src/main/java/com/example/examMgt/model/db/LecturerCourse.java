package com.example.examMgt.model.db;

import java.sql.Timestamp;

public class LecturerCourse {

    private long lecturerId;
    private long courseId;
    private Timestamp createDate;

    public LecturerCourse() {}

    public LecturerCourse(long lecturerId, long courseId, Timestamp createDate) {
        this.lecturerId = lecturerId;
        this.courseId = courseId;
        this.createDate = createDate;
    }

    public long getLecturerId() {
        return lecturerId;
    }

    public void setLecturerId(long lecturerId) {
        this.lecturerId = lecturerId;
    }

    public long getCourseId() {
        return courseId;
    }

    public void setCourseId(long courseId) {
        this.courseId = courseId;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    @Override
    public String toString() {
        return "LecturerCourse{" +
                "lecturerId=" + lecturerId + ", " +
                "courseId=" + courseId + ", " +
                "createDate=" + createDate +
                "}";
    }
}
