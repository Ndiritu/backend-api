package com.example.examMgt.model.db;

import java.sql.Timestamp;

public class ExternalExaminerCourse {

    private long id;
    private long extExaminerId;
    private long courseId;
    private Timestamp startDate;
    private Timestamp endDate;
    private Timestamp createDate;
    private Timestamp lastUpdate;

    public ExternalExaminerCourse() {}

    public ExternalExaminerCourse(long id, long extExaminerId, long courseId, Timestamp startDate, Timestamp endDate, Timestamp createDate, Timestamp lastUpdate) {
        this.id = id;
        this.extExaminerId = extExaminerId;
        this.courseId = courseId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.createDate = createDate;
        this.lastUpdate = lastUpdate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getExtExaminerId() {
        return extExaminerId;
    }

    public void setExtExaminerId(long extExaminerId) {
        this.extExaminerId = extExaminerId;
    }

    public long getCourseId() {
        return courseId;
    }

    public void setCourseId(long courseId) {
        this.courseId = courseId;
    }

    public Timestamp getStartDate() {
        return startDate;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    public Timestamp getEndDate() {
        return endDate;
    }

    public void setEndDate(Timestamp endDate) {
        this.endDate = endDate;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Override
    public String toString() {
        return "ExternalExaminerCourse {" +
                "id=" + id + ", " +
                "extExaminerId=" + extExaminerId + ", " +
                "courseId=" + courseId + ", " +
                "startDate=" + startDate + ", " +
                "endDate=" + endDate + ", " +
                "createDate=" + createDate + ", " +
                "lastUpdate=" + lastUpdate +
                "}";
    }


}
