package com.example.examMgt.model.db;

public class ExamCreated {

    private long id;
    private String examId;
    private long examCourseId;
    private long examPeriodId;


    public ExamCreated() {}

    public ExamCreated(long id, String examId, long examCourseId, long examPeriodId) {
        this.id = id;
        this.examId = examId;
        this.examCourseId = examCourseId;
        this.examPeriodId = examPeriodId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getExamId() {
        return examId;
    }

    public void setExamId(String examId) {
        this.examId = examId;
    }

    public long getExamCourseId() {
        return examCourseId;
    }

    public void setExamCourseId(long examCourseId) {
        this.examCourseId = examCourseId;
    }

    public long getExamPeriodId() {
        return examPeriodId;
    }

    public void setExamPeriodId(long examPeriodId) {
        this.examPeriodId = examPeriodId;
    }
}
