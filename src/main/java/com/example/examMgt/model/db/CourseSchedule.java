package com.example.examMgt.model.db;

import java.sql.Timestamp;

public class CourseSchedule {

    private long id;
    private long courseId;
    private long examPeriodId;
    private Timestamp examDate;
    private Timestamp startTime;
    private Timestamp endTime;
    private Timestamp createDate;
    private Timestamp lastUpdate;

    public CourseSchedule() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCourseId() {
        return courseId;
    }

    public void setCourseId(long courseId) {
        this.courseId = courseId;
    }

    public long getExamPeriodId() {
        return examPeriodId;
    }

    public void setExamPeriodId(long examPeriodId) {
        this.examPeriodId = examPeriodId;
    }

    public Timestamp getExamDate() {
        return examDate;
    }

    public void setExamDate(Timestamp examDate) {
        this.examDate = examDate;
    }

    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Override
    public String toString() {
        return "CourseSchedule {" +
                "id=" + id + ", " +
                "courseId=" + courseId + ", " +
                "examPeriodId=" + examPeriodId + ", " +
                "examDate=" + examDate + ", " +
                "startTime=" + startTime + ", " +
                "endTime=" + endTime + ", " +
                "createDate=" + createDate + ", " +
                "lastUpdate=" + lastUpdate +
                "}";
    }

}
