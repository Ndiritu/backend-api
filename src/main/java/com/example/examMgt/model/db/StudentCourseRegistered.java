package com.example.examMgt.model.db;

import java.sql.Timestamp;

public class StudentCourseRegistered {

    private long studentId;
    private long courseId;
    private Timestamp createDate;

    public StudentCourseRegistered() {}

    public StudentCourseRegistered(long studentId, long courseId, Timestamp createDate) {
        this.studentId = studentId;
        this.courseId = courseId;
        this.createDate = createDate;
    }

    public long getStudentId() {
        return studentId;
    }

    public void setStudentId(long studentId) {
        this.studentId = studentId;
    }

    public long getCourseId() {
        return courseId;
    }

    public void setCourseId(long courseId) {
        this.courseId = courseId;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    @Override
    public String toString() {
        return "StudentCourseRegistered{" +
                "studentId=" + studentId + ", " +
                "courseId=" + courseId + ", " +
                "createDate=" + createDate +
                "}";
    }

}
