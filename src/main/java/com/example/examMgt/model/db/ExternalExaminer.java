package com.example.examMgt.model.db;

import com.example.examMgt.model.Staff;

import java.sql.Timestamp;

public class ExternalExaminer extends Staff {

    private boolean approved;
    private long approvedByChairpersonId;

    public ExternalExaminer() {}

    public ExternalExaminer(long id, String firstName, String otherNames, String surname, String email, String password, boolean active, Timestamp createDate, Timestamp lastUpdate, boolean approved, long approvedByChairpersonId) {
        super(id, firstName, otherNames, surname, email, password, active, createDate, lastUpdate);
        this.approved = approved;
        this.approvedByChairpersonId = approvedByChairpersonId;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    public long getApprovedByChairpersonId() {
        return approvedByChairpersonId;
    }

    public void setApprovedByChairpersonId(long approvedByChairpersonId) {
        this.approvedByChairpersonId = approvedByChairpersonId;
    }

    @Override
    public String toString() {
        return "Lecturer{" +
                "id=" + super.getId() + ", " +
                "firstName=" + super.getFirstName() + ", " +
                "otherNames=" + super.getOtherNames() + ", " +
                "surname=" + super.getSurname() + ", " +
                "email=" + super.getEmail() + ", " +
                "password=" + super.getPassword() + ", " +
                "approved=" + approved + ", " +
                "approvedByChairpersonId=" + approvedByChairpersonId + ", " +
                "active=" + super.isActive() + ", " +
                "createDate=" + super.getCreateDate() + ", " +
                "lastUpdate=" + super.getLastUpdate() +
                "}";
    }
}
