package com.example.examMgt.model;

import java.sql.Timestamp;

public class Staff {

    private long id;
    private String firstName;
    private String otherNames;
    private String surname;
    private String email;
    private String password;
    private boolean active;
    private Timestamp createDate;
    private Timestamp lastUpdate;

    protected Staff(long id, String firstName, String otherNames, String surname, String email, String password, boolean active, Timestamp createDate, Timestamp lastUpdate) {
        this.id = id;
        this.firstName = firstName;
        this.otherNames = otherNames;
        this.surname = surname;
        this.email = email;
        this.password = password;
        this.active = active;
        this.createDate = createDate;
        this.lastUpdate = lastUpdate;
    }

    protected Staff() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getOtherNames() {
        return otherNames;
    }

    public void setOtherNames(String otherNames) {
        this.otherNames = otherNames;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
}
