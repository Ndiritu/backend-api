package com.example.examMgt.model;

import org.hyperledger.fabric.sdk.Channel;
import org.hyperledger.fabric.sdk.HFClient;
import org.springframework.stereotype.Component;

@Component
public class UserSession {

    public enum UserType {
        LECTURER, EXT_EXAMINER, EXAM_CENTRE_STAFF, STUDENT, CHAIRPERSON, SYS_ADMIN
    }

    //todo: Add other fields e.g. token, email, firstname?, id?

    private long id;
    private String firstName;
    private String surname;
    private String email;
    private UserType userType;
    private HFClient hfClient;
    private Channel channelExams = null;
    private Channel channelResults = null;

    public UserSession() {}

    public UserSession(long id, String firstName, String surname, String email, UserType userType, HFClient hfClient, Channel channelExams, Channel channelResults) {
        this.id = id;
        this.firstName = firstName;
        this.surname = surname;
        this.email = email;
        this.userType = userType;
        this.hfClient = hfClient;
        this.channelExams = channelExams;
        this.channelResults = channelResults;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public HFClient getHfClient() {
        return hfClient;
    }

    public void setHfClient(HFClient hfClient) {
        this.hfClient = hfClient;
    }

    public Channel getChannelExams() {
        return channelExams;
    }

    public void setChannelExams(Channel channelExams) {
        this.channelExams = channelExams;
    }

    public Channel getChannelResults() {
        return channelResults;
    }

    public void setChannelResults(Channel channelResults) {
        this.channelResults = channelResults;
    }

    @Override
    public String toString() {
        return "UserSession {" +
                "id=" + id + ", " +
                "firstName=" + firstName + ", " +
                "surname=" + surname + ", " +
                "email=" + email + ", " +
                "userType=" + userType +
                "}";
    }

}
