package com.example.examMgt.model;

public class HFResponse {

    private String key;
    private String record;

    public HFResponse() {}

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getRecord() {
        return record;
    }

    public void setRecord(String record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "HFResponse{ " +
                "key=" + key + ", " +
                "record=" + record +
                "}";
    }
}
