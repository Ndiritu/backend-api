package com.example.examMgt.model;

public class ApiResponse {

    private ResponseMetadata metadata;
    private Object data;

    public ApiResponse() {}

    public ApiResponse(ResponseMetadata metadata, Object data) {
        this.metadata = metadata;
        this.data = data;
    }

    public ResponseMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(ResponseMetadata metadata) {
        this.metadata = metadata;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ApiResponse {" +
                "metadata=" + metadata + ", " +
                "data=" + data +
                "}";
    }
}
