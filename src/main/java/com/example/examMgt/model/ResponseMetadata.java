package com.example.examMgt.model;

public class ResponseMetadata {

    private long queryTime;
    private String message;

    public ResponseMetadata(long queryTime, String message) {
        this.queryTime = queryTime;
        this.message = message;
    }

    public ResponseMetadata() {}

    public long getQueryTime() {
        return queryTime;
    }

    public void setQueryTime(long queryTime) {
        this.queryTime = queryTime;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "ResponseMetadata {" +
                "queryTime=" + queryTime + ", " +
                "message=" + message +
                "}";
    }
}
