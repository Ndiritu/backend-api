package com.example.examMgt.model;

public class TxHistoryInfo {

    private String txId;
    private long blockNumber;
    private String sslCertificate;
    private String creatorEmail;
    private String creatorFirstName;
    private String creatorSurname;

    public TxHistoryInfo() {}

    public TxHistoryInfo(String txId, long blockNumber, String sslCertificate, String creatorEmail, String creatorFirstName, String creatorSurname) {
        this.txId = txId;
        this.blockNumber = blockNumber;
        this.sslCertificate = sslCertificate;
        this.creatorEmail = creatorEmail;
        this.creatorFirstName = creatorFirstName;
        this.creatorSurname = creatorSurname;
    }

    public String getTxId() {
        return txId;
    }

    public void setTxId(String txId) {
        this.txId = txId;
    }

    public long getBlockNumber() {
        return blockNumber;
    }

    public void setBlockNumber(long blockNumber) {
        this.blockNumber = blockNumber;
    }

    public String getSslCertificate() {
        return sslCertificate;
    }

    public void setSslCertificate(String sslCertificate) {
        this.sslCertificate = sslCertificate;
    }

    public String getCreatorEmail() {
        return creatorEmail;
    }

    public void setCreatorEmail(String creatorEmail) {
        this.creatorEmail = creatorEmail;
    }

    public String getCreatorFirstName() {
        return creatorFirstName;
    }

    public void setCreatorFirstName(String creatorFirstName) {
        this.creatorFirstName = creatorFirstName;
    }

    public String getCreatorSurname() {
        return creatorSurname;
    }

    public void setCreatorSurname(String creatorSurname) {
        this.creatorSurname = creatorSurname;
    }

    @Override
    public String toString() {
        return "TxHistoryInfo {" +
                "txId=" + txId + ", " +
                "blockNumber=" + blockNumber + ", " +
                "sslCertificate=" + sslCertificate + ", " +
                "creatorEmail=" + creatorEmail + ", " +
                "creatorFirstName=" + creatorFirstName + ", " +
                "creatorSurname=" + creatorSurname +
                "}";
    }

}
