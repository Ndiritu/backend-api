package com.example.examMgt.model.viewModel;

import com.example.examMgt.model.db.Course;
import com.example.examMgt.model.hf.ExamGeneration;
import com.example.examMgt.model.hf.Examination;

import java.sql.Timestamp;

public class ExaminationView {

    private String id;
    private String name;
    private Examination.ExaminationType examinationType;
    private long courseId;
    private String courseName;
    private Course.CourseCode courseCode;
    private int yearOfStudy;
    private long createdByLecturerId;
    private String lecturerFirstName;
    private String lecturerSurname;
    private long examDate;
    private long startTime;
    private long duration;
    private String primaryNumberingFormat;
    private String secondaryNumberingFormat;
    private long createDate;
    private long lastUpdate;
    private boolean deleted;
    private long deleteDate;
    private ItemModerationView moderationDetails;
    private ExamApprovalView approvalDetails;
    private ExamGenerationView generationDetails;

    public ExaminationView() {}

    public ExaminationView(String id, String name, Examination.ExaminationType examinationType, long courseId, String courseName, Course.CourseCode courseCode, long createdByLecturerId, String lecturerFirstName, String lecturerSurname, long examDate, long startTime, long duration, String primaryNumberingFormat, String secondaryNumberingFormat, long createDate, long lastUpdate) {
        this.id = id;
        this.name = name;
        this.examinationType = examinationType;
        this.courseId = courseId;
        this.courseName = courseName;
        this.courseCode = courseCode;
        this.createdByLecturerId = createdByLecturerId;
        this.lecturerFirstName = lecturerFirstName;
        this.lecturerSurname = lecturerSurname;
        this.examDate = examDate;
        this.startTime = startTime;
        this.duration = duration;
        this.primaryNumberingFormat = primaryNumberingFormat;
        this.secondaryNumberingFormat = secondaryNumberingFormat;
        this.createDate = createDate;
        this.lastUpdate = lastUpdate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Examination.ExaminationType getExaminationType() {
        return examinationType;
    }

    public void setExaminationType(Examination.ExaminationType examinationType) {
        this.examinationType = examinationType;
    }

    public long getCourseId() {
        return courseId;
    }

    public void setCourseId(long courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public Course.CourseCode getCourseCode() {
        return courseCode;
    }

    public void setCourseCode(Course.CourseCode courseCode) {
        this.courseCode = courseCode;
    }

    public int getYearOfStudy() {
        return yearOfStudy;
    }

    public void setYearOfStudy(int yearOfStudy) {
        this.yearOfStudy = yearOfStudy;
    }

    public long getCreatedByLecturerId() {
        return createdByLecturerId;
    }

    public void setCreatedByLecturerId(long createdByLecturerId) {
        this.createdByLecturerId = createdByLecturerId;
    }

    public String getLecturerFirstName() {
        return lecturerFirstName;
    }

    public void setLecturerFirstName(String lecturerFirstName) {
        this.lecturerFirstName = lecturerFirstName;
    }

    public String getLecturerSurname() {
        return lecturerSurname;
    }

    public void setLecturerSurname(String lecturerSurname) {
        this.lecturerSurname = lecturerSurname;
    }

    public long getExamDate() {
        return examDate;
    }

    public void setExamDate(long examDate) {
        this.examDate = examDate;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public String getPrimaryNumberingFormat() {
        return primaryNumberingFormat;
    }

    public void setPrimaryNumberingFormat(String primaryNumberingFormat) {
        this.primaryNumberingFormat = primaryNumberingFormat;
    }

    public String getSecondaryNumberingFormat() {
        return secondaryNumberingFormat;
    }

    public void setSecondaryNumberingFormat(String secondaryNumberingFormat) {
        this.secondaryNumberingFormat = secondaryNumberingFormat;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public long getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public long getDeleteDate() {
        return deleteDate;
    }

    public void setDeleteDate(long deleteDate) {
        this.deleteDate = deleteDate;
    }

    public ItemModerationView getModerationDetails() {
        return moderationDetails;
    }

    public void setModerationDetails(ItemModerationView moderationDetails) {
        this.moderationDetails = moderationDetails;
    }

    public ExamApprovalView getApprovalDetails() {
        return approvalDetails;
    }

    public void setApprovalDetails(ExamApprovalView approvalDetails) {
        this.approvalDetails = approvalDetails;
    }

    public ExamGenerationView getGenerationDetails() {
        return generationDetails;
    }

    public void setGenerationDetails(ExamGenerationView generationDetails) {
        this.generationDetails = generationDetails;
    }

    @Override
    public String toString() {
        return "ExaminationView{" +
                "id=" + id + ", " +
                "name=" + name + ", " +
                "examinationType=" + examinationType + ", " +
                "courseId=" + courseId + ", " +
                "courseName=" + courseName + ", " +
                "courseCode=" + courseCode + ", " +
                "yearOfStudy=" + yearOfStudy + ", " +
                "createdByLecturerId=" + createdByLecturerId + ", " +
                "lecturerFirstName=" + lecturerFirstName + ", " +
                "lecturerSurname=" + lecturerSurname + ", " +
                "examDate=" + examDate + ", " +
                "startTime=" + startTime + ", " +
                "duration=" + duration + ", " +
                "primaryNumberingFormat=" + primaryNumberingFormat + ", " +
                "secondaryNumberingFormat=" + secondaryNumberingFormat + ", " +
                "createDate=" + createDate + ", " +
                "lastUpdate=" + lastUpdate + ", " +
                "deleted=" + deleted + ", " +
                "deleteDate=" + deleteDate + ", " +
                "moderationDetails=" + moderationDetails + ", " +
                "approvalDetails=" + approvalDetails + ", " +
                "generationDetails=" + generationDetails +
                "}";
    }

}
