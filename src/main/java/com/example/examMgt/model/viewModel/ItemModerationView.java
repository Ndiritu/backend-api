package com.example.examMgt.model.viewModel;

import com.example.examMgt.model.hf.ItemModeration;

import java.sql.Timestamp;

public class ItemModerationView {

    private String itemType;
    private String itemId;
    private long extExaminerId;
    private String extExaminerFirstName;
    private String extExaminerSurname;
    private boolean status;
    private long createDate;
    private long lastUpdate;

    public ItemModerationView() {}

    public ItemModerationView(String itemType, String itemId, long extExaminerId, String extExaminerFirstName, String extExaminerSurname, boolean status, long createDate, long lastUpdate) {
        this.itemType = itemType;
        this.itemId = itemId;
        this.extExaminerId = extExaminerId;
        this.extExaminerFirstName = extExaminerFirstName;
        this.extExaminerSurname = extExaminerSurname;
        this.status = status;
        this.createDate = createDate;
        this.lastUpdate = lastUpdate;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(ItemModeration.ItemType itemType) {
        this.itemType = itemType.toString();
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public long getExtExaminerId() {
        return extExaminerId;
    }

    public void setExtExaminerId(long extExaminerId) {
        this.extExaminerId = extExaminerId;
    }

    public String getExtExaminerFirstName() {
        return extExaminerFirstName;
    }

    public void setExtExaminerFirstName(String extExaminerFirstName) {
        this.extExaminerFirstName = extExaminerFirstName;
    }

    public String getExtExaminerSurname() {
        return extExaminerSurname;
    }

    public void setExtExaminerSurname(String extExaminerSurname) {
        this.extExaminerSurname = extExaminerSurname;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public long getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Override
    public String toString() {
        return "ItemModerationView {" +
                "itemType=" + itemType + ", " +
                "itemId=" + itemId + ", " +
                "extExaminerId=" + extExaminerId + ", " +
                "extExaminerFirstName=" + extExaminerFirstName + ", " +
                "extExaminerSurname=" + extExaminerSurname + ", " +
                "status=" + status + ", " +
                "createDate=" + new Timestamp(createDate) + ", " +
                "lastUpdate=" + new Timestamp(lastUpdate) +
                "}";
    }
}
