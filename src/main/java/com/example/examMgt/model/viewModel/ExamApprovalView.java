package com.example.examMgt.model.viewModel;

public class ExamApprovalView {

    private String examId;
    private long chairpersonId;
    private String chairpersonFirstName;
    private String chairpersonSurname;
    private boolean status;
    private long createDate;
    private long lastUpdate;

    public ExamApprovalView(String examId, long chairpersonId, String chairpersonFirstName, String chairpersonSurname, boolean status, long createDate, long lastUpdate) {
        this.examId = examId;
        this.chairpersonId = chairpersonId;
        this.chairpersonFirstName = chairpersonFirstName;
        this.chairpersonSurname = chairpersonSurname;
        this.status = status;
        this.createDate = createDate;
        this.lastUpdate = lastUpdate;
    }

    public ExamApprovalView() {}

    public String getExamId() {
        return examId;
    }

    public void setExamId(String examId) {
        this.examId = examId;
    }

    public long getChairpersonId() {
        return chairpersonId;
    }

    public void setChairpersonId(long chairpersonId) {
        this.chairpersonId = chairpersonId;
    }

    public String getChairpersonFirstName() {
        return chairpersonFirstName;
    }

    public void setChairpersonFirstName(String chairpersonFirstName) {
        this.chairpersonFirstName = chairpersonFirstName;
    }

    public String getChairpersonSurname() {
        return chairpersonSurname;
    }

    public void setChairpersonSurname(String chairpersonSurname) {
        this.chairpersonSurname = chairpersonSurname;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public long getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Override
    public String toString() {
        return "ExamApprovalView {" +
                "examId=" + examId + ", " +
                "chairpersonId=" + chairpersonId + ", " +
                "chairpersonFirstName=" + chairpersonFirstName + ", " +
                "chairpersonSurname=" + chairpersonSurname + ", " +
                "status=" + status + ", " +
                "createDate=" + createDate + ", " +
                "lastUpdate=" + lastUpdate +
                "}";
    }
}
