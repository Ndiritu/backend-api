package com.example.examMgt.model.viewModel;

import com.example.examMgt.model.hf.Comment;
import com.example.examMgt.model.hf.ItemModeration;

public class CommentView {

    private String id;
    private String text;
    private Comment.SenderType senderType;
    private long senderId;
    private String senderFirstName;
    private String senderSurname;
    private Comment.RecipientType recipientType;
    private long recipientId;
    private String recipientFirstName;
    private String recipientSurname;
    private ItemModeration.ItemType itemType;
    private String itemId;
    private String examId;
    private long createDate;
    private long lastUpdate;
    private boolean deleted;
    private long deleteDate;

    public CommentView() {}

    public CommentView(String id, String text, Comment.SenderType senderType, long senderId, String senderFirstName, String senderSurname, Comment.RecipientType recipientType, long recipientId, String recipientFirstName, String recipientSurname, ItemModeration.ItemType itemType, String itemId, String examId, long createDate, long lastUpdate, boolean deleted, long deleteDate) {
        this.id = id;
        this.text = text;
        this.senderType = senderType;
        this.senderId = senderId;
        this.senderFirstName = senderFirstName;
        this.senderSurname = senderSurname;
        this.recipientType = recipientType;
        this.recipientId = recipientId;
        this.recipientFirstName = recipientFirstName;
        this.recipientSurname = recipientSurname;
        this.itemType = itemType;
        this.itemId = itemId;
        this.examId = examId;
        this.createDate = createDate;
        this.lastUpdate = lastUpdate;
        this.deleted = deleted;
        this.deleteDate = deleteDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Comment.SenderType getSenderType() {
        return senderType;
    }

    public void setSenderType(Comment.SenderType senderType) {
        this.senderType = senderType;
    }

    public long getSenderId() {
        return senderId;
    }

    public void setSenderId(long senderId) {
        this.senderId = senderId;
    }

    public String getSenderFirstName() {
        return senderFirstName;
    }

    public void setSenderFirstName(String senderFirstName) {
        this.senderFirstName = senderFirstName;
    }

    public String getSenderSurname() {
        return senderSurname;
    }

    public void setSenderSurname(String senderSurname) {
        this.senderSurname = senderSurname;
    }

    public Comment.RecipientType getRecipientType() {
        return recipientType;
    }

    public void setRecipientType(Comment.RecipientType recipientType) {
        this.recipientType = recipientType;
    }

    public long getRecipientId() {
        return recipientId;
    }

    public void setRecipientId(long recipientId) {
        this.recipientId = recipientId;
    }

    public String getRecipientFirstName() {
        return recipientFirstName;
    }

    public void setRecipientFirstName(String recipientFirstName) {
        this.recipientFirstName = recipientFirstName;
    }

    public String getRecipientSurname() {
        return recipientSurname;
    }

    public void setRecipientSurname(String recipientSurname) {
        this.recipientSurname = recipientSurname;
    }

    public ItemModeration.ItemType getItemType() {
        return itemType;
    }

    public void setItemType(ItemModeration.ItemType itemType) {
        this.itemType = itemType;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getExamId() {
        return examId;
    }

    public void setExamId(String examId) {
        this.examId = examId;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public long getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public long getDeleteDate() {
        return deleteDate;
    }

    public void setDeleteDate(long deleteDate) {
        this.deleteDate = deleteDate;
    }

    @Override
    public String toString() {
        return "CommentView {" +
                "id=" + id + ", " +
                "text=" + text + ", " +
                "senderType=" + senderType + ", " +
                "senderId=" + senderId + ", " +
                "senderFirstName=" + senderFirstName + ", " +
                "senderSurname=" + senderSurname + ", " +
                "recipientType=" + recipientType + ", " +
                "recipientId=" + recipientId + ", " +
                "recipientFirstName=" + recipientFirstName + ", " +
                "recipientSurname=" + recipientSurname + ", " +
                "itemType=" + itemType + ", " +
                "itemId=" + itemId + ", " +
                "examId=" + examId + ", " +
                "createDate=" + createDate + ", " +
                "lastUpdate=" + lastUpdate + ", " +
                "deleted=" + deleted + ", " +
                "deleteDate=" + deleteDate +
                "}";
    }

}
