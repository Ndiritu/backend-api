package com.example.examMgt.model.viewModel;

import java.sql.Timestamp;

public class ChairpersonView {

    private long id;
    private long lecturerId;
    private String firstName;
    private String otherNames;
    private String surname;
    private String email;
    private boolean active;
    private Timestamp startDate;
    private Timestamp endDate;

    public ChairpersonView() {}

    public ChairpersonView(long id, long lecturerId, String firstName, String otherNames, String surname, String email, boolean active, Timestamp startDate, Timestamp endDate) {
        this.id = id;
        this.lecturerId = lecturerId;
        this.firstName = firstName;
        this.otherNames = otherNames;
        this.surname = surname;
        this.email = email;
        this.active = active;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getLecturerId() {
        return lecturerId;
    }

    public void setLecturerId(long lecturerId) {
        this.lecturerId = lecturerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getOtherNames() {
        return otherNames;
    }

    public void setOtherNames(String otherNames) {
        this.otherNames = otherNames;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Timestamp getStartDate() {
        return startDate;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    public Timestamp getEndDate() {
        return endDate;
    }

    public void setEndDate(Timestamp endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "ChairpersonView{" +
                "id=" + id + ", " +
                "lecturerId=" + lecturerId + ", " +
                "firstName=" + firstName + ", " +
                "otherNames=" + otherNames + ", " +
                "surname=" + surname + ", " +
                "email=" + email + ", " +
                "active=" + active + ", " +
                "startDate=" + startDate + ", " +
                "endDate=" + endDate +
                "}";
    }

}
