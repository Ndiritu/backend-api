package com.example.examMgt.model.viewModel;

public class InstructionView {

    private String id;
    private String examId;
    private int displayIndex;
    private String type;
    private String text;
    private ItemModerationView moderationDetails;
    private long createDate;
    private long lastUpdate;
    private boolean deleted;
    private long deleteDate;
    private String docType;

    public InstructionView() {}

    public InstructionView(String id, String examId, int displayIndex, String type, String text, ItemModerationView moderationDetails, long createDate, long lastUpdate, boolean deleted, long deleteDate, String docType) {
        this.id = id;
        this.examId = examId;
        this.displayIndex = displayIndex;
        this.type = type;
        this.text = text;
        this.moderationDetails = moderationDetails;
        this.createDate = createDate;
        this.lastUpdate = lastUpdate;
        this.deleted = deleted;
        this.deleteDate = deleteDate;
        this.docType = docType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getExamId() {
        return examId;
    }

    public void setExamId(String examId) {
        this.examId = examId;
    }

    public int getDisplayIndex() {
        return displayIndex;
    }

    public void setDisplayIndex(int displayIndex) {
        this.displayIndex = displayIndex;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public ItemModerationView getModerationDetails() {
        return moderationDetails;
    }

    public void setModerationDetails(ItemModerationView moderationDetails) {
        this.moderationDetails = moderationDetails;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public long getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public long getDeleteDate() {
        return deleteDate;
    }

    public void setDeleteDate(long deleteDate) {
        this.deleteDate = deleteDate;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    @Override
    public String toString() {
        return "InstructionView { " +
                "id=" + id + ", " +
                "examId=" + examId + ", " +
                "displayIndex=" + displayIndex + ", " +
                "type=" + type + ", " +
                "text=" + text + ", " +
                "moderationDetails=" + moderationDetails + ", " +
                "createDate=" + createDate + ", " +
                "lastUpdate=" + lastUpdate + ", " +
                "deleted=" + deleted + ", " +
                "deleteDate=" + deleteDate + ", " +
                "docType=" + docType +
                "}";
    }
}
