package com.example.examMgt.model.viewModel;

public class ExamGenerationView {

    private String examId;
    private long examCentreStaffId;
    private String examCentreStaffFirstName;
    private String examCentreStaffSurname;
    private boolean examCentreStatus;
    private int examCentreNumCopies;
    private long lecturerId;
    private String lecturerFirstName;
    private String lecturerSurname;
    private boolean lecturerStatus;
    private int lecturerNumCopies;
    private long createDate;
    private long lastUpdate;
    private String docType;

    public ExamGenerationView(String examId, long examCentreStaffId, String examCentreStaffFirstName, String examCentreStaffSurname, boolean examCentreStatus, int examCentreNumCopies, long lecturerId, String lecturerFirstName, String lecturerSurname, boolean lecturerStatus, int lecturerNumCopies, long createDate, long lastUpdate, String docType) {
        this.examId = examId;
        this.examCentreStaffId = examCentreStaffId;
        this.examCentreStaffFirstName = examCentreStaffFirstName;
        this.examCentreStaffSurname = examCentreStaffSurname;
        this.examCentreStatus = examCentreStatus;
        this.examCentreNumCopies = examCentreNumCopies;
        this.lecturerId = lecturerId;
        this.lecturerFirstName = lecturerFirstName;
        this.lecturerSurname = lecturerSurname;
        this.lecturerStatus = lecturerStatus;
        this.lecturerNumCopies = lecturerNumCopies;
        this.createDate = createDate;
        this.lastUpdate = lastUpdate;
        this.docType = docType;
    }

    public ExamGenerationView() {}

    public String getExamId() {
        return examId;
    }

    public void setExamId(String examId) {
        this.examId = examId;
    }

    public long getExamCentreStaffId() {
        return examCentreStaffId;
    }

    public void setExamCentreStaffId(long examCentreStaffId) {
        this.examCentreStaffId = examCentreStaffId;
    }

    public String getExamCentreStaffFirstName() {
        return examCentreStaffFirstName;
    }

    public void setExamCentreStaffFirstName(String examCentreStaffFirstName) {
        this.examCentreStaffFirstName = examCentreStaffFirstName;
    }

    public String getExamCentreStaffSurname() {
        return examCentreStaffSurname;
    }

    public void setExamCentreStaffSurname(String examCentreStaffSurname) {
        this.examCentreStaffSurname = examCentreStaffSurname;
    }

    public boolean isExamCentreStatus() {
        return examCentreStatus;
    }

    public void setExamCentreStatus(boolean examCentreStatus) {
        this.examCentreStatus = examCentreStatus;
    }

    public int getExamCentreNumCopies() {
        return examCentreNumCopies;
    }

    public void setExamCentreNumCopies(int examCentreNumCopies) {
        this.examCentreNumCopies = examCentreNumCopies;
    }

    public long getLecturerId() {
        return lecturerId;
    }

    public void setLecturerId(long lecturerId) {
        this.lecturerId = lecturerId;
    }

    public String getLecturerFirstName() {
        return lecturerFirstName;
    }

    public void setLecturerFirstName(String lecturerFirstName) {
        this.lecturerFirstName = lecturerFirstName;
    }

    public String getLecturerSurname() {
        return lecturerSurname;
    }

    public void setLecturerSurname(String lecturerSurname) {
        this.lecturerSurname = lecturerSurname;
    }

    public boolean isLecturerStatus() {
        return lecturerStatus;
    }

    public void setLecturerStatus(boolean lecturerStatus) {
        this.lecturerStatus = lecturerStatus;
    }

    public int getLecturerNumCopies() {
        return lecturerNumCopies;
    }

    public void setLecturerNumCopies(int lecturerNumCopies) {
        this.lecturerNumCopies = lecturerNumCopies;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public long getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    @Override
    public String toString() {
        return "ExamGenerationView {" +
                "examId=" + examId + ", " +
                "examCentreStaffId=" + examCentreStaffId + ", " +
                "examCentreStaffFirstName=" + examCentreStaffFirstName + ", " +
                "examCentreStaffSurname=" + examCentreStaffSurname + ", " +
                "examCentreStatus=" + examCentreStatus + ", " +
                "examCentreNumCopies=" + examCentreNumCopies + ", " +
                "lecturerId=" + lecturerId + ", " +
                "lecturerFirstName=" + lecturerFirstName + ", " +
                "lecturerSurname=" + lecturerSurname + ", " +
                "lecturerStatus=" + lecturerStatus + ", " +
                "lecturerNumCopies=" + lecturerNumCopies + ", " +
                "createDate=" + createDate + ", " +
                "lastUpdate=" + lastUpdate + ", " +
                "docType=" + docType +
                "}";
    }

}
