package com.example.examMgt.model.viewModel;

public class ExaminationHistoryView {

    private String txId;
    private ExaminationView value;
    private long timestamp;
    private boolean isDelete;

    public ExaminationHistoryView(String txId, ExaminationView value, long timestamp, boolean isDelete) {
        this.txId = txId;
        this.value = value;
        this.timestamp = timestamp;
        this.isDelete = isDelete;
    }

    public ExaminationHistoryView() {}

    public String getTxId() {
        return txId;
    }

    public void setTxId(String txId) {
        this.txId = txId;
    }

    public ExaminationView getValue() {
        return value;
    }

    public void setValue(ExaminationView value) {
        this.value = value;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean delete) {
        isDelete = delete;
    }

    @Override
    public String toString() {
        return "ExaminationHistoryView {" +
                "txId=" + txId + ", " +
                "value=" + value + ", " +
                "timestamp=" + timestamp + ", " +
                "isDelete=" + isDelete +
                "}";
    }
}
