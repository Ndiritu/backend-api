package com.example.examMgt.service;

import com.example.examMgt.model.db.Course;
import com.example.examMgt.model.db.CourseSchedule;
import com.example.examMgt.utils.SqlUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CourseService {

    private static final Logger logger = Logger.getLogger(CourseService.class);

    @Autowired
    private JdbcTemplateService jdbcTemplateService;

    public List<Course> getAllCourses() {
        try {
            String sql = SqlUtils.instance.findSqlStatement("getAllCourses");
            return jdbcTemplateService.theACJdbcTemplate().query(sql, new CourseRowMapper());

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public List<Course> getCoursesTaughtByLec(long lecturerId) {
        try {
            Map<String, Long> params = new HashMap<>();
            params.put("lecturerId", lecturerId);

            String sql = SqlUtils.instance.findSqlStatement("getCoursesTaughtByLecturer");
            return jdbcTemplateService.theACJdbcTemplate().query(sql, params, new CourseRowMapper());

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public List<Course> getCoursesByExtExaminer(long extExaminerId) {
        try {
            Map<String, Long> params = new HashMap<>();
            params.put("extExaminerId", extExaminerId);

            String sql = SqlUtils.instance.findSqlStatement("getCoursesByExtExaminer");
            return jdbcTemplateService.theACJdbcTemplate().query(sql, params, new CourseRowMapper());

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public Course getCourseById(long courseId) {
        try {
            Map<String, Long> params = new HashMap<>();
            params.put("courseId", courseId);

            String sql = SqlUtils.instance.findSqlStatement("getCourseById");
            CourseCallbackHandler courseCallbackHandler = new CourseCallbackHandler();
            jdbcTemplateService.theACJdbcTemplate().query(sql, params, courseCallbackHandler);

            Course course = courseCallbackHandler.getCourse();
            if (course.getId() == 0) {
                return null;
            }
            return course;

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public List<Course> getCoursesByIds(List<Long> courseIds) {
        try {
            Array courseIdArr = SqlUtils.getPostgresBigintArray(courseIds, jdbcTemplateService.getDBConnection());

            Map<String, Object> params = new HashMap<>();
            params.put("courseIds", courseIdArr);

            String sql = SqlUtils.instance.findSqlStatement("getCoursesByIds");
            return jdbcTemplateService.theACJdbcTemplate().query(sql, params, new CourseRowMapper());

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return new ArrayList<>();
        }
    }

    public CourseSchedule getCourseScheduleByExamAndPeriod(String examId, long examPeriod) {
        try {
            Map<String, Object> params = new HashMap<>();
            params.put("examId", examId);
            params.put("examPeriodId", examPeriod);

            String sql = SqlUtils.instance.findSqlStatement("getCourseScheduleByExamAndPeriod");
            CourseScheduleCallbackHandler courseScheduleCallbackHandler = new CourseScheduleCallbackHandler();
            jdbcTemplateService.theACJdbcTemplate().query(sql, params, courseScheduleCallbackHandler);

            return courseScheduleCallbackHandler.getCourseSchedule();

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    private static class CourseScheduleCallbackHandler implements RowCallbackHandler {
        private CourseSchedule courseSchedule;

        public CourseSchedule getCourseSchedule() {
            return courseSchedule;
        }

        private void setCourseSchedule(CourseSchedule courseSchedule) {
            this.courseSchedule = courseSchedule;
        }

        @Override
        public void processRow(ResultSet rs) throws SQLException {
            long id = rs.getLong("id");
            long courseId = rs.getLong("course_id");
            long examPeriodId = rs.getLong("exam_period_id");
            Timestamp examDate = rs.getTimestamp("exam_date");
            Timestamp startTime = rs.getTimestamp("start_time");
            Timestamp endTime = rs.getTimestamp("end_time");
            Timestamp createDate = rs.getTimestamp("create_date");
            Timestamp lastUpdate = rs.getTimestamp("last_update");

            CourseSchedule courseSchedule = new CourseSchedule();
            courseSchedule.setId(id);
            courseSchedule.setCourseId(courseId);
            courseSchedule.setExamPeriodId(examPeriodId);
            courseSchedule.setExamDate(examDate);
            courseSchedule.setStartTime(startTime);
            courseSchedule.setEndTime(endTime);
            courseSchedule.setCreateDate(createDate);
            courseSchedule.setLastUpdate(lastUpdate);

            setCourseSchedule(courseSchedule);

        }
    }

    private static class CourseRowMapper implements RowMapper<Course> {
        @Override
        public Course mapRow(ResultSet rs, int i) throws SQLException {
            long id = rs.getLong("id");
            String name = rs.getString("name");
            Course.CourseCode courseCode = Course.CourseCode.valueOf(rs.getString("course_code"));
            int yearOfStudy = rs.getInt("year_of_study");
            Course.Semester semester = Course.Semester.valueOf(rs.getString("semester"));
            long degreeId = rs.getLong("degree_id");

            Course course = new Course();
            course.setId(id);
            course.setName(name);
            course.setCourseCode(courseCode);
            course.setYearOfStudy(yearOfStudy);
            course.setSemester(semester);
            course.setDegreeId(degreeId);
            return course;
        }
    }

    private static class CourseCallbackHandler implements RowCallbackHandler {
        private Course course;

        public Course getCourse() {
            return course;
        }

        private void setCourse(Course course) {
            this.course = course;
        }

        @Override
        public void processRow(ResultSet rs) throws SQLException {
            long id = rs.getLong("id");
            String name = rs.getString("name");
            Course.CourseCode courseCode = Course.CourseCode.valueOf(rs.getString("course_code"));
            int yearOfStudy = rs.getInt("year_of_study");
            Course.Semester semester = Course.Semester.valueOf(rs.getString("semester"));
            long degreeId = rs.getLong("degree_id");

            Course course = new Course();
            course.setId(id);
            course.setName(name);
            course.setCourseCode(courseCode);
            course.setYearOfStudy(yearOfStudy);
            course.setSemester(semester);
            course.setDegreeId(degreeId);

            setCourse(course);
        }
    }
}
