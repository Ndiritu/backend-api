package com.example.examMgt.service;

import com.example.examMgt.model.HFResponse;
import com.example.examMgt.model.UserSession;
import com.example.examMgt.model.ValidationResponse;
import com.example.examMgt.model.hf.ExamItem;
import com.example.examMgt.model.hf.Instruction;
import com.example.examMgt.model.hf.ItemModeration;
import com.example.examMgt.model.hf.Question;
import com.example.examMgt.model.hf.history.HFHistoryResponse;
import com.example.examMgt.model.hf.history.QuestionHistory;
import com.example.examMgt.model.viewModel.ItemModerationView;
import com.example.examMgt.model.viewModel.QuestionView;
import org.apache.log4j.Logger;
import org.hyperledger.fabric.sdk.BlockEvent;
import org.hyperledger.fabric.sdk.ChaincodeResponse;
import org.hyperledger.fabric.sdk.ProposalResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@Service
public class QuestionService extends BaseService {

    private static final Logger logger = Logger.getLogger(QuestionService.class);

    private @Value("${hf.chaincode.question.chaincodeId}") String chaincodeQstnId;

    private static EnumMap<Question.QuestionFunction, String> chaincodeFnMap = new EnumMap<>(Question.QuestionFunction.class);

    private @Value("${hf.chaincode.question.create}") String create;
    private @Value("${hf.chaincode.question.update}") String update;
    private @Value("${hf.chaincode.question.getById}") String getById;
    private @Value("${hf.chaincode.question.getByExamId}") String getByExamId;
    private @Value("${hf.chaincode.question.getAll}") String getAll;
    private @Value("${hf.chaincode.question.getHistory}") String getHistory;
    private @Value("${hf.chaincode.question.delete}") String delete;

    @Autowired private ItemModerationService itemModerationService;

    @Autowired private ExaminationService examinationService;

    @Autowired private InstructionService instructionService;

    @PostConstruct
    public void init() {
        initChaincodeFnMap();
    }

    private void initChaincodeFnMap() {
        chaincodeFnMap.put(Question.QuestionFunction.CREATE, create);
        chaincodeFnMap.put(Question.QuestionFunction.UPDATE, update);
        chaincodeFnMap.put(Question.QuestionFunction.GET_BY_ID, getById);
        chaincodeFnMap.put(Question.QuestionFunction.GET_BY_EXAM_ID, getByExamId);
        chaincodeFnMap.put(Question.QuestionFunction.GET_ALL, getAll);
        chaincodeFnMap.put(Question.QuestionFunction.GET_HISTORY, getHistory);
        chaincodeFnMap.put(Question.QuestionFunction.DELETE, delete);
    }

    public List<QuestionView> convertToQuestionViews(UserSession userSession, List<Question> questions) {
        if (questions.isEmpty()) {
            return new ArrayList<>();
        }

        List<ItemModeration> moderations = new ArrayList<>();
        ItemModeration itemModeration;
        for (Question question : questions) {
            itemModeration = itemModerationService.getModerationByItem(
                    userSession, ItemModeration.ItemType.QUESTION, question.getId()
            );
            if (itemModeration != null) {
                moderations.add(itemModeration);
            }
        }

        List<ItemModerationView> moderationViews = itemModerationService.convertToItemModerationView(
                moderations
        );
        HashMap<String, ItemModerationView> moderationViewHashMap = new HashMap<>();
        for (ItemModerationView moderationView : moderationViews) {
            moderationViewHashMap.put(moderationView.getItemId(), moderationView);
        }

        List<QuestionView> questionViews = new ArrayList<>();
        for (Question question : questions) {
            QuestionView questionView = new QuestionView();
            questionView.setId(question.getId());
            questionView.setExamId(question.getExamId());
            questionView.setDisplayIndex(question.getDisplayIndex());
            questionView.setType(question.getType());
            questionView.setText(question.getText());
            questionView.setMarks(question.getMarks());
            questionView.setModerationDetails(
                    moderationViewHashMap.get(question.getId())
            );
            questionView.setCreateDate(question.getCreateDate());
            questionView.setLastUpdate(question.getLastUpdate());
            questionView.setDeleted(question.isDeleted());
            questionView.setDeleteDate(question.getDeleteDate());
            questionView.setDocType(question.getDocType());

            questionViews.add(questionView);
        }
        return questionViews;
    }

    public void deleteNestedItems(UserSession userSession, Question question) throws Exception {
        List<Object> examTree = examinationService.buildExamTree(userSession, question.getExamId());

        List<Object> itemsAfterQuestion = new ArrayList<>();
        for (int i = 0; i <= examTree.size() - 1; i ++) {
            ExamItem item = (ExamItem) examTree.get(i);
            if (item.getId() == question.getId()) {
                itemsAfterQuestion = examTree.subList(i + 1, examTree.size());
            }
        }

        //TODO: Loop will delete sub-question that doesn't belong to main question but still in section

        for (Object item : itemsAfterQuestion) {
            ExamItem examItem = (ExamItem) item;

            if (examItem.getDocType().equals("question")) {
                Question qstn = (Question) examItem;
                if (Question.QuestionType.MAIN_QUESTION
                        == Question.QuestionType.valueOf(qstn.getType())) {
                    break;
                } else {
                    logger.debug("Deleting=" + question);
//                    deleteQuestion(userSession, examItem.getId());
                }
            }
            if (examItem.getDocType().equals("section")) {
                break;
            }
            if (examItem.getDocType().equals("instruction")) {
                Instruction instruction = (Instruction) examItem;
                if (Instruction.InstructionType.QUESTION_LEVEL
                        == Instruction.InstructionType.valueOf(instruction.getType())) {
                    logger.debug("Deleting=" + instruction);
//                    instructionService.deleteInstruction(userSession, instruction.getId());
                } else {
                    break;
                }
            }
        }
    }

    public boolean deleteQuestion(UserSession userSession, String questionId) {
        try {
            ArrayList<String> args = new ArrayList<>();
            args.add(String.valueOf(userSession.getId()));
            args.add(questionId);

            //todo: handle completable future better. Then check if transactionEvent.isValid()
            try {
                BlockEvent.TransactionEvent transactionEvent = invokeChaincode(
                        userSession.getHfClient(),
                        userSession.getChannelExams(),
                        chaincodeQstnId,
                        chaincodeFnMap.get(Question.QuestionFunction.DELETE),
                        args
                ).get(1, TimeUnit.SECONDS);
            } catch (TimeoutException ex) {
                //todo: May not always return most updated question. Method shd probably return boolean
                return true;
            }
            throw new Exception("Unexpected error occurred");

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return false;
        }
    }

    public List<QuestionHistory> getQuestionHistory(UserSession userSession, String questionId) {
        try {
            ArrayList<String> args = new ArrayList<>();
            args.add(questionId);

            return unpackHFHistoryPayload(
                    queryChaincode(
                            userSession.getHfClient(),
                            userSession.getChannelExams(),
                            chaincodeQstnId,
                            chaincodeFnMap.get(Question.QuestionFunction.GET_HISTORY),
                            args
                    )
            );

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public List<Question> getAllQuestions(UserSession userSession) {
        try {
            return unpackHFPayload(
                    queryChaincode(
                            userSession.getHfClient(),
                            userSession.getChannelExams(),
                            chaincodeQstnId,
                            chaincodeFnMap.get(Question.QuestionFunction.GET_ALL),
                            new ArrayList<>()
                    )
            );

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    private ArrayList<String> getCreateValuesList(Question question) {
        //Order of args must align with chaincode order of params
        ArrayList<String> args = new ArrayList<>();
        args.add(question.getId());
        args.add(question.getExamId());
        args.add(String.valueOf(question.getDisplayIndex()));
        args.add(question.getType());
        args.add(question.getText());
        args.add(String.valueOf(question.getMarks()));
        return args;
    }

    private ArrayList<String> getUpdateValuesList(Question question, Long lecturerId) {
        //Order of args insertion must align with chaincode order of params
        ArrayList<String> args = new ArrayList<>();
        args.add(question.getId());
        args.add(String.valueOf(lecturerId));
        args.add(String.valueOf(question.getDisplayIndex()));
        args.add(question.getType());
        args.add(question.getText());
        args.add(String.valueOf(question.getMarks()));
        return args;
    }

    public boolean updateQuestion(UserSession userSession, Question updatedQuestion) {
        try {
            //todo: handle completable future better. Then check if transactionEvent.isValid()
            try {
                BlockEvent.TransactionEvent transactionEvent = invokeChaincode(
                        userSession.getHfClient(),
                        userSession.getChannelExams(),
                        chaincodeQstnId,
                        chaincodeFnMap.get(Question.QuestionFunction.UPDATE),
                        getUpdateValuesList(updatedQuestion, userSession.getId())
                ).get(1, TimeUnit.SECONDS);
            } catch (TimeoutException ex) {
                //todo: May not always return most updated question. Method shd probably return boolean
                return true;
            }
            throw new Exception("Unexpected error occurred");

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return false;
        }
    }

    //todo: complete validation
    public ValidationResponse validateQuestion(Question question) {
        ValidationResponse validationResponse = new ValidationResponse();
        try {
            validationResponse.setValid(true);
            validationResponse.setMessage("");

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            validationResponse.setValid(false);
            validationResponse.setMessage(ex.toString());
        }
        return validationResponse;
    }

    public List<Question> getQuestionsByExamId(UserSession userSession, String examId) {
        try {
            ArrayList<String> args = new ArrayList<>();
            args.add(examId);

            return unpackHFPayload(
                    queryChaincode(
                            userSession.getHfClient(),
                            userSession.getChannelExams(),
                            chaincodeQstnId,
                            chaincodeFnMap.get(Question.QuestionFunction.GET_BY_EXAM_ID),
                            args
                    )
            );

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    private List<QuestionHistory> unpackHFHistoryPayload(
            Collection<ProposalResponse> proposalResponses) throws Exception {
        List<QuestionHistory> questionHistories = new ArrayList<>();

        String payload;
        for (ProposalResponse response : proposalResponses) {
            payload = new String(response.getChaincodeActionResponsePayload());
            logger.info("response: " + payload);

            if (response.isVerified()) {
                if (response.getStatus() == ChaincodeResponse.Status.SUCCESS) {
                    HFHistoryResponse[] responses = gson.fromJson(payload, HFHistoryResponse[].class);

                    for (HFHistoryResponse r : responses) {
                        Question question = gson.fromJson(r.getValue(), Question.class);

                        QuestionHistory questionHistory = new QuestionHistory();
                        questionHistory.setTxId(r.getTxId());
                        questionHistory.setValue(question);
                        questionHistory.setTimestamp(r.getTimestamp());
                        questionHistory.setDelete(r.isDelete());

                        questionHistories.add(questionHistory);
                    }

                } else {
                    logger.error("response failed: status=" + response.getStatus() + ", msg=" + payload);
                }

            } else {
                logger.error("response unverified: status=" + response.getStatus() + ", msg=" + payload);
            }
        }

        return questionHistories;
    }

    private List<Question> unpackHFPayload(Collection<ProposalResponse> proposalResponses) throws Exception {
        List<Question> questions = new ArrayList<>();

        for (ProposalResponse response : proposalResponses) {
            String payload = new String(response.getChaincodeActionResponsePayload());

            if (response.isVerified()) {
                if (response.getStatus() == ChaincodeResponse.Status.SUCCESS) {
                    logger.info("response: " + payload);

                    HFResponse[] responses = gson.fromJson(payload, HFResponse[].class);
                    for (HFResponse r : responses) {
                        Question question = gson.fromJson(r.getRecord(), Question.class);
                        questions.add(question);
                    }

                } else {
                    logger.error("Response failed: status=" + response.getStatus() + ", message=" + payload);
                }
            } else {
                logger.error("Response is unverified: status=" + response.getStatus() + ", message=" + payload);
            }
        }
        return questions;
    }

    public Question getQuestionById(UserSession userSession, String id) {
        try {
            ArrayList<String> args = new ArrayList<>();
            args.add(id);

            List<Question> questions = unpackHFPayload(
                    queryChaincode(
                            userSession.getHfClient(),
                            userSession.getChannelExams(),
                            chaincodeQstnId,
                            chaincodeFnMap.get(Question.QuestionFunction.GET_BY_ID),
                            args
                    )
            );

            if (questions.isEmpty()) {
                return null;
            } else {
                return questions.get(0);
            }

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public Question createQuestion(UserSession userSession, Question question) {
        try {
            String questionId = getUUID();
            question.setId(questionId);

            //set defaults
            if (question.getType().isEmpty()) {
                question.setType(Question.QuestionType.MAIN_QUESTION);
            }

            //todo: handle completable future better. Then check if transactionEvent.isValid()
            try {
                BlockEvent.TransactionEvent transactionEvent = invokeChaincode(
                        userSession.getHfClient(),
                        userSession.getChannelExams(),
                        chaincodeQstnId,
                        chaincodeFnMap.get(Question.QuestionFunction.CREATE),
                        getCreateValuesList(question)
                ).get(1, TimeUnit.SECONDS);
            } catch (TimeoutException ex) {
//                return getQuestionById(userSession, questionId);
                question.setCreateDate(System.currentTimeMillis());
                question.setLastUpdate(System.currentTimeMillis());
                question.setDocType("question");
                return question;
            }
            throw new Exception("Unexpected error occurred");

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }
}
