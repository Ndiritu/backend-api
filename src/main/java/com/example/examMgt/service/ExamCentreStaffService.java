package com.example.examMgt.service;

import com.example.examMgt.model.db.ExamCentreStaff;
import com.example.examMgt.utils.SqlUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ExamCentreStaffService {

    private static final Logger logger = Logger.getLogger(ExamCentreStaffService.class);

    @Autowired
    private JdbcTemplateService jdbcTemplateService;

    public ExamCentreStaff authenticate(String email, String password) {
        try {
            Map<String, String> params = new HashMap<>();
            params.put("email", email);
            params.put("password", password);

            String sql = SqlUtils.instance.findSqlStatement("authenticateExamCentreStaff");
            ExamCentreStaffCallbackHandler examCentreStaffCallbackHandler = new ExamCentreStaffCallbackHandler();
            jdbcTemplateService.theACJdbcTemplate().query(sql, params, examCentreStaffCallbackHandler);

            ExamCentreStaff examCentreStaff = examCentreStaffCallbackHandler.getExamCentreStaff();
            if (examCentreStaff == null || examCentreStaff.getId() == 0) {
                return null;
            }
            return examCentreStaff;

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public List<ExamCentreStaff> getExamCentrStaffByIds(List<Long> ids) {
        try {
            Array idsArr = SqlUtils.getPostgresBigintArray(ids, jdbcTemplateService.getDBConnection());
            Map<String, Object> params = new HashMap<>();
            params.put("ids", idsArr);

            String sql = SqlUtils.instance.findSqlStatement("getExamCentreStaffByIds");
            return jdbcTemplateService.theACJdbcTemplate().query(sql, params, new ExamCentreStaffRowMapper());


        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public ExamCentreStaff getExamCentreStaffById(long examCentreStaffId) {
        try {
            Map<String, Long> params = new HashMap<>();
            params.put("examCentreStaffId", examCentreStaffId);

            String sql = SqlUtils.instance.findSqlStatement("getExamCentreStaffById");
            ExamCentreStaffCallbackHandler examCentreStaffCallbackHandler = new ExamCentreStaffCallbackHandler();
            jdbcTemplateService.theACJdbcTemplate().query(sql, params, examCentreStaffCallbackHandler);

            ExamCentreStaff examCentreStaff = examCentreStaffCallbackHandler.getExamCentreStaff();
            if (examCentreStaff.getId() == 0) {
                return null;
            }
            return examCentreStaff;

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }


    private static class ExamCentreStaffRowMapper implements RowMapper<ExamCentreStaff> {
        @Override
        public ExamCentreStaff mapRow(ResultSet rs, int i) throws SQLException {
            long id = rs.getLong("id");
            String firstName = rs.getString("first_name");
            String otherNames = rs.getString("other_names");
            String surname = rs.getString("surname");
            String email = rs.getString("email");
            String password = rs.getString("password");
            boolean active = rs.getBoolean("active");
            Timestamp createDate = rs.getTimestamp("create_date");
            Timestamp lastUpdate = rs.getTimestamp("last_update");

            ExamCentreStaff examCentreStaff = new ExamCentreStaff();
            examCentreStaff.setId(id);
            examCentreStaff.setFirstName(firstName);
            examCentreStaff.setOtherNames(otherNames);
            examCentreStaff.setSurname(surname);
            examCentreStaff.setEmail(email);
            examCentreStaff.setPassword(password);
            examCentreStaff.setActive(active);
            examCentreStaff.setCreateDate(createDate);
            examCentreStaff.setLastUpdate(lastUpdate);
            return examCentreStaff;
        }
    }



    private static class ExamCentreStaffCallbackHandler implements RowCallbackHandler {
        private ExamCentreStaff examCentreStaff;

        public ExamCentreStaffCallbackHandler() {}

        public ExamCentreStaff getExamCentreStaff() {
            return examCentreStaff;
        }

        private void setExamCentreStaff(ExamCentreStaff examCentreStaff) {
            this.examCentreStaff = examCentreStaff;
        }

        @Override
        public void processRow(ResultSet rs) throws SQLException {
            long id = rs.getLong("id");
            String firstName = rs.getString("first_name");
            String otherNames = rs.getString("other_names");
            String surname = rs.getString("surname");
            String email = rs.getString("email");
            String password = rs.getString("password");
            boolean active = rs.getBoolean("active");
            Timestamp createDate = rs.getTimestamp("create_date");
            Timestamp lastUpdate = rs.getTimestamp("last_update");

            ExamCentreStaff examCentreStaff = new ExamCentreStaff();
            examCentreStaff.setId(id);
            examCentreStaff.setFirstName(firstName);
            examCentreStaff.setOtherNames(otherNames);
            examCentreStaff.setSurname(surname);
            examCentreStaff.setEmail(email);
            examCentreStaff.setPassword(password);
            examCentreStaff.setActive(active);
            examCentreStaff.setCreateDate(createDate);
            examCentreStaff.setLastUpdate(lastUpdate);

            setExamCentreStaff(examCentreStaff);
        }
    }
}
