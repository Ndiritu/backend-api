package com.example.examMgt.service;

import com.example.examMgt.model.UserSession;
import com.example.examMgt.model.db.Course;
import com.example.examMgt.model.db.Degree;
import com.example.examMgt.model.hf.ExamItem;
import com.example.examMgt.model.hf.Examination;
import com.example.examMgt.model.hf.Instruction;
import com.example.examMgt.model.hf.Question;
import com.example.examMgt.model.hf.Section;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@Service
public class ExamPrintingService {

    private static final Logger logger = Logger.getLogger(ExamPrintingService.class);

    private @Value("${printedExam.filePath}") String filePath;
    private @Value("${printedExam.universityLogo.filePath}") String uonLogoFilePath;

    private Font universityNameFont = new Font(Font.FontFamily.TIMES_ROMAN, 13, Font.BOLD);
    private Font largeFont = new Font(Font.FontFamily.TIMES_ROMAN, 11, Font.BOLD);
    private Font basicFont = new Font(Font.FontFamily.TIMES_ROMAN, 11);
    private int paragraphLeading = 15;
    private float underlineThickness = 0.1f;
    private float underLineYPosition = -2f;

    @Autowired private ExaminationService examinationService;
    @Autowired private CourseService courseService;
    @Autowired private DegreeService degreeService;

    public boolean printExam(UserSession userSession, Examination exam) {
        try {
            List<Object> examTree = examinationService.buildExamTree(userSession, exam.getId());
            Course course = courseService.getCourseById(exam.getCourseId());
            Degree degree = degreeService.getDegreeById(course.getDegreeId());

            logger.debug("Course=" + course);
            logger.debug("Degree=" + degree);

            String fileName = filePath;
            String docName = "exam-" + exam.getId() + ".pdf";
            fileName += docName;
            logger.debug("FileName=" + fileName);

            File examPdfFile = new File(fileName);
            if (!examPdfFile.exists()) {
                if (examPdfFile.createNewFile()) {
                    logger.debug("Created new file=" + fileName);
                } else {
                    logger.debug("Failed to create new file=" + fileName);
                }
            }

            int margin = 40;
            Document document = new Document(PageSize.A4, margin + 20, margin + 20, margin, margin + 50);
            PdfWriter.getInstance(document, new FileOutputStream(fileName, false));

            document.open();

            Paragraph examHeader = getExamHeader(course, exam);
            document.add(examHeader);

            int mainQuestionIndex = 0;
            int subQuestionIndex = 0;
            boolean isFirstSection = true;

            for (Object item : examTree) {
                ExamItem examItem = (ExamItem) item;

                if (examItem.getDocType().equals("section")) {
                   if (!isFirstSection) {
                       document.newPage();
                   }
                    Section section = (Section) examItem;
                    document.add(getSection(section));
                    if (isFirstSection) {
                        isFirstSection = false;
                    }
                }
                if (examItem.getDocType().equals("instruction")) {
                    document.add(getInstruction(examItem));
                }
                if (examItem.getDocType().equals("question")) {
                    Question question = (Question) examItem;
                    if (Question.QuestionType.MAIN_QUESTION.equals(
                            Question.QuestionType.valueOf(question.getType())
                    )) {
                        document.add(getMainQuestion(question,
                                mainQuestionIndex,
                                Examination.NumberingFormat.valueOf(exam.getPrimaryNumberingFormat()),
                                examTree));
                        mainQuestionIndex += 1;
                        subQuestionIndex = 0;
                    } else {
                        document.add(getSubQuestion(
                                question,
                                subQuestionIndex,
                                Examination.NumberingFormat.valueOf(exam.getSecondaryNumberingFormat())
                        ));
                        subQuestionIndex += 1;
                    }
                }

            }

            document.close();
            return true;

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return false;
        }
    }

    private Paragraph getSection(Section section) throws Exception {
        StringBuilder sectionBuilder = new StringBuilder();
        sectionBuilder.append("SECTION ");
        sectionBuilder.append(section.getName());

        Chunk sectionChunk = new Chunk(sectionBuilder.toString(), largeFont);
        sectionChunk.setUnderline(underlineThickness, underLineYPosition);

        Paragraph paragraph = new Paragraph(paragraphLeading);
        paragraph.setAlignment(Element.ALIGN_CENTER);

        paragraph.add(Chunk.NEWLINE);
        paragraph.add(sectionChunk);
        paragraph.add(Chunk.NEWLINE);
        paragraph.add(Chunk.NEWLINE);

        return paragraph;

    }

    private String getQuestionNumber(int index, Examination.NumberingFormat numberingFormat) {
        if (Examination.NumberingFormat.NUMERICAL.equals(numberingFormat)) {
            List<String> numbers = new ArrayList<>();
            for (int i = 1; i <= 100; i ++) {
                numbers.add(String.valueOf(i));
            }
            return numbers.get(index);
        }
        if (Examination.NumberingFormat.ALPHABETICAL.equals(numberingFormat)) {
            String alphabet = "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z";
            List<String> letters = Arrays.asList(alphabet.split(","));
            return letters.get(index);
        }
        if (Examination.NumberingFormat.ROMAN_NUMERALS.equals(numberingFormat)) {
            String numerals = "i,ii,iii,iv,v,vi,vii,viii,ix,x,xi,xii,xiii,xiv,xv,xvi,xvii,xviii,xix,xx";
            List<String> options = Arrays.asList(numerals.split(","));
            return options.get(index);
        }
        return "";
    }

    private Paragraph getSubQuestion(Question subQuestion,
                                     int questionIndex,
                                     Examination.NumberingFormat secNumFormat) throws Exception {
        String questionNumber = getQuestionNumber(questionIndex, secNumFormat);

        StringBuilder questionBuilder = new StringBuilder();
        questionBuilder.append(questionNumber);
        questionBuilder.append(") ");
        questionBuilder.append(subQuestion.getText());
        questionBuilder.append("  [");
        questionBuilder.append(String.valueOf(subQuestion.getMarks()));
        questionBuilder.append(" marks]");

        Chunk question = new Chunk(questionBuilder.toString(), basicFont);
        Paragraph paragraph = new Paragraph(paragraphLeading);
        paragraph.add(question);
        paragraph.add(Chunk.NEWLINE);
        paragraph.add(Chunk.NEWLINE);
        return paragraph;
    }

    private Paragraph getMainQuestion(Question mainQuestion,
                                      int mainQuestionIndex,
                                      Examination.NumberingFormat primNumFormat,
                                      List<Object> examTree) throws Exception {
        logger.debug("Entered getMainQuestion() primNumFormat=" + primNumFormat);
        String questionNumber = getQuestionNumber(mainQuestionIndex, primNumFormat);
        if (!primNumFormat.equals(Examination.NumberingFormat.NUMERICAL)) {
            logger.debug("pri num format is not numerical. Question number capitalised");
            questionNumber = questionNumber.toUpperCase();
            logger.debug("question number=" + questionNumber);
        }

        int marks = 0;
        for (Object item : examTree) {
            ExamItem examItem = (ExamItem) item;
            if (examItem.getDisplayIndex() > mainQuestion.getDisplayIndex()) {
                if (examItem.getDocType().equals("question")) {
                    Question question = (Question) examItem;
                    if (Question.QuestionType.MAIN_QUESTION.equals(
                            Question.QuestionType.valueOf(question.getType())
                    )) {
                        break;
                    } else {
                        marks += question.getMarks();
                    }
                }
                if (examItem.getDocType().equals("section")) {
                    break;
                }
                if (examItem.getDocType().equals("instruction")) {
                    Instruction instruction = (Instruction) examItem;
                    if (Instruction.InstructionType.SECTION_LEVEL.equals(
                            Instruction.InstructionType.valueOf(instruction.getType())
                    )) {
                        break;
                    }
                    if (Instruction.InstructionType.EXAM_LEVEL.equals(
                            Instruction.InstructionType.valueOf(instruction.getType())
                    )) {
                        break;
                    }
                }
            }
        }

        StringBuilder questionBuilder = new StringBuilder();
        questionBuilder.append("Question ");
        questionBuilder.append(questionNumber);
        questionBuilder.append(" (");
        questionBuilder.append(String.valueOf(marks));
        questionBuilder.append(" Marks)");

        Chunk question = new Chunk(questionBuilder.toString(), largeFont);
        Paragraph paragraph = new Paragraph(paragraphLeading);
        paragraph.add(question);
        paragraph.add(Chunk.NEWLINE);
        paragraph.add(Chunk.NEWLINE);
        return paragraph;
    }

    private Paragraph getInstruction(ExamItem examItem) throws Exception {
        Instruction instruction = (Instruction) examItem;
        Chunk text = new Chunk(instruction.getText(), largeFont);
        Paragraph paragraph = new Paragraph(paragraphLeading);
        paragraph.add(text);
        paragraph.add(Chunk.NEWLINE);
        if (Instruction.InstructionType.EXAM_LEVEL.equals(
                Instruction.InstructionType.valueOf(instruction.getType())
        )) {
            paragraph.add(Chunk.NEWLINE);
        }
        return paragraph;
    }

    private Paragraph getExamHeader(Course course, Examination exam) throws Exception {


        //University logo
        Image image = Image.getInstance(uonLogoFilePath);
        image.setAlignment(Element.ALIGN_CENTER);

        //University name
        Chunk universityName = new Chunk("UNIVERSITY OF NAIROBI", universityNameFont);

        //Semester string
        String semester = "";
        if (Course.Semester.FIRST_SEMESTER == course.getSemester()) {
            semester ="FIRST SEMESTER";
        }
        if (Course.Semester.SECOND_SEMESTER == course.getSemester()) {
            semester = "SECOND SEMESTER";
        }
        StringBuilder semesterLine = new StringBuilder();
        semesterLine.append(semester);
        semesterLine.append(" EXAMINATIONS 2017/2018");

        Chunk semLine = new Chunk(semesterLine.toString(), largeFont);
        semLine.setUnderline(underlineThickness, underLineYPosition);

        //Year string
        String year = "";
        HashMap<Integer, String> yearOfStudyMap = new HashMap<>();
        yearOfStudyMap.put(1, "FIRST");
        yearOfStudyMap.put(2, "SECOND");
        yearOfStudyMap.put(3, "THIRD");
        yearOfStudyMap.put(4, "FOURTH");
        year = yearOfStudyMap.get(course.getYearOfStudy());

        StringBuilder yearBuilder = new StringBuilder();
        yearBuilder.append(year);
        yearBuilder.append(" YEAR EXAMINATIONS FOR THE DEGREE OF BACHELOR OF SCIENCE IN COMPUTER SCIENCE");

        Chunk yearLine = new Chunk(yearBuilder.toString(), largeFont);
        yearLine.setUnderline(underlineThickness, underLineYPosition);

        List<String> courseCodeStr = Arrays.asList(course.getCourseCode().toString().split("_"));
        StringBuilder courseBuilder = new StringBuilder();
        courseBuilder.append(courseCodeStr.get(0));
        courseBuilder.append(" " + courseCodeStr.get(1) + ": ");
        courseBuilder.append(course.getName());

        Chunk courseLine = new Chunk(courseBuilder.toString(), largeFont);
        courseLine.setUnderline(underlineThickness, underLineYPosition);

        String examDate = new SimpleDateFormat("MMMM d, y").format(new Timestamp(exam.getExamDate()));
        String startTime = new SimpleDateFormat("hh:mm").format(new Timestamp(exam.getStartTime()));
        String endTime = new SimpleDateFormat("hh:mm a").format(
                new Timestamp(exam.getStartTime() + exam.getDuration())
        );

        StringBuilder examDateBuilder = new StringBuilder();
        examDateBuilder.append("DATE: ");
        examDateBuilder.append(examDate);
        Chunk examDateLine = new Chunk(examDateBuilder.toString(), largeFont);

        StringBuilder examTimeBuilder = new StringBuilder();
        examTimeBuilder.append("TIME: " );
        examTimeBuilder.append(startTime);
        examTimeBuilder.append("-");
        examTimeBuilder.append(endTime);

        Chunk examTimeLine = new Chunk(examTimeBuilder.toString(), largeFont);

        Paragraph examHeader = new Paragraph(paragraphLeading);
        examHeader.setAlignment(Element.ALIGN_CENTER);
        examHeader.setTabSettings(new TabSettings(56f));

        examHeader.add(image);
        examHeader.add(universityName);
        examHeader.add(Chunk.NEWLINE);
        examHeader.add(Chunk.NEWLINE);
        examHeader.add(semLine);
        examHeader.add(Chunk.NEWLINE);
        examHeader.add(yearLine);
        examHeader.add(Chunk.NEWLINE);
        examHeader.add(Chunk.NEWLINE);
        examHeader.add(courseLine);
        examHeader.add(Chunk.NEWLINE);
        examHeader.add(Chunk.NEWLINE);
        examHeader.add(examDateLine);
        examHeader.add(Chunk.TABBING);
        examHeader.add(Chunk.TABBING);
        examHeader.add(Chunk.TABBING);
        examHeader.add(Chunk.TABBING);
        examHeader.add(examTimeLine);
        examHeader.add(Chunk.NEWLINE);
        examHeader.add(Chunk.NEWLINE);

        return examHeader;
    }



}
