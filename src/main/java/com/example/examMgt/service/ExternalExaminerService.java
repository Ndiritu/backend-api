package com.example.examMgt.service;

import com.example.examMgt.model.db.ExternalExaminer;
import com.example.examMgt.utils.SqlUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ExternalExaminerService {

    private static final Logger logger = Logger.getLogger(ExternalExaminerService.class);

    @Autowired
    public JdbcTemplateService jdbcTemplateService;

    public ExternalExaminer authenticate(String email, String password) {
        try {
            Map<String, String> params = new HashMap<>();
            params.put("email", email);
            params.put("password", password);

            String sql = SqlUtils.instance.findSqlStatement("authenticate");
            ExtExaminerCallbackHandler extExaminerCallbackHandler = new ExtExaminerCallbackHandler();
            jdbcTemplateService.theACJdbcTemplate().query(sql, params, extExaminerCallbackHandler);

            ExternalExaminer externalExaminer = extExaminerCallbackHandler.getExternalExaminer();
            if (externalExaminer == null || externalExaminer.getId() == 0) {
                return null;
            }
            return externalExaminer;

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public ExternalExaminer getExtExaminerModeratingCourse(long courseId) {
        try {
            Map<String, Long> params = new HashMap<>();
            params.put("courseId", courseId);

            String sql = SqlUtils.instance.findSqlStatement("getExtExaminerModeratingCourse");
            ExtExaminerCallbackHandler extExaminerCallbackHandler = new ExtExaminerCallbackHandler();
            jdbcTemplateService.theACJdbcTemplate().query(sql, params, extExaminerCallbackHandler);

            ExternalExaminer externalExaminer = extExaminerCallbackHandler.getExternalExaminer();
            if (externalExaminer.getId() == 0) {
                return null;
            }
            return externalExaminer;

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public List<ExternalExaminer> getExtExaminersByIds(List<Long> ids) {
        try {
            Array extExaminerIdArr = SqlUtils.getPostgresBigintArray(ids, jdbcTemplateService.getDBConnection());
            HashMap<String, Object> params = new HashMap<>();
            params.put("ids", extExaminerIdArr);

            String sql = SqlUtils.instance.findSqlStatement("getExtExaminersByIds");
            return jdbcTemplateService.theACJdbcTemplate().query(sql, params, new ExtExaminerRowMapper());

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return new ArrayList<>();
        }
    }

    public ExternalExaminer getExtExaminerByEmail(String email) {
        try {
            Map<String, String> params = new HashMap<>();
            params.put("email", email);

            String sql = SqlUtils.instance.findSqlStatement("getExtExaminerByEmail");
            ExtExaminerCallbackHandler callbackHandler = new ExtExaminerCallbackHandler();
            jdbcTemplateService.theACJdbcTemplate().query(sql, params, callbackHandler);

            ExternalExaminer externalExaminer = callbackHandler.getExternalExaminer();
            if (externalExaminer.getId() == 0) {
                return null;
            }
            return externalExaminer;

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public ExternalExaminer getExternalExaminerById(long id) {
        try {
            Map<String, Long> params = new HashMap<>();
            params.put("id", id);

            String sql = SqlUtils.instance.findSqlStatement("getExtExaminerById");
            ExtExaminerCallbackHandler callbackHandler = new ExtExaminerCallbackHandler();
            jdbcTemplateService.theACJdbcTemplate().query(sql, params, callbackHandler);

            ExternalExaminer externalExaminer = callbackHandler.getExternalExaminer();
            if (externalExaminer.getId() == 0) {
                return null;
            }
            return externalExaminer;

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    private static class ExtExaminerRowMapper implements RowMapper<ExternalExaminer> {
        @Override
        public ExternalExaminer mapRow(ResultSet rs, int i) throws SQLException {
            long id = rs.getLong("id");
            String firstName = rs.getString("first_name");
            String otherNames = rs.getString("other_names");
            String surname = rs.getString("surname");
            String email = rs.getString("email");
            String password = rs.getString("password");
            boolean active = rs.getBoolean("active");
            boolean approved = rs.getBoolean("approved");
            long approvedByChairpersonId = rs.getLong("approved_by_chairperson_id");
            Timestamp createDate = rs.getTimestamp("create_date");
            Timestamp lastUpdate = rs.getTimestamp("last_update");

            ExternalExaminer externalExaminer = new ExternalExaminer();
            externalExaminer.setId(id);
            externalExaminer.setFirstName(firstName);
            externalExaminer.setOtherNames(otherNames);
            externalExaminer.setSurname(surname);
            externalExaminer.setEmail(email);
            externalExaminer.setPassword(password);
            externalExaminer.setActive(active);
            externalExaminer.setApproved(approved);
            externalExaminer.setApprovedByChairpersonId(approvedByChairpersonId);
            externalExaminer.setCreateDate(createDate);
            externalExaminer.setLastUpdate(lastUpdate);
            return externalExaminer;
        }
    }

    private static class ExtExaminerCallbackHandler implements RowCallbackHandler {
        private ExternalExaminer externalExaminer;

        private ExtExaminerCallbackHandler() {}

        private ExternalExaminer getExternalExaminer() {
            return externalExaminer;
        }

        private void setExternalExaminer(ExternalExaminer externalExaminer) {
            this.externalExaminer = externalExaminer;
        }

        public void processRow(ResultSet rs) throws SQLException {
            long id = rs.getLong("id");
            String firstName = rs.getString("first_name");
            String otherNames = rs.getString("other_names");
            String surname = rs.getString("surname");
            String email = rs.getString("email");
            String password = rs.getString("password");
            boolean active = rs.getBoolean("active");
            boolean approved = rs.getBoolean("approved");
            long approvedByChairpersonId = rs.getLong("approved_by_chairperson_id");
            Timestamp createDate = rs.getTimestamp("create_date");
            Timestamp lastUpdate = rs.getTimestamp("last_update");

            ExternalExaminer externalExaminer = new ExternalExaminer();
            externalExaminer.setId(id);
            externalExaminer.setFirstName(firstName);
            externalExaminer.setOtherNames(otherNames);
            externalExaminer.setSurname(surname);
            externalExaminer.setEmail(email);
            externalExaminer.setPassword(password);
            externalExaminer.setActive(active);
            externalExaminer.setApproved(approved);
            externalExaminer.setApprovedByChairpersonId(approvedByChairpersonId);
            externalExaminer.setCreateDate(createDate);
            externalExaminer.setLastUpdate(lastUpdate);

            setExternalExaminer(externalExaminer);
        }
    }


}
