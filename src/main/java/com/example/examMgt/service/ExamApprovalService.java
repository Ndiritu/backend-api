package com.example.examMgt.service;

import com.example.examMgt.model.HFResponse;
import com.example.examMgt.model.UserSession;
import com.example.examMgt.model.hf.ExamApproval;
import com.example.examMgt.model.hf.history.ExamApprovalHistory;
import com.example.examMgt.model.hf.history.HFHistoryResponse;
import com.example.examMgt.model.viewModel.ChairpersonView;
import com.example.examMgt.model.viewModel.ExamApprovalView;
import org.apache.log4j.Logger;
import org.hyperledger.fabric.sdk.BlockEvent;
import org.hyperledger.fabric.sdk.ChaincodeResponse;
import org.hyperledger.fabric.sdk.ProposalResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@Service
public class ExamApprovalService extends BaseService {

    private static final Logger logger = Logger.getLogger(ExamApprovalService.class);

    private @Value("${hf.chaincode.examApproval.chaincodeId}") String chaincodeId;
    private EnumMap<ExamApproval.ExamApprovalFn, String> chaincodeFnMap = new EnumMap<>(ExamApproval.ExamApprovalFn.class);

    private @Value("${hf.chaincode.examApproval.create}") String create;
    private @Value("${hf.chaincode.examApproval.update}") String update;
    private @Value("${hf.chaincode.examApproval.getExamsByStatus}") String getExamsByStatus;
    private @Value("${hf.chaincode.examApproval.getByExamId}") String getByExamId;
    private @Value("${hf.chaincode.examApproval.getHistoryByExamId}") String getHistoryByExamId;
    private @Value("${hf.chaincode.examApproval.getAll}") String getAll;

    @Autowired private ChairpersonService chairpersonService;

    @PostConstruct
    public void init() {
        initChaincodeFnMap();
    }

    private void initChaincodeFnMap() {
        chaincodeFnMap.put(ExamApproval.ExamApprovalFn.CREATE, create);
        chaincodeFnMap.put(ExamApproval.ExamApprovalFn.UPDATE, update);
        chaincodeFnMap.put(ExamApproval.ExamApprovalFn.GET_EXAMS_BY_STATUS, getExamsByStatus);
        chaincodeFnMap.put(ExamApproval.ExamApprovalFn.GET_BY_EXAM_ID, getByExamId);
        chaincodeFnMap.put(ExamApproval.ExamApprovalFn.GET_HISTORY_BY_EXAM_ID, getHistoryByExamId);
        chaincodeFnMap.put(ExamApproval.ExamApprovalFn.GET_ALL, getAll);
    }

    public List<ExamApprovalView> convertApprovalsToApprovalViews(List<ExamApproval> approvals) {
        logger.debug("Executing convertApprovalsToApprovalViews()...received=" + approvals);
        if (approvals.isEmpty()) {
            return new ArrayList<>();
        }

        HashSet<Long> chairpersonIds = new HashSet<>();

        Long chairpersonId;
        for (ExamApproval approval : approvals) {
            chairpersonId = approval.getChairpersonId();
            if (!chairpersonIds.contains(chairpersonId)) {
                chairpersonIds.add(chairpersonId);
            }
        }

        List<ChairpersonView> chairpersons = chairpersonService.getChairpersonsByIds(
                new ArrayList<>(chairpersonIds)
        );

        HashMap<Long, ChairpersonView> chairpersonMap = new HashMap<>();
        for (ChairpersonView chairperson : chairpersons) {
            chairpersonMap.put(chairperson.getId(), chairperson);
        }

        List<ExamApprovalView> approvalViews = new ArrayList<>();

        String chairpersonFirstName;
        String chairpersonSurname;
        for (ExamApproval approval : approvals) {
            chairpersonFirstName = chairpersonMap.get(approval.getChairpersonId()).getFirstName();
            chairpersonSurname = chairpersonMap.get(approval.getChairpersonId()).getSurname();

            ExamApprovalView approvalView = new ExamApprovalView();
            approvalView.setExamId(approval.getExamId());
            approvalView.setChairpersonId(approval.getChairpersonId());
            approvalView.setChairpersonFirstName(chairpersonFirstName);
            approvalView.setChairpersonSurname(chairpersonSurname);
            approvalView.setStatus(approval.isStatus());
            approvalView.setCreateDate(approval.getCreateDate());
            approvalView.setLastUpdate(approval.getLastUpdate());

            approvalViews.add(approvalView);
        }

        return approvalViews;
    }

    public List<ExamApproval> getAllExamApprovals(UserSession userSession) {
        try {
            return unpackHFPayload(
                    queryChaincode(
                            userSession.getHfClient(),
                            userSession.getChannelExams(),
                            chaincodeId,
                            chaincodeFnMap.get(ExamApproval.ExamApprovalFn.GET_ALL),
                            new ArrayList<>()
                    )
            );

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public List<ExamApprovalHistory> getApprovalHistoryByExamId(UserSession userSession, String examId) {
        try {
            ArrayList<String> args = new ArrayList<>();
            args.add(examId);

            return unpackHFHistorypayload(
                    queryChaincode(
                            userSession.getHfClient(),
                            userSession.getChannelExams(),
                            chaincodeId,
                            chaincodeFnMap.get(ExamApproval.ExamApprovalFn.GET_HISTORY_BY_EXAM_ID),
                            args
                    )
            );

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public ExamApproval getApprovalByExamId(UserSession userSession, String examId) {
        try {
            ArrayList<String> args = new ArrayList<>();
            args.add(examId);

            List<ExamApproval> examApprovals = unpackHFPayload(
                    queryChaincode(
                            userSession.getHfClient(),
                            userSession.getChannelExams(),
                            chaincodeId,
                            chaincodeFnMap.get(ExamApproval.ExamApprovalFn.GET_BY_EXAM_ID),
                            args
                    )
            );

            if (examApprovals.isEmpty()) {
                return null;
            }

            return examApprovals.get(0);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public List<ExamApproval> getExamsByApprovalStatus(UserSession userSession, boolean status) {
        try {
            ArrayList<String> args = new ArrayList<>();
            args.add(String.valueOf(userSession.getId()));
            args.add(String.valueOf(status));

            return unpackHFPayload(
                    queryChaincode(
                            userSession.getHfClient(),
                            userSession.getChannelExams(),
                            chaincodeId,
                            chaincodeFnMap.get(ExamApproval.ExamApprovalFn.GET_EXAMS_BY_STATUS),
                            args
                    )
            );

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public boolean updateExamApproval(UserSession userSession, ExamApproval examApproval) {
        try {
            //todo: handle completable future better
            try {
                BlockEvent.TransactionEvent transactionEvent = invokeChaincode(
                        userSession.getHfClient(),
                        userSession.getChannelExams(),
                        chaincodeId,
                        chaincodeFnMap.get(ExamApproval.ExamApprovalFn.UPDATE),
                        getUpdateArgsList(examApproval)
                ).get(1, TimeUnit.SECONDS);
            } catch (TimeoutException ex) {
                //todo: find way of returning inserted object?
                return true;
            }
            throw new Exception("Unexpected error occurred");


        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return false;
        }
    }

    public ExamApproval createExamApproval(UserSession userSession, ExamApproval examApproval) {
        try {
            //todo: handle completable future better
            try {
                BlockEvent.TransactionEvent transactionEvent = invokeChaincode(
                        userSession.getHfClient(),
                        userSession.getChannelExams(),
                        chaincodeId,
                        chaincodeFnMap.get(ExamApproval.ExamApprovalFn.CREATE),
                        getCreateArgsList(examApproval)
                ).get(1, TimeUnit.SECONDS);
            } catch (TimeoutException ex) {
                //todo: find way of returning inserted object?
                examApproval.setCreateDate(System.currentTimeMillis());
                examApproval.setLastUpdate(System.currentTimeMillis());
                return examApproval;
            }
            throw new Exception("Unexpected error occurred");

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    private List<ExamApprovalHistory> unpackHFHistorypayload(Collection<ProposalResponse> proposalResponses) throws Exception {
        List<ExamApprovalHistory> examApprovalHistories = new ArrayList<>();

        String payload;
        for (ProposalResponse response : proposalResponses) {
            payload = new String(response.getChaincodeActionResponsePayload());
            logger.info("response: " + payload);

            if (response.isVerified()) {
                if (response.getStatus() == ChaincodeResponse.Status.SUCCESS) {
                    HFHistoryResponse[] responses = gson.fromJson(payload, HFHistoryResponse[].class);

                    for (HFHistoryResponse r : responses) {
                        ExamApproval examApproval = gson.fromJson(r.getValue(), ExamApproval.class);

                        ExamApprovalHistory examApprovalHistory = new ExamApprovalHistory();
                        examApprovalHistory.setTxId(r.getTxId());
                        examApprovalHistory.setValue(examApproval);
                        examApprovalHistory.setTimestamp(r.getTimestamp());
                        examApprovalHistory.setDelete(r.isDelete());
                        examApprovalHistories.add(examApprovalHistory);
                    }

                } else {
                    logger.error("response failed: status=" + response.getStatus() + ", msg=" + payload);
                }

            } else {
                logger.error("response unverified: status=" + response.getStatus() + ", msg=" + payload);
            }

        }

        return examApprovalHistories;
    }

    private List<ExamApproval> unpackHFPayload(Collection<ProposalResponse> proposalResponses) throws Exception {
        List<ExamApproval> examApprovals = new ArrayList<>();

        String payload;
        for (ProposalResponse response : proposalResponses) {
            payload = new String(response.getChaincodeActionResponsePayload());
            logger.info("response: " + payload);

            if (response.isVerified()) {
                if (response.getStatus() == ChaincodeResponse.Status.SUCCESS) {
                    HFResponse[] responses = gson.fromJson(payload, HFResponse[].class);

                    for (HFResponse r : responses) {
                        ExamApproval examApproval = gson.fromJson(r.getRecord(), ExamApproval.class);
                        examApprovals.add(examApproval);
                    }

                } else {
                    logger.error("Response failed: status=" + response.getStatus() + ", msg=" + payload);
                }

            } else {
                logger.error("Response unverified: status=" + response.getStatus() + ", msg=" + payload);
            }

        }

        return examApprovals;
    }

    private ArrayList<String> getUpdateArgsList(ExamApproval examApproval) {
        ArrayList<String> args = new ArrayList<>();
        args.add(examApproval.getExamId());
        args.add(String.valueOf(examApproval.getChairpersonId()));
        args.add(String.valueOf(examApproval.isStatus()));
        return args;
    }

    private ArrayList<String> getCreateArgsList(ExamApproval examApproval) {
        ArrayList<String> args = new ArrayList<>();
        args.add(examApproval.getExamId());
        args.add(String.valueOf(examApproval.getChairpersonId()));
        args.add(String.valueOf(examApproval.isStatus()));
        return args;
    }

}

