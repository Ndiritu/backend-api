package com.example.examMgt.service;

import com.example.examMgt.model.HFResponse;
import com.example.examMgt.model.UserSession;
import com.example.examMgt.model.db.ExternalExaminer;
import com.example.examMgt.model.hf.ItemModeration;
import com.example.examMgt.model.hf.history.HFHistoryResponse;
import com.example.examMgt.model.hf.history.ItemModerationHistory;
import com.example.examMgt.model.viewModel.ItemModerationView;
import org.apache.log4j.Logger;
import org.hyperledger.fabric.sdk.BlockEvent;
import org.hyperledger.fabric.sdk.ChaincodeResponse;
import org.hyperledger.fabric.sdk.ProposalResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@Service
public class ItemModerationService extends BaseService {

    private static final Logger logger = Logger.getLogger(ItemModerationService.class);

    private EnumMap<ItemModeration.ItemModFunction, String> chaincodeFnMap = new EnumMap<>(ItemModeration.ItemModFunction.class);

    private @Value("${hf.chaincode.itemModeration.chaincodeId}") String chaincodeId;
    private @Value("${hf.chaincode.itemModeration.create}") String create;
    private @Value("${hf.chaincode.itemModeration.update}") String update;
    private @Value("${hf.chaincode.itemModeration.getByItem}") String getByItem;
    private @Value("${hf.chaincode.itemModeration.getByExtExaminer}") String getByExtExaminer;
    private @Value("${hf.chaincode.itemModeration.getHistoryByItem}") String getHistoryByItem;
    private @Value("${hf.chaincode.itemModeration.getAll}") String getAll;

    @Autowired
    private ExternalExaminerService externalExaminerService;

    @PostConstruct
    public void init() {
        initChaincodeFnMap();
    }

    private void initChaincodeFnMap() {
        chaincodeFnMap.put(ItemModeration.ItemModFunction.CREATE, create);
        chaincodeFnMap.put(ItemModeration.ItemModFunction.UPDATE, update);
        chaincodeFnMap.put(ItemModeration.ItemModFunction.GET_BY_ITEM, getByItem);
        chaincodeFnMap.put(ItemModeration.ItemModFunction.GET_BY_EXT_EXAMINER, getByExtExaminer);
        chaincodeFnMap.put(ItemModeration.ItemModFunction.GET_HISTORY_BY_ITEM, getHistoryByItem);
        chaincodeFnMap.put(ItemModeration.ItemModFunction.GET_ALL, getAll);
    }

    public List<ItemModerationView> convertToItemModerationView(List<ItemModeration> itemModerations) {
        logger.debug("Executing convertToItemModerationView() itemModerations=" + itemModerations);
        if (itemModerations.isEmpty()) {
            return new ArrayList<>();
        }

        HashSet<Long> extExaminerIds = new HashSet<>();
        Long extExaminerId;
        for (ItemModeration itemModeration : itemModerations) {
            extExaminerId = itemModeration.getExtExaminerId();
            if (!extExaminerIds.contains(extExaminerId)) {
                extExaminerIds.add(extExaminerId);
            }
        }

        List<ExternalExaminer> externalExaminers = externalExaminerService.getExtExaminersByIds(
                new ArrayList<>(extExaminerIds)
        );

        HashMap<Long, ExternalExaminer> extExaminerMap = new HashMap<>();
        for (ExternalExaminer externalExaminer : externalExaminers) {
            extExaminerMap.put(externalExaminer.getId(), externalExaminer);
        }

        List<ItemModerationView> moderationViews = new ArrayList<>();
        String extExaminerFirstName;
        String extExaminerSurname;
        for (ItemModeration itemModeration : itemModerations) {
            extExaminerFirstName = extExaminerMap.get(itemModeration.getExtExaminerId()).getFirstName();
            extExaminerSurname = extExaminerMap.get(itemModeration.getExtExaminerId()).getSurname();

            ItemModerationView itemModerationView = new ItemModerationView();
            itemModerationView.setItemType(ItemModeration.ItemType.valueOf(
                    itemModeration.getItemType())
            ) ;
            itemModerationView.setItemId(itemModeration.getItemId());
            itemModerationView.setExtExaminerId(itemModeration.getExtExaminerId());
            itemModerationView.setExtExaminerFirstName(extExaminerFirstName);
            itemModerationView.setExtExaminerSurname(extExaminerSurname);
            itemModerationView.setStatus(itemModeration.isStatus());
            itemModerationView.setCreateDate(itemModeration.getCreateDate());
            itemModerationView.setLastUpdate(itemModeration.getLastUpdate());

            moderationViews.add(itemModerationView);
        }

        return moderationViews;

    }

    public List<ItemModeration> getAllModerations(UserSession userSession) {
        try {
            return unpackHFPayload(
                    queryChaincode(
                            userSession.getHfClient(),
                            userSession.getChannelExams(),
                            chaincodeId,
                            chaincodeFnMap.get(ItemModeration.ItemModFunction.GET_ALL),
                            new ArrayList<>()
                    )
            );

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public List<ItemModerationHistory> getModerationHistoryByItem(UserSession userSession,
                                                                   String itemId) {
        try {
            ArrayList<String> args = new ArrayList<>();
            args.add(itemId);

            return unpackHFHistoryPayload(
                    queryChaincode(
                            userSession.getHfClient(),
                            userSession.getChannelExams(),
                            chaincodeId,
                            chaincodeFnMap.get(ItemModeration.ItemModFunction.GET_HISTORY_BY_ITEM),
                            args
                    )
            );

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public List<ItemModeration> getModerationsByExtExaminerId(UserSession userSession) {
        try {
            ArrayList<String> args = new ArrayList<>();
            args.add(String.valueOf(userSession.getId()));

            return unpackHFPayload(
                    queryChaincode(
                            userSession.getHfClient(),
                            userSession.getChannelExams(),
                            chaincodeId,
                            chaincodeFnMap.get(ItemModeration.ItemModFunction.GET_BY_EXT_EXAMINER),
                            args
                    )
            );

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public ItemModeration getModerationByItem(UserSession userSession, ItemModeration.ItemType itemType, String itemId) {
        try {
            ArrayList<String> args = new ArrayList<>();
            args.add(itemType.toString());
            args.add(itemId);

            List<ItemModeration> itemModerations = unpackHFPayload(
                    queryChaincode(
                            userSession.getHfClient(),
                            userSession.getChannelExams(),
                            chaincodeId,
                            chaincodeFnMap.get(ItemModeration.ItemModFunction.GET_BY_ITEM),
                            args
                    )
            );

            if (itemModerations.isEmpty()) {
                return null;
            }

            return itemModerations.get(0);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public boolean updateItemModeration(UserSession userSession, ItemModeration updatedItemModeration) {
        try {
            //todo: handle completable future better
            try {
                BlockEvent.TransactionEvent transactionEvent = invokeChaincode(
                        userSession.getHfClient(),
                        userSession.getChannelExams(),
                        chaincodeId,
                        chaincodeFnMap.get(ItemModeration.ItemModFunction.UPDATE),
                        getUpdateArgsList(userSession.getId(), updatedItemModeration)
                ).get(1, TimeUnit.SECONDS);
            } catch (TimeoutException ex) {
                //todo: find way of returning inserted object?
                return true;
            }
            throw new Exception("Unexpected error occurred");

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return false;
        }
    }

    public ItemModeration createItemModeration(UserSession userSession, ItemModeration itemModeration) {
        try {
            //todo: handle completable future better
            try {
                BlockEvent.TransactionEvent transactionEvent = invokeChaincode(
                        userSession.getHfClient(),
                        userSession.getChannelExams(),
                        chaincodeId,
                        chaincodeFnMap.get(ItemModeration.ItemModFunction.CREATE),
                        getCreateArgsList(itemModeration)
                ).get(1, TimeUnit.SECONDS);
            } catch (TimeoutException ex) {
                //todo: find way of returning inserted object?
                return itemModeration;
            }
            throw new Exception("Unexpected error occurred");

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    private List<ItemModerationHistory> unpackHFHistoryPayload(Collection<ProposalResponse> proposalResponses) throws Exception {
        List<ItemModerationHistory> itemModerationHistories = new ArrayList<>();

        String payload;
        for (ProposalResponse response : proposalResponses) {
            payload = new String(response.getChaincodeActionResponsePayload());

            if (response.isVerified()) {
                if (response.getStatus() == ChaincodeResponse.Status.SUCCESS) {
                    HFHistoryResponse[] responses = gson.fromJson(payload, HFHistoryResponse[].class);

                    for (HFHistoryResponse r : responses) {
                        ItemModeration itemModeration = gson.fromJson(r.getValue(), ItemModeration.class);

                        ItemModerationHistory itemModerationHistory = new ItemModerationHistory();
                        itemModerationHistory.setTxId(r.getTxId());
                        itemModerationHistory.setValue(itemModeration);
                        itemModerationHistory.setTimestamp(r.getTimestamp());
                        itemModerationHistory.setDelete(r.isDelete());

                        itemModerationHistories.add(itemModerationHistory);
                    }

                } else {
                    logger.error("response failed; status=" + response.getStatus() + ", mgs=" + payload);
                }
            } else {
                logger.error("response unverified: status=" + response.getStatus() + ", msg=" + payload);
            }
        }

        return itemModerationHistories;
    }

    private List<ItemModeration> unpackHFPayload(Collection<ProposalResponse> proposalResponses) throws Exception {
        List<ItemModeration> itemModerations = new ArrayList<>();

        String payload;
        for (ProposalResponse response : proposalResponses) {
            payload = new String(response.getChaincodeActionResponsePayload());
            logger.info("response: " + payload);

            if (response.isVerified()) {
                if (response.getStatus() == ChaincodeResponse.Status.SUCCESS) {
                    HFResponse[] responses = gson.fromJson(payload, HFResponse[].class);

                    for (HFResponse r : responses) {
                        ItemModeration itemModeration = gson.fromJson(r.getRecord(), ItemModeration.class);
                        itemModerations.add(itemModeration);
                    }
                } else {
                    logger.error("response failed: status=" + response.getStatus() + ", msg=" + payload);
                }

            } else {
                logger.error("response unverified: status=" + response.getStatus() + ", msg=" + payload);
            }
        }

        return itemModerations;
    }

    private ArrayList<String> getUpdateArgsList(long extExaminerId, ItemModeration itemModeration) {
        ArrayList<String> args = new ArrayList<>();
        args.add(String.valueOf(extExaminerId));
        args.add(itemModeration.getItemId());
        args.add(String.valueOf(itemModeration.isStatus()));
        return args;
    }

    private ArrayList<String> getCreateArgsList(ItemModeration itemModeration) {
        ArrayList<String> args = new ArrayList<>();
        args.add(itemModeration.getItemType());
        args.add(itemModeration.getItemId());
        args.add(String.valueOf(itemModeration.getExtExaminerId()));
        args.add(String.valueOf(itemModeration.isStatus()));
        return args;
    }

}
