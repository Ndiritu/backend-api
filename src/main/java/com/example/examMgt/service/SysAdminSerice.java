package com.example.examMgt.service;

import com.example.examMgt.model.db.SysAdmin;
import com.example.examMgt.utils.SqlUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

@Service
public class SysAdminSerice {

    private static final Logger logger = Logger.getLogger(SysAdminSerice.class);

    @Autowired
    private JdbcTemplateService jdbcTemplateService;

    public SysAdmin getSysAdminByEmail(String email) {
        try {
            Map<String, String> params = new HashMap<>();
            params.put("email", email);

            String sql = SqlUtils.instance.findSqlStatement("getSysAdminByEmail");
            SysAdminCallbackHandler sysAdminCallbackHandler = new SysAdminCallbackHandler();
            jdbcTemplateService.theACJdbcTemplate().query(sql, params, sysAdminCallbackHandler);

            return sysAdminCallbackHandler.getSysAdmin();

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public SysAdmin authenticate(String email, String password) {
        try {
            Map<String, String> params = new HashMap<>();
            params.put("email", email);
            params.put("password", password);

            String sql = SqlUtils.instance.findSqlStatement("authenticateSysAdmin");
            SysAdminCallbackHandler sysAdminCallbackHandler = new SysAdminCallbackHandler();
            jdbcTemplateService.theACJdbcTemplate().query(sql, params, sysAdminCallbackHandler);

            SysAdmin sysAdmin = sysAdminCallbackHandler.getSysAdmin();
            if (sysAdmin == null || sysAdmin.getId() == 0) {
                return null;
            }
            return sysAdmin;

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public SysAdmin getSysAdminById(long sysAdminId) {
        try {
            Map<String, Long> params = new HashMap<>();
            params.put("sysAdminId", sysAdminId);

            String sql = SqlUtils.instance.findSqlStatement("getSysAdminById");
            SysAdminCallbackHandler sysAdminCallbackHandler = new SysAdminCallbackHandler();
            jdbcTemplateService.theACJdbcTemplate().query(sql, params, sysAdminCallbackHandler);

            SysAdmin sysAdmin = sysAdminCallbackHandler.getSysAdmin();
            if (sysAdmin.getId() == 0) {
                return null;
            }
            return sysAdmin;

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    private static class SysAdminCallbackHandler implements RowCallbackHandler {
        private SysAdmin sysAdmin;

        public SysAdminCallbackHandler() {}

        public SysAdmin getSysAdmin() {
            return sysAdmin;
        }

        private void setSysAdmin(SysAdmin sysAdmin) {
            this.sysAdmin = sysAdmin;
        }

        @Override
        public void processRow(ResultSet rs) throws SQLException {
            long id = rs.getLong("id");
            String firstName = rs.getString("first_name");
            String otherNames = rs.getString("other_names");
            String surname = rs.getString("surname");
            String email = rs.getString("email");
            String password = rs.getString("password");
            boolean active = rs.getBoolean("active");
            Timestamp createDate = rs.getTimestamp("create_date");
            Timestamp lastUpdate = rs.getTimestamp("last_update");

            SysAdmin sysAdmin = new SysAdmin();
            sysAdmin.setId(id);
            sysAdmin.setFirstName(firstName);
            sysAdmin.setOtherNames(otherNames);
            sysAdmin.setSurname(surname);
            sysAdmin.setEmail(email);
            sysAdmin.setPassword(password);
            sysAdmin.setActive(active);
            sysAdmin.setCreateDate(createDate);
            sysAdmin.setLastUpdate(lastUpdate);

            setSysAdmin(sysAdmin);
        }
    }

}
