package com.example.examMgt.service;

import com.example.examMgt.model.TxHistoryInfo;
import com.example.examMgt.model.db.ExternalExaminer;
import com.example.examMgt.model.db.Lecturer;
import com.example.examMgt.model.db.SysAdmin;
import org.apache.log4j.Logger;
import org.hyperledger.fabric.sdk.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.security.cert.X509Certificate;

@Service
public class QsccService extends BaseService {

    private static final Logger logger = Logger.getLogger(QsccService.class);

    private static final String chaincodeId = "qscc";

    @Autowired private ExternalExaminerService externalExaminerService;
    @Autowired private LecturerService lecturerService;
    @Autowired private SysAdminSerice sysAdminSerice;


    public TxHistoryInfo getTransactionById(Channel channel, String txId) {
        try {
            BlockInfo blockInfo = channel.queryBlockByTransactionID(txId);
            BlockInfo.EnvelopeInfo envelopeInfo = blockInfo.getEnvelopeInfo(0);
            BlockInfo.EnvelopeInfo.IdentitiesInfo identitiesInfo = envelopeInfo.getCreator();

            long blockNumber = blockInfo.getBlockNumber();
            String sslCert = identitiesInfo.getId();
            String creatorEmail = "";
            String creatorFirstName = "";
            String creatorSurname = "";

            X509Certificate x509Certificate = X509Certificate.getInstance(sslCert.getBytes());
            String sslCertSubject = x509Certificate.getSubjectDN().getName();
            String[] subjectSegments = sslCertSubject.split(",");

            for (int i = 0; i <= subjectSegments.length; i ++) {
                String trimmedStr = subjectSegments[i].trim();
                String[] subjectParams = trimmedStr.split("=");
                if (subjectParams[0].equals("CN")) {
                    creatorEmail = subjectParams[1];
                    break;
                }
            }

            //Get creator first name + surname
            Lecturer lecturer = lecturerService.getLecturerByEmail(creatorEmail);
            if (lecturer != null) {
                creatorFirstName = lecturer.getFirstName();
                creatorSurname = lecturer.getSurname();
            } else {
                ExternalExaminer externalExaminer = externalExaminerService.getExtExaminerByEmail(creatorEmail);
                if (externalExaminer != null) {
                    creatorFirstName = externalExaminer.getFirstName();
                    creatorSurname = externalExaminer.getSurname();
                } else {
                    SysAdmin sysAdmin = sysAdminSerice.getSysAdminByEmail(creatorEmail);
                    if (sysAdmin != null) {
                        creatorFirstName = sysAdmin.getFirstName();
                        creatorSurname = sysAdmin.getSurname();
                    } else {
                        throw new Exception("Could not find creator info");
                    }
                }
            }

            TxHistoryInfo txHistoryInfo = new TxHistoryInfo();
            txHistoryInfo.setTxId(txId);
            txHistoryInfo.setBlockNumber(blockNumber);
            txHistoryInfo.setSslCertificate(sslCert);
            txHistoryInfo.setCreatorEmail(creatorEmail);
            txHistoryInfo.setCreatorFirstName(creatorFirstName);
            txHistoryInfo.setCreatorSurname(creatorSurname);
            return txHistoryInfo;

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }


}
