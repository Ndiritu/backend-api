package com.example.examMgt.service;

import com.example.examMgt.model.db.ExamCreated;
import com.example.examMgt.utils.SqlUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ExamCreatedService {

    private static final Logger logger = Logger.getLogger(ExamCreatedService.class);

    @Autowired private JdbcTemplateService jdbcTemplateService;

    public List<ExamCreated> getExamCreatedByCourseId(Long courseId) {
        try {
            Map<String, Long> params = new HashMap<>();
            params.put("courseId", courseId);

            String sql = SqlUtils.instance.findSqlStatement("getExamCreatedByCourseId");
            return jdbcTemplateService.theACJdbcTemplate().query(sql, params, new ExamCreatedRowMapper());


        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    private static class ExamCreatedRowMapper implements RowMapper<ExamCreated> {
        @Override
        public ExamCreated mapRow(ResultSet rs, int i) throws SQLException {
            long id = rs.getLong("id");
            String examId = rs.getString("exam_id");
            long examCourseId = rs.getLong("exam_course_id");
            long examPeriodId = rs.getLong("exam_period_id");

            ExamCreated examCreated = new ExamCreated();
            examCreated.setId(id);
            examCreated.setExamId(examId);
            examCreated.setExamCourseId(examCourseId);
            examCreated.setExamPeriodId(examPeriodId);

            return examCreated;
        }
    }

}
