package com.example.examMgt.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

@Service
public class JdbcTemplateService {

    private static final Logger logger = Logger.getLogger(JdbcTemplateService.class);

    private final DataSource examMgtDatasource;

    @Autowired
    public JdbcTemplateService(final DataSource examMgtDatasource) {
        this.examMgtDatasource = examMgtDatasource;
    }

    public NamedParameterJdbcTemplate theACJdbcTemplate() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(examMgtDatasource);
        jdbcTemplate.setFetchSize(1000);
        return new NamedParameterJdbcTemplate(jdbcTemplate);
    }

    public Connection getDBConnection() throws SQLException {
        return examMgtDatasource.getConnection();
    }
}
