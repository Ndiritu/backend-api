package com.example.examMgt.service;

import com.example.examMgt.model.HFResponse;
import com.example.examMgt.model.UserSession;
import com.example.examMgt.model.ValidationResponse;
import com.example.examMgt.model.hf.ItemModeration;
import com.example.examMgt.model.hf.Section;
import com.example.examMgt.model.hf.history.HFHistoryResponse;
import com.example.examMgt.model.hf.history.SectionHistory;
import com.example.examMgt.model.viewModel.ItemModerationView;
import com.example.examMgt.model.viewModel.SectionView;
import org.apache.log4j.Logger;
import org.hyperledger.fabric.sdk.BlockEvent;
import org.hyperledger.fabric.sdk.ChaincodeResponse;
import org.hyperledger.fabric.sdk.ProposalResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@Service
public class SectionService extends BaseService {

    private static final Logger logger = Logger.getLogger(SectionService.class);

    private @Value("${hf.chaincode.section.chaincodeId}") String chaincodeSectionId;

    private static EnumMap<Section.SectionFunction, String> chaincodeFnMap = new EnumMap<>(Section.SectionFunction.class);

    private @Value("${hf.chaincode.section.create}") String create;
    private @Value("${hf.chaincode.section.update}") String update;
    private @Value("${hf.chaincode.section.getByExamId}") String getByExamId;
    private @Value("${hf.chaincode.section.getById}") String getById;
    private @Value("${hf.chaincode.section.getAll}") String getAll;
    private @Value("${hf.chaincode.section.delete}") String delete;
    private @Value("${hf.chaincode.section.getHistory}") String getHistory;

    @Autowired private ItemModerationService itemModerationService;

    @PostConstruct
    public void init() {
        initChaincodeFnMap();
    }

    private void initChaincodeFnMap() {
        chaincodeFnMap.put(Section.SectionFunction.CREATE, create);
        chaincodeFnMap.put(Section.SectionFunction.UPDATE, update);
        chaincodeFnMap.put(Section.SectionFunction.GET_BY_EXAM_ID, getByExamId);
        chaincodeFnMap.put(Section.SectionFunction.GET_BY_ID, getById);
        chaincodeFnMap.put(Section.SectionFunction.GET_ALL, getAll);
        chaincodeFnMap.put(Section.SectionFunction.DELETE, delete);
        chaincodeFnMap.put(Section.SectionFunction.GET_HISTORY, getHistory);
    }

    public List<SectionView> convertToSectionViews(UserSession userSession, List<Section> sections) {
        if (sections.isEmpty()) {
            return new ArrayList<>();
        }

        List<ItemModeration> moderations = new ArrayList<>();
        ItemModeration itemModeration;
        for (Section section : sections) {
            itemModeration = itemModerationService.getModerationByItem(
                    userSession, ItemModeration.ItemType.SECTION, section.getId());
            if (itemModeration != null) {
                moderations.add(itemModeration);
            }
        }

        List<ItemModerationView> moderationViews = itemModerationService.convertToItemModerationView(
                moderations);
        HashMap<String, ItemModerationView> moderationViewMap = new HashMap<>();
        for (ItemModerationView moderationView : moderationViews) {
            moderationViewMap.put(moderationView.getItemId(), moderationView);
        }

        List<SectionView> sectionViews = new ArrayList<>();
        for (Section section : sections) {
            SectionView sectionView = new SectionView();
            sectionView.setId(section.getId());
            sectionView.setExamId(section.getExamId());
            sectionView.setDisplayIndex(section.getDisplayIndex());
            sectionView.setName(section.getName());
            sectionView.setModerationDetails(
                    moderationViewMap.get(section.getId())
            );
            sectionView.setCreateDate(section.getCreateDate());
            sectionView.setLastUpdate(section.getLastUpdate());
            sectionView.setDeleted(section.isDeleted());
            sectionView.setDeleteDate(section.getDeleteDate());
            sectionView.setDocType(section.getDocType());

            sectionViews.add(sectionView);
        }
        return sectionViews;
    }

    public List<SectionHistory> getSectionHistory(UserSession userSession, String sectionId) {
        try {
            ArrayList<String> args = new ArrayList<>();
            args.add(sectionId);

            return unpackHFHistoryPayload(
                    queryChaincode(
                            userSession.getHfClient(),
                            userSession.getChannelExams(),
                            chaincodeSectionId,
                            chaincodeFnMap.get(Section.SectionFunction.GET_HISTORY),
                            args
                    )
            );

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public boolean deleteSection(UserSession userSession, String sectionId) {
        try {
            ArrayList<String> args = new ArrayList<>();
            args.add(String.valueOf(userSession.getId()));
            args.add(sectionId);

            //todo: handle completable future better. Then check if transactionEvent.isValid()
            try {
                BlockEvent.TransactionEvent transactionEvent = invokeChaincode(
                        userSession.getHfClient(),
                        userSession.getChannelExams(),
                        chaincodeSectionId,
                        chaincodeFnMap.get(Section.SectionFunction.DELETE),
                        args
                ).get(1, TimeUnit.SECONDS);
            } catch (TimeoutException ex) {
                //todo: May not always return most updated section. Method shd probably return boolean
                return true;
            }
            throw new Exception("Unexpected error occurred");

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return false;
        }
    }

    public List<Section> getAllSections(UserSession userSession) {
        try {
            return unpackHFPayload(
                    queryChaincode(
                            userSession.getHfClient(),
                            userSession.getChannelExams(),
                            chaincodeSectionId,
                            chaincodeFnMap.get(Section.SectionFunction.GET_ALL),
                            new ArrayList<>()
                    )
            );

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public Section getSectionById(UserSession userSession, String sectionId) {
        try {
            ArrayList<String> args = new ArrayList<>();
            args.add(sectionId);

            List<Section> sections = unpackHFPayload(
                        queryChaincode(
                                userSession.getHfClient(),
                                userSession.getChannelExams(),
                                chaincodeSectionId,
                                chaincodeFnMap.get(Section.SectionFunction.GET_BY_ID),
                                args
                        )
            );
            if (!sections.isEmpty()) {
                return sections.get(0);
            }
            return null;

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    //todo: Complete validation
    public ValidationResponse validateSection(Section section) {
        ValidationResponse validationResponse = new ValidationResponse();
        try {
            validationResponse.setValid(true);
            validationResponse.setMessage("");
        } catch (Exception ex) {
            validationResponse.setValid(false);
            validationResponse.setMessage(ex.toString());
        }
        return validationResponse;
    }

    public List<Section> getSectionsByExamId(UserSession userSession, String examId) {
        try {
            ArrayList<String> args = new ArrayList<>();
            args.add(examId);

            return unpackHFPayload(
                    queryChaincode(
                            userSession.getHfClient(),
                            userSession.getChannelExams(),
                            chaincodeSectionId,
                            chaincodeFnMap.get(Section.SectionFunction.GET_BY_EXAM_ID),
                            args
                    )
            );

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public boolean updateSection(UserSession userSession, Section section) {
        try {
            //todo: handle completable future better. Then check if transactionEvent.isValid()
            try {
                BlockEvent.TransactionEvent transactionEvent = invokeChaincode(
                        userSession.getHfClient(),
                        userSession.getChannelExams(),
                        chaincodeSectionId,
                        chaincodeFnMap.get(Section.SectionFunction.UPDATE),
                        getUpdateArgsList(section, userSession.getId())
                ).get(1, TimeUnit.SECONDS);
            } catch (TimeoutException ex) {
                //todo: May not always return most updated section. Method shd probably return boolean
                return true;
            }
            throw new Exception("Unexpected error occurred");

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return false;
        }
    }

    private List<SectionHistory> unpackHFHistoryPayload(
            Collection<ProposalResponse> proposalResponses) throws Exception {
        List<SectionHistory> sectionHistories = new ArrayList<>();

        String payload;
        for (ProposalResponse response : proposalResponses) {
            payload = new String(response.getChaincodeActionResponsePayload());
            logger.info("response: " + payload);

            if (response.isVerified()) {
                if (response.getStatus() == ChaincodeResponse.Status.SUCCESS) {
                    HFHistoryResponse[] responses = gson.fromJson(payload, HFHistoryResponse[].class);

                    for (HFHistoryResponse r : responses) {
                        Section section = gson.fromJson(r.getValue(), Section.class);

                        SectionHistory sectionHistory = new SectionHistory();
                        sectionHistory.setTxId(r.getTxId());
                        sectionHistory.setValue(section);
                        sectionHistory.setTimestamp(r.getTimestamp());
                        sectionHistory.setDelete(r.isDelete());

                        sectionHistories.add(sectionHistory);
                    }

                } else {
                    logger.error("Response unsuccessful: status=" + response.getStatus() + ", message=" + payload);
                }
            } else {
                logger.error("Response unverified: status=" + response.getStatus() + ", message=" + payload);
            }
        }

        return sectionHistories;
    }

    private List<Section> unpackHFPayload(Collection<ProposalResponse> proposalResponses) throws Exception {
        List<Section> sections = new ArrayList<>();

        String payload;
        for (ProposalResponse response : proposalResponses) {
            payload = new String(response.getChaincodeActionResponsePayload());
            logger.info("response: " + payload);

            if (response.isVerified()) {
                if (response.getStatus() == ChaincodeResponse.Status.SUCCESS) {
                    HFResponse[] responses = gson.fromJson(payload, HFResponse[].class);

                    for (HFResponse r : responses) {
                        Section section = gson.fromJson(r.getRecord(), Section.class);
                        sections.add(section);
                    }

                } else {
                    logger.error("Response unsuccessful: status=" + response.getStatus() + ", message=" + payload);
                }
            } else {
                logger.error("Response unverified: status=" + response.getStatus() + ", message=" + payload);
            }
        }
        return sections;
    }

    private ArrayList<String> getCreateArgsList(Section section) {
        //Order of args should match order of params in ccd
        ArrayList<String> args = new ArrayList<>();
        args.add(section.getId());
        args.add(section.getExamId());
        args.add(String.valueOf(section.getDisplayIndex()));
        args.add(section.getName());
        return args;
    }

    private ArrayList<String> getUpdateArgsList(Section section, Long lecturerId) {
        //order of args should match order of params in ccd
        ArrayList<String> args = new ArrayList<>();
        args.add(String.valueOf(lecturerId));
        args.add(section.getId());
        args.add(String.valueOf(section.getDisplayIndex()));
        args.add(section.getName());
        return args;
    }

    public Section createSection(UserSession userSession, Section section) {
        try {
            String sectionId = getUUID();
            section.setId(sectionId);

            //todo: handle completable future better. Then check if transactionEvent.isValid()
            try {
                BlockEvent.TransactionEvent transactionEvent = invokeChaincode(
                        userSession.getHfClient(),
                        userSession.getChannelExams(),
                        chaincodeSectionId,
                        chaincodeFnMap.get(Section.SectionFunction.CREATE),
                        getCreateArgsList(section)
                ).get(1, TimeUnit.SECONDS);
            } catch (TimeoutException ex) {
//                return getQuestionById(userSession, questionId);
                section.setCreateDate(new Timestamp(System.currentTimeMillis()).getTime());
                section.setLastUpdate(new Timestamp(System.currentTimeMillis()).getTime());
                section.setDocType("section");
                return section;
            }
            throw new Exception("Unexpected error occurred");

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }
}
