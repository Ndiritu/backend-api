package com.example.examMgt.service;

import com.example.examMgt.model.db.Student;
import com.example.examMgt.utils.SqlUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

@Service
public class StudentService {

    private static final Logger logger = Logger.getLogger(StudentService.class);

    @Autowired
    private JdbcTemplateService jdbcTemplateService;

    public Student getStudentById(long studentId) {
        try {
            Map<String, Long> params = new HashMap<>();
            params.put("studentId", studentId);

            String sql = SqlUtils.instance.findSqlStatement("getStudentById");
            StudentCallbackHandler  studentCallbackHandler = new StudentCallbackHandler();
            jdbcTemplateService.theACJdbcTemplate().query(sql, params, studentCallbackHandler);

            Student student = studentCallbackHandler.getStudent();
            if (student.getId() == 0) {
                return null;
            }

            return student;

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    private static class StudentCallbackHandler implements RowCallbackHandler {
        private Student student;

        private StudentCallbackHandler() {}

        private Student getStudent() {
            return student;
        }

        private void setStudent(Student student) {
            this.student = student;
        }

        @Override
        public void processRow(ResultSet rs) throws SQLException {
            long id = rs.getLong("id");
            String firstName = rs.getString("first_name");
            String otherNames = rs.getString("other_names");
            String surname = rs.getString("surname");
            String email = rs.getString("email");
            String password = rs.getString("password");
            String registrationNumber = rs.getString("registration_number");
            long natlIdNumber = rs.getLong("natl_id_number");
            int yearOfStudy = rs.getInt("year_of_study");
            Timestamp createDate = rs.getTimestamp("create_date");
            Timestamp lastUpdate = rs.getTimestamp("last_update");

            Student student = new Student();
            student.setId(id);
            student.setFirstName(firstName);
            student.setOtherNames(otherNames);
            student.setSurname(surname);
            student.setEmail(email);
            student.setPassword(password);
            student.setRegistrationNumber(registrationNumber);
            student.setNatlIdNumber(natlIdNumber);
            student.setYearOfStudy(yearOfStudy);
            student.setCreateDate(createDate);
            student.setLastUpdate(lastUpdate);

            setStudent(student);
        }
    }
}
