package com.example.examMgt.service;

import com.example.examMgt.model.UserSession;
import com.example.examMgt.model.db.ExamCentreStaff;
import com.example.examMgt.model.db.ExternalExaminer;
import com.example.examMgt.model.db.Lecturer;
import com.example.examMgt.model.db.Notification;
import com.example.examMgt.model.hf.Examination;
import com.example.examMgt.model.viewModel.ExaminationView;
import com.example.examMgt.model.viewModel.NotificationView;
import com.example.examMgt.utils.SqlUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

@Service
public class NotificationService {

    private static final Logger logger = Logger.getLogger(NotificationService.class);

    @Autowired private JdbcTemplateService jdbcTemplateService;

    @Autowired private LecturerService lecturerService;
    @Autowired private ExternalExaminerService externalExaminerService;
    @Autowired private ExamCentreStaffService examCentreStaffService;
    @Autowired private ExaminationService examinationService;
    @Autowired private MailService mailService;

    public void sendExamGeneratedNotification(UserSession userSession, String examId) throws Exception {
        Examination examination = examinationService.getExaminationById(userSession, examId);
        List<Examination> examinations = new ArrayList<>();
        examinations.add(examination);
        List<ExaminationView> examinationViews = examinationService.convertExamsToExamViews(
                userSession, examinations
        );

        String lecturerFirstName = examinationViews.get(0).getLecturerFirstName();
        String examName = examination.getName();
        String courseCode = String.valueOf(examinationViews.get(0).getCourseCode());
        String courseName = examinationViews.get(0).getCourseName();

        String to = "philipndiritu@gmail.com";
        String subject = "SCI EMS | Exam Generated";
        StringBuilder text = new StringBuilder();
        text.append("Dear ");
        text.append(lecturerFirstName);
        text.append(",\n\nThe following exam has been generated and awaits your collection.\n\nExam Name: ");
        text.append(examName);
        text.append("\nCourse: ");
        text.append(courseCode);
        text.append(" - ");
        text.append(courseName);
        text.append("\n\nRegards,\nSCI Exam Management System");

        mailService.sendMail(to, subject, text.toString());
    }

    public void sendExamModeratedNotification(UserSession userSession, String examId) throws Exception {
        Examination examination = examinationService.getExaminationById(userSession, examId);
        List<Examination> examinations = new ArrayList<>();
        examinations.add(examination);
        List<ExaminationView> examinationViews = examinationService.convertExamsToExamViews(
                userSession, examinations
        );

        String lecturerFirstName = examinationViews.get(0).getLecturerFirstName();
        String examName = examination.getName();
        String courseCode = String.valueOf(examinationViews.get(0).getCourseCode());
        String courseName = examinationViews.get(0).getCourseName();

        String to = "philipndiritu@gmail.com";
        String subject = "SCI EMS | Exam Moderated";
        StringBuilder text = new StringBuilder();
        text.append("Dear ");
        text.append(lecturerFirstName);
        text.append(",\n\nThe following exam has been moderated.\n\nExam Name: ");
        text.append(examName);
        text.append("\nCourse: ");
        text.append(courseCode);
        text.append(" - ");
        text.append(courseName);
        text.append("\n\nRegards,\nSCI Exam Management System");

        mailService.sendMail(to, subject, text.toString());
    }

    public List<NotificationView> convertNotificationToNotificationView(List<Notification> notifications) {
        try {
            HashSet<Long> lecturerIds = new HashSet<>();
            HashSet<Long> extExaminerIds = new HashSet<>();
            HashSet<Long> examCentreStaffIds = new HashSet<>();

            for (Notification notification : notifications) {
                if (notification.getSenderType().equals(UserSession.UserType.LECTURER)) {
                    if (!lecturerIds.contains(notification.getSenderId())) {
                        lecturerIds.add(notification.getSenderId());
                    }
                }

                if (notification.getSenderType().equals(UserSession.UserType.EXT_EXAMINER)) {
                    if (!extExaminerIds.contains(notification.getSenderId())) {
                        extExaminerIds.add(notification.getSenderId());
                    }
                }

                if (notification.getSenderType().equals(UserSession.UserType.EXAM_CENTRE_STAFF)) {
                    if (!examCentreStaffIds.contains(notification.getSenderId())) {
                        examCentreStaffIds.add(notification.getSenderId());
                    }
                }
            }

            List<Lecturer> lecturers = lecturerService.getLecturersByIds(new ArrayList<>(lecturerIds));
            List<ExternalExaminer> externalExaminers = externalExaminerService.getExtExaminersByIds(
                    new ArrayList<>(extExaminerIds)
            );
            List<ExamCentreStaff> examCentreStaffs = examCentreStaffService.getExamCentrStaffByIds(
                    new ArrayList<>(examCentreStaffIds)
            );

            HashMap<Long, Lecturer> lecHashMap = new HashMap<>();
            for (Lecturer lecturer : lecturers) {
                lecHashMap.put(lecturer.getId(), lecturer);
            }
            HashMap<Long, ExternalExaminer> examinerHashMap = new HashMap<>();
            for (ExternalExaminer externalExaminer : externalExaminers) {
                examinerHashMap.put(externalExaminer.getId(), externalExaminer);
            }
            HashMap<Long, ExamCentreStaff> examCentreStaffHashMap = new HashMap<>();
            for (ExamCentreStaff examCentreStaff : examCentreStaffs) {
                examCentreStaffHashMap.put(examCentreStaff.getId(), examCentreStaff);
            }


            List<NotificationView> notificationViews = new ArrayList<>();
            String senderFirstName = "";
            String senderSurname = "";
            for (Notification notification: notifications) {
                long senderId = notification.getSenderId();
                if (notification.getSenderType().equals(UserSession.UserType.LECTURER)) {
                    senderFirstName = lecHashMap.get(senderId).getFirstName();
                    senderSurname = lecHashMap.get(senderId).getSurname();
                }
                if (notification.getSenderType().equals(UserSession.UserType.EXT_EXAMINER)) {
                    senderFirstName = examinerHashMap.get(senderId).getFirstName();
                    senderSurname = examinerHashMap.get(senderId).getSurname();
                }
                if (notification.getSenderType().equals(UserSession.UserType.EXAM_CENTRE_STAFF)) {
                    senderFirstName = examCentreStaffHashMap.get(senderId).getFirstName();
                    senderSurname = examCentreStaffHashMap.get(senderId).getSurname();
                }

                NotificationView notificationView = new NotificationView();
                notificationView.setId(notification.getId());
                notificationView.setType(notification.getType());
                notificationView.setSenderId(notification.getSenderId());
                notificationView.setSenderType(notification.getSenderType());
                notificationView.setSenderFirstName(senderFirstName);
                notificationView.setSenderSurname(senderSurname);
                notificationView.setRecipientId(notification.getRecipientId());
                notificationView.setRecipientType(notification.getRecipientType());
                notificationView.setSeen(notification.isSeen());
                notificationView.setTargetType(notification.getTargetType());
                notificationView.setTargetId(notification.getTargetId());
                notificationView.setCreateDate(notification.getCreateDate());
                notificationView.setLastUpdate(notification.getLastUpdate());

                notificationViews.add(notificationView);
            }

            return notificationViews;

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public List<Notification> getNotificationsBySender(UserSession.UserType senderType, long senderId) {
        try {
            Map<String, Object> params = new HashMap<>();
            params.put("senderType", String.valueOf(senderType));
            params.put("senderId", senderId);

            String sql = SqlUtils.instance.findSqlStatement("getNotificationsBySender");
            return jdbcTemplateService.theACJdbcTemplate().query(sql, params, new NotificationRowMapper());

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }


    public void markNotificationAsSeen(List<Long> ids) {
        try {
            Array idArr = SqlUtils.getPostgresBigintArray(ids, jdbcTemplateService.getDBConnection());
            Map<String, Object> params = new HashMap<>();
            params.put("id", idArr);

            String sql = SqlUtils.instance.findSqlStatement("markNotificationAsSeen");
            jdbcTemplateService.theACJdbcTemplate().update(sql, params);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
        }
    }

    public void createNotification(Notification notification) {
        try {
            Map<String, Object> params = new HashMap<>();
            params.put("type", String.valueOf(notification.getType()));
            params.put("senderId", notification.getSenderId());
            params.put("senderType", String.valueOf(notification.getSenderType()));
            params.put("recipientId", notification.getRecipientId());
            params.put("recipientType", String.valueOf(notification.getRecipientType()));
            params.put("targetType", notification.getTargetType());
            params.put("targetId", notification.getTargetId());

            String sql = SqlUtils.instance.findSqlStatement("createNotification");
            jdbcTemplateService.theACJdbcTemplate().update(sql, params);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
        }
    }


    private static class NotificationCallbackHandler implements RowCallbackHandler {
        private Notification notification;

        public Notification getNotification() {
            return notification;
        }

        private void setNotification(Notification notification) {
            this.notification = notification;
        }

        @Override
        public void processRow(ResultSet rs) throws SQLException {
            long id = rs.getLong("id");
            Notification.NotificationType type = Notification.NotificationType.valueOf(
                    rs.getString("type")
            );
            long senderId = rs.getLong("sender_id");
            UserSession.UserType senderType = UserSession.UserType.valueOf(
                    rs.getString("sender_type")
            );
            long recipientId = rs.getLong("recipient_id");
            UserSession.UserType recipientType = UserSession.UserType.valueOf(
                    rs.getString("recipient_type")
            );
            boolean seen = rs.getBoolean("seen");
            String targetType = rs.getString("target_type");
            String targetId = rs.getString("target_id");
            Timestamp createDate = rs.getTimestamp("create_date");
            Timestamp lastUpdate = rs.getTimestamp("last_update");

            Notification notification = new Notification();
            notification.setId(id);
            notification.setType(type);
            notification.setSenderId(senderId);
            notification.setSenderType(senderType);
            notification.setRecipientId(recipientId);
            notification.setRecipientType(recipientType);
            notification.setSeen(seen);
            notification.setTargetType(targetType);
            notification.setTargetId(targetId);
            notification.setCreateDate(createDate);
            notification.setLastUpdate(lastUpdate);

            setNotification(notification);
        }
    }

    private static class NotificationRowMapper implements RowMapper<Notification> {
        @Override
        public Notification mapRow(ResultSet rs, int i) throws SQLException {
            long id = rs.getLong("id");
            Notification.NotificationType type = Notification.NotificationType.valueOf(
                    rs.getString("type")
            );
            long senderId = rs.getLong("sender_id");
            UserSession.UserType senderType = UserSession.UserType.valueOf(
                    rs.getString("sender_type")
            );
            long recipientId = rs.getLong("recipient_id");
            UserSession.UserType recipientType = UserSession.UserType.valueOf(
                    rs.getString("recipient_type")
            );
            boolean seen = rs.getBoolean("seen");
            String targetType = rs.getString("target_type");
            String targetId = rs.getString("target_id");
            Timestamp createDate = rs.getTimestamp("create_date");
            Timestamp lastUpdate = rs.getTimestamp("last_update");

            Notification notification = new Notification();
            notification.setId(id);
            notification.setType(type);
            notification.setSenderId(senderId);
            notification.setSenderType(senderType);
            notification.setRecipientId(recipientId);
            notification.setRecipientType(recipientType);
            notification.setSeen(seen);
            notification.setTargetType(targetType);
            notification.setTargetId(targetId);
            notification.setCreateDate(createDate);
            notification.setLastUpdate(lastUpdate);

            return notification;
        }
    }

}
