package com.example.examMgt.service;

import com.example.examMgt.model.UserSession;
import com.example.examMgt.model.db.LoginSession;
import com.example.examMgt.utils.SqlUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

@Service
public class LoginSessionService {

    public enum Verify2FACodeResponse {
        INVALID_CODE, EXPIRED_CODE, SUCCESS
    }

    private static final Logger logger = Logger.getLogger(LoginSessionService.class);

    @Autowired private MailService mailService;

    @Autowired private JdbcTemplateService jdbcTemplateService;

    private void send2FACodeEmail(UserSession userSession, String code) {
        String to = "philipndiritu@gmail.com";
        String subject = "SCI EMS | Verify it's you";

        StringBuilder text = new StringBuilder();
        text.append("Dear ");
        text.append(userSession.getFirstName());
        text.append(",\nKindly use the code below to log into your account. It shall expire in 2 minutes.\n\n");
        text.append(code);
        text.append("\n\nRegards,\nSCI Exam Management System");

        new Thread(() -> {
            mailService.sendMail(to, subject, text.toString());
        }).start();
    }

    public void logOutSession(UserSession userSession) {
        Map<String, Object> params = new HashMap<>();
        params.put("userId", userSession.getId());
        params.put("userType", String.valueOf(userSession.getUserType()));

        String sql = SqlUtils.instance.findSqlStatement("logOutSession");
        jdbcTemplateService.theACJdbcTemplate().update(sql, params);
    }

    public void refreshSession(UserSession userSession) {
        logger.debug("Running refrehSession: userSession=" + userSession);

        Random random = new Random();
        int code = random.nextInt(999999) + 102395;
        //default expiry is 3mins
        Timestamp expiry = new Timestamp(System.currentTimeMillis() + (2 * 60 * 1000));

        HashMap<String, Object> params = new HashMap<>();
        params.put("userId", userSession.getId());
        params.put("userType", String.valueOf(userSession.getUserType()));
        params.put("code", String.valueOf(code));
        params.put("expiry", expiry.getTime());

        String sql = SqlUtils.instance.findSqlStatement("refreshSession");
        jdbcTemplateService.theACJdbcTemplate().update(sql, params);

        send2FACodeEmail(userSession, String.valueOf(code));
    }

    public Verify2FACodeResponse verify2FACode(UserSession.UserType userType, long userId, String code) {
        LoginSession loginSession = getLoginSession(userType, userId);

        if (loginSession == null) {
            return null;
        } else {
            if (!loginSession.getTwoFACode().equals(code)) {
                return Verify2FACodeResponse.INVALID_CODE;
            }
            else if (loginSession.getTwoFACode().equals(code)) {
                return Verify2FACodeResponse.SUCCESS;
            } else {
                return Verify2FACodeResponse.EXPIRED_CODE;
            }
        }
    }

    private LoginSession getLoginSession(UserSession.UserType userType, long userId) {
        try {
            Map<String, Object> params = new HashMap<>();
            params.put("userType", String.valueOf(userType));
            params.put("userId", userId);

            String sql = SqlUtils.instance.findSqlStatement("getLoginSession");
            LoginSessionCallbackHandler loginSessionCallbackHandler = new LoginSessionCallbackHandler();
            jdbcTemplateService.theACJdbcTemplate().query(sql, params, loginSessionCallbackHandler);

            return loginSessionCallbackHandler.getLoginSession();

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    private static class LoginSessionCallbackHandler implements RowCallbackHandler {
        private LoginSession loginSession;

        public LoginSession getLoginSession() {
            return loginSession;
        }

        private void setLoginSession(LoginSession loginSession) {
            this.loginSession = loginSession;
        }

        @Override
        public void processRow(ResultSet rs) throws SQLException {
            long id = rs.getLong("id");
            long userId = rs.getLong("user_id");
            UserSession.UserType userType = UserSession.UserType.valueOf(rs.getString("user_type"));
            String twoFACode = rs.getString("two_fa_code");
            Timestamp sessionExpiry = rs.getTimestamp("session_expiry");

            LoginSession loginSession = new LoginSession();
            loginSession.setId(id);
            loginSession.setUserId(userId);
            loginSession.setUserType(userType);
            loginSession.setTwoFACode(twoFACode);
            loginSession.setSessionExpiry(sessionExpiry);

            setLoginSession(loginSession);
        }
    }

}
