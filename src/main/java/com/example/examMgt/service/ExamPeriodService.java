package com.example.examMgt.service;

import com.example.examMgt.model.db.ExamCreated;
import com.example.examMgt.model.db.ExamPeriod;
import com.example.examMgt.utils.SqlUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

@Service
public class ExamPeriodService {

    private static final Logger logger = Logger.getLogger(ExamPeriodService.class);

    @Autowired private JdbcTemplateService jdbcTemplateService;
    @Autowired private ExamCreatedService examCreatedService;

    public ExamPeriod getExamPeriodByExamId(String examId) {
        try {
            Map<String, String> params = new HashMap<>();
            params.put("examId", examId);

            String sql = SqlUtils.instance.findSqlStatement("getExamPeriodByExamId");
            ExamPeriodCallbackHandler examPeriodCallbackHandler = new ExamPeriodCallbackHandler();
            jdbcTemplateService.theACJdbcTemplate().query(sql, params, examPeriodCallbackHandler);

            return examPeriodCallbackHandler.getExamPeriod();

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public List<ExamPeriod> getAvailableExamPeriodsByCourseId(Long courseId) {
        try {
            List<ExamPeriod> allExamPeriods = getAllExamPeriods();
            HashMap<Long, ExamPeriod> examPeriodHashMap = new HashMap<>();
            for (ExamPeriod examPeriod : allExamPeriods) {
                examPeriodHashMap.put(examPeriod.getId(), examPeriod);
            }

            List<ExamCreated> examCreateds = examCreatedService.getExamCreatedByCourseId(courseId);
            for (ExamCreated examCreated : examCreateds) {
                if (examPeriodHashMap.containsKey(examCreated.getExamPeriodId())) {
                    examPeriodHashMap.remove(examCreated.getExamPeriodId());
                }
            }

            return new ArrayList<>(examPeriodHashMap.values());

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public boolean examAlreadyCreatedForPeriod(long courseId, long periodId) {
        try {
            Map<String, Long> params = new HashMap<>();
            params.put("courseId", courseId);
            params.put("periodId", periodId);

            String sql = SqlUtils.instance.findSqlStatement("examAlreadyCreatedForPeriod");
            ExamCreatedCallbackHandler examCreatedCallbackHandler = new ExamCreatedCallbackHandler();
            jdbcTemplateService.theACJdbcTemplate().query(sql, params, examCreatedCallbackHandler);

            if (examCreatedCallbackHandler.getExamCreatedMap().isEmpty()) {
                return false;
            }
            return true;


        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return false;
        }
    }

    public List<ExamPeriod> getAllExamPeriods() {
        try {
            String sql = SqlUtils.instance.findSqlStatement("getAllExamPeriods");
            return jdbcTemplateService.theACJdbcTemplate().query(sql, new ExamPeriodRowMapper());

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    private static class ExamCreatedCallbackHandler implements RowCallbackHandler {
        HashMap<String, Long> examCreatedMap = new HashMap<>();

        public HashMap<String, Long> getExamCreatedMap() {
            return examCreatedMap;
        }

        private void setExamCreatedMap(HashMap<String, Long> examCreatedMap) {
            this.examCreatedMap = examCreatedMap;
        }

        @Override
        public void processRow(ResultSet rs) throws SQLException {
            long id = rs.getLong("id");
            HashMap<String, Long> map = new HashMap<>();
            map.put("id", id);
            setExamCreatedMap(map);
        }
    }

    private static class ExamPeriodRowMapper implements RowMapper<ExamPeriod> {
        @Override
        public ExamPeriod mapRow(ResultSet rs, int i) throws SQLException {
            long id = rs.getLong("id");
            String name = rs.getString("name");
            Timestamp startDate = rs.getTimestamp("start_date");
            Timestamp createDate = rs.getTimestamp("create_date");
            Timestamp lastUpdate = rs.getTimestamp("last_update");

            ExamPeriod examPeriod = new ExamPeriod();
            examPeriod.setId(id);
            examPeriod.setName(name);
            examPeriod.setStartDate(startDate);
            examPeriod.setCreateDate(createDate);
            examPeriod.setLastUpdate(lastUpdate);
            return examPeriod;
        }
    }

    private static class ExamPeriodCallbackHandler implements RowCallbackHandler {
        private ExamPeriod examPeriod;

        public ExamPeriod getExamPeriod() {
            return examPeriod;
        }

        private void setExamPeriod(ExamPeriod examPeriod) {
            this.examPeriod = examPeriod;
        }

        @Override
        public void processRow(ResultSet rs) throws SQLException {
            long id = rs.getLong("id");
            String name = rs.getString("name");
            Timestamp startDate = rs.getTimestamp("start_date");
            Timestamp createDate = rs.getTimestamp("create_date");
            Timestamp lastUpdate = rs.getTimestamp("last_update");

            ExamPeriod examPeriod = new ExamPeriod();
            examPeriod.setId(id);
            examPeriod.setName(name);
            examPeriod.setStartDate(startDate);
            examPeriod.setCreateDate(createDate);
            examPeriod.setLastUpdate(lastUpdate);

            setExamPeriod(examPeriod);
        }
    }
}
