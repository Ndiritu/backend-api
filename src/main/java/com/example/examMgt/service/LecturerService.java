package com.example.examMgt.service;

import com.example.examMgt.model.db.Lecturer;
import com.example.examMgt.utils.SqlUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class LecturerService {

    private static final Logger logger = Logger.getLogger(LecturerService.class);

    @Autowired
    private JdbcTemplateService jdbcTemplateService;

    public Lecturer authenticateLecturer(String email, String password) {
        try {
            Map<String, String> params = new HashMap<>();
            params.put("email", email);
            params.put("password", password);

            String sql = SqlUtils.instance.findSqlStatement("authenticateLecturer");
            LecturerCallbackHandler lecturerCallbackHandler = new LecturerCallbackHandler();
            jdbcTemplateService.theACJdbcTemplate().query(sql, params, lecturerCallbackHandler);

            Lecturer lecturer = lecturerCallbackHandler.getLecturer();
            if (lecturer == null || lecturer.getId() == 0) {
                return null;
            }
            return lecturer;

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public Lecturer getLecturerByEmail(String email) {
        try {
            Map<String, String> params = new HashMap<>();
            params.put("email", email);

            String sql = SqlUtils.instance.findSqlStatement("getLecByEmail");
            LecturerCallbackHandler callbackHandler = new LecturerCallbackHandler();
            jdbcTemplateService.theACJdbcTemplate().query(sql, params, callbackHandler);

            Lecturer lecturer = callbackHandler.getLecturer();
            if (lecturer == null || lecturer.getId() == 0) {
                return null;
            }

            return lecturer;

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public Lecturer getLecturerById(long id) {
        try {
            Map<String, Long> params = new HashMap<>();
            params.put("id", id);

            String sql = SqlUtils.instance.findSqlStatement("getLecById");
            LecturerCallbackHandler callbackHandler = new LecturerCallbackHandler();
            jdbcTemplateService.theACJdbcTemplate().query(sql, params, callbackHandler);

            Lecturer lecturer = callbackHandler.getLecturer();
            if (lecturer == null || lecturer.getId() == 0) {
                return null;
            }

            return lecturer;

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public List<Lecturer> getLecturersByIds(List<Long> ids) {
        try {
            Array lecIdsArr = SqlUtils.getPostgresBigintArray(ids, jdbcTemplateService.getDBConnection());
            Map<String, Object> params = new HashMap<>();
            params.put("lecIds", lecIdsArr);

            String sql = SqlUtils.instance.findSqlStatement("getLecsByIds");
            return jdbcTemplateService.theACJdbcTemplate().query(sql, params, new LecturerRowMapper());

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return new ArrayList<>();
        }

    }

    private static class LecturerRowMapper implements RowMapper<Lecturer> {
        @Override
        public Lecturer mapRow(ResultSet rs, int i) throws SQLException {
            long id = rs.getLong("id");
            String firstName = rs.getString("first_name");
            String otherNames = rs.getString("other_names");
            String surname = rs.getString("surname");
            String email = rs.getString("email");
            String password = rs.getString("password");
            boolean active = rs.getBoolean("active");
            Timestamp createDate= rs.getTimestamp("create_date");
            Timestamp lastUpdate = rs.getTimestamp("last_update");

            Lecturer lecturer = new Lecturer();
            lecturer.setId(id);
            lecturer.setFirstName(firstName);
            lecturer.setOtherNames(otherNames);
            lecturer.setSurname(surname);
            lecturer.setEmail(email);
            lecturer.setPassword(password);
            lecturer.setActive(active);
            lecturer.setCreateDate(createDate);
            lecturer.setLastUpdate(lastUpdate);
            return lecturer;
        }
    }

    private static class LecturerCallbackHandler implements RowCallbackHandler {
        private Lecturer lecturer;

        private LecturerCallbackHandler() {}

        private Lecturer getLecturer() {
            return lecturer;
        }

        private void setLecturer(Lecturer lecturer) {
            this.lecturer = lecturer;
        }

        @Override
        public void processRow(ResultSet rs) throws SQLException {
            long id = rs.getLong("id");
            String firstName = rs.getString("first_name");
            String otherNames = rs.getString("other_names");
            String surname = rs.getString("surname");
            String email = rs.getString("email");
            String password = rs.getString("password");
            boolean active = rs.getBoolean("active");
            Timestamp createDate= rs.getTimestamp("create_date");
            Timestamp lastUpdate = rs.getTimestamp("last_update");

            Lecturer lecturer = new Lecturer();
            lecturer.setId(id);
            lecturer.setFirstName(firstName);
            lecturer.setOtherNames(otherNames);
            lecturer.setSurname(surname);
            lecturer.setEmail(email);
            lecturer.setPassword(password);
            lecturer.setActive(active);
            lecturer.setCreateDate(createDate);
            lecturer.setLastUpdate(lastUpdate);

            setLecturer(lecturer);
        }
    }

}
