package com.example.examMgt.service;

import com.example.examMgt.model.hf.*;
import com.example.examMgt.model.hf.history.HFHistoryResponse;
import com.example.examMgt.model.HFResponse;
import com.example.examMgt.model.UserSession;
import com.example.examMgt.model.ValidationResponse;
import com.example.examMgt.model.db.Course;
import com.example.examMgt.model.db.Lecturer;
import com.example.examMgt.model.hf.history.ExaminationHistory;
import com.example.examMgt.model.viewModel.*;
import com.example.examMgt.utils.SqlUtils;
import org.apache.log4j.Logger;
import org.hyperledger.fabric.sdk.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.*;

@Service
public class ExaminationService extends BaseService{

    private static final Logger logger = Logger.getLogger(ExaminationService.class);

    @Autowired private JdbcTemplateService jdbcTemplateService;

    private @Value("${hf.chaincode.exam.chaincodeId}") String chaincodeExamId;

    private static EnumMap<Examination.ExaminationFunction, String> chaincodeFnMap = new EnumMap<>(Examination.ExaminationFunction.class);

    private @Value("${hf.chaincode.exam.create}") String create = "";
    private @Value("${hf.chaincode.exam.getAll}") String getAll = "";
    private @Value("${hf.chaincode.exam.getByCourseId}") String getByCourseId = "";
    private @Value("${hf.chaincode.exam.getByLecId}") String getByLecId = "";
    private @Value("${hf.chaincode.exam.getById}") String getById = "";
    private @Value("${hf.chaincode.exam.update}") String update = "";
    private @Value("${hf.chaincode.exam.getHistory}") String getHistory = "";
    private @Value("${hf.chaincode.exam.delete}") String delete = "";

    @Autowired private CourseService courseService;
    @Autowired private LecturerService lecturerService;
    @Autowired private ExternalExaminerService externalExaminerService;
    @Autowired private ChairpersonService chairpersonService;
    @Autowired private ExamApprovalService examApprovalService;
    @Autowired private ItemModerationService itemModerationService;
    @Autowired private SectionService sectionService;
    @Autowired private InstructionService instructionService;
    @Autowired private QuestionService questionService;
    @Autowired private ExamGenerationService examGenerationService;

    @PostConstruct
    public void init() {
        initChaincodeFnMap();
    }

    private void initChaincodeFnMap() {
        chaincodeFnMap.put(Examination.ExaminationFunction.CREATE, create);
        chaincodeFnMap.put(Examination.ExaminationFunction.GET_ALL, getAll);
        chaincodeFnMap.put(Examination.ExaminationFunction.GET_BY_COURSE, getByCourseId);
        chaincodeFnMap.put(Examination.ExaminationFunction.GET_BY_LECTURER, getByLecId);
        chaincodeFnMap.put(Examination.ExaminationFunction.GET_BY_ID, getById);
        chaincodeFnMap.put(Examination.ExaminationFunction.UPDATE, update);
        chaincodeFnMap.put(Examination.ExaminationFunction.GET_HISTORY, getHistory);
        chaincodeFnMap.put(Examination.ExaminationFunction.DELETE, delete);
    }

    public List<Object> buildExamTree(UserSession userSession, String examId) throws Exception {
        List<Section> sections = sectionService.getSectionsByExamId(userSession, examId);
        List<Instruction> instructions = instructionService.getInstructionsByExamId(userSession, examId);
        List<Question> questions = questionService.getQuestionsByExamId(userSession, examId);

        List<Object> sortedContent = new ArrayList<>();
        while (!questions.isEmpty()
                || !sections.isEmpty()
                || !instructions.isEmpty()) {

            HashMap<Integer, Object> displayIndexExamItemMap = new HashMap<>();
            if (!questions.isEmpty()) {
                Question question = questions.get(questions.size() - 1);
                displayIndexExamItemMap.put(question.getDisplayIndex(), question);
            }
            if (!sections.isEmpty()) {
                Section section = sections.get(sections.size() - 1);
                displayIndexExamItemMap.put(section.getDisplayIndex(), section);
            }
            if (!instructions.isEmpty()) {
                Instruction instruction = instructions.get(instructions.size() - 1);
                displayIndexExamItemMap.put(instruction.getDisplayIndex(), instruction);
            }

            int minDisplayIndex = getMinimumDisplayIndex(
                    displayIndexExamItemMap.keySet().toArray(new Integer[
                            displayIndexExamItemMap.keySet().size()])
            );

            sortedContent.add(displayIndexExamItemMap.get(minDisplayIndex));

            if (displayIndexExamItemMap.get(minDisplayIndex).getClass() == Question.class) {
                questions.remove(questions.size() - 1);
            } else if (displayIndexExamItemMap.get(minDisplayIndex).getClass() == Instruction.class) {
                instructions.remove(instructions.size() - 1);
            } else {
                sections.remove(sections.size() - 1);
            }
        }
        return sortedContent;
    }

    public void updateDisplayIndexesBeforeCreate(UserSession userSession, String examId, int startIndex) {
        List<Question> questions = questionService.getQuestionsByExamId(userSession, examId);
        List<Section> sections = sectionService.getSectionsByExamId(userSession, examId);
        List<Instruction> instructions = instructionService.getInstructionsByExamId(userSession, examId);

        for (Question question : questions) {
            int displayIndex = question.getDisplayIndex();
            if (displayIndex >= startIndex) {
                question.setDisplayIndex(displayIndex + 1);
                questionService.updateQuestion(userSession, question);
            }
        }

        for (Section section : sections) {
            int displayIndex = section.getDisplayIndex();
            if (displayIndex >= startIndex) {
                section.setDisplayIndex(displayIndex + 1);
                sectionService.updateSection(userSession, section);
            }
        }

        for (Instruction instruction : instructions) {
            int displayIndex = instruction.getDisplayIndex();
            if (displayIndex >= startIndex) {
                instruction.setDisplayIndex(displayIndex + 1);
                instructionService.updateInstruction(userSession, instruction);
            }
        }
    }

    private int getMinimumDisplayIndex(Integer[] indexes) throws Exception {
        if (indexes.length == 0) {
            throw new Exception("Empty list");
        }

        if (indexes.length == 1) {
            return indexes[0];
        }

        int minIndex = indexes[0];
        for (int i = 1; i <= indexes.length - 1; i ++) {
            if (indexes[i] < minIndex) {
                minIndex = indexes[i];
            }
        }
        return minIndex;
    }

    public List<Object> getExamContent(UserSession userSession, String examId) {
        try {
            long start = System.currentTimeMillis();

            List<Question> questions = questionService.getQuestionsByExamId(userSession, examId);
            List<Section> sections = sectionService.getSectionsByExamId(userSession, examId);
            List<Instruction> instructions = instructionService.getInstructionsByExamId(userSession, examId);

            List<QuestionView> questionViews = questionService.convertToQuestionViews(userSession, questions);
            List<SectionView> sectionViews = sectionService.convertToSectionViews(userSession, sections);
            List<InstructionView> instructionViews = instructionService.convertToInstructionViews(userSession, instructions);

            // sort according to asc order of displayIndex
            List<Object> sortedContent = new ArrayList<>();
            while (!questionViews.isEmpty()
                    || !sectionViews.isEmpty()
                    || !instructionViews.isEmpty()) {

                HashMap<Integer, Object> displayIndexExamItemMap = new HashMap<>();
                if (!questionViews.isEmpty()) {
                    QuestionView questionView = questionViews.get(questionViews.size() - 1);
                    displayIndexExamItemMap.put(questionView.getDisplayIndex(), questionView);
                }
                if (!sectionViews.isEmpty()) {
                    SectionView sectionView = sectionViews.get(sectionViews.size() - 1);
                    displayIndexExamItemMap.put(sectionView.getDisplayIndex(), sectionView);
                }
                if (!instructionViews.isEmpty()) {
                    InstructionView instructionView = instructionViews.get(instructionViews.size() - 1);
                    displayIndexExamItemMap.put(instructionView.getDisplayIndex(), instructionView);
                }

                int minDisplayIndex = getMinimumDisplayIndex(
                        displayIndexExamItemMap.keySet().toArray(new Integer[
                                displayIndexExamItemMap.keySet().size()])
                );

                sortedContent.add(displayIndexExamItemMap.get(minDisplayIndex));

                if (displayIndexExamItemMap.get(minDisplayIndex).getClass() == QuestionView.class) {
                    questionViews.remove(questionViews.size() - 1);
                } else if (displayIndexExamItemMap.get(minDisplayIndex).getClass() == InstructionView.class) {
                    instructionViews.remove(instructionViews.size() - 1);
                } else {
                    sectionViews.remove(sectionViews.size() - 1);
                }
            }

            logger.debug("Sorted content now has=" + sortedContent);
            logger.debug("End of execution: " + (System.currentTimeMillis() - start) + " ms");
            return sortedContent;
        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public List<ExaminationHistoryView> convertExamHistoriesToExamHistoryViews(
            UserSession userSession,
            List<ExaminationHistory> examinationHistories
    ) throws Exception {
        List<Examination> exams = new ArrayList<>();
        for (ExaminationHistory examinationHistory : examinationHistories) {
            exams.add(examinationHistory.getValue());
        }

        List<ExaminationView> examViews = convertExamsToExamViews(userSession, exams);
        HashMap<String, ExaminationView> examIdExamViewMap = new HashMap<>();
        for (ExaminationView examView : examViews) {
            examIdExamViewMap.put(examView.getId(), examView);
        }


        List<ExaminationHistoryView> examinationHistoryViews = new ArrayList<>();
        ExaminationView examView;
        for (ExaminationHistory examinationHistory : examinationHistories) {
            examView = examIdExamViewMap.get(examinationHistory.getValue().getId());

            ExaminationHistoryView examinationHistoryView = new ExaminationHistoryView();
            examinationHistoryView.setTxId(examinationHistory.getTxId());
            examinationHistoryView.setValue(examView);
            examinationHistoryView.setTimestamp(examinationHistory.getTimestamp());
            examinationHistoryView.setDelete(examinationHistory.isDelete());

            examinationHistoryViews.add(examinationHistoryView);
        }

        return examinationHistoryViews;
    }

    private ArrayList<String> getUpdateArgsList(Examination examination, long lecturerId) {
        ArrayList<String> args = new ArrayList<>();
        args.add(examination.getId());
        args.add(String.valueOf(lecturerId));
        args.add(examination.getName());
        args.add(examination.getExaminationType());
        args.add(String.valueOf(examination.getCourseId()));
        args.add(String.valueOf(examination.getExamDate()));
        args.add(String.valueOf(examination.getStartTime()));
        args.add(String.valueOf(examination.getDuration()));
        args.add(examination.getPrimaryNumberingFormat());
        args.add(examination.getSecondaryNumberingFormat());

        return args;
    }

    private ArrayList<String> getCreateArgsList(Examination examination) {
        //Order must align with order of params in chaincode
        ArrayList<String> args = new ArrayList<>();
        args.add(examination.getId());
        args.add(examination.getName());
        args.add(examination.getExaminationType());
        args.add(String.valueOf(examination.getCourseId()));
        args.add(String.valueOf(examination.getCreatedByLecturerId()));
        args.add(String.valueOf(examination.getUniversityId()));
        args.add(String.valueOf(examination.getExamDate()));
        args.add(String.valueOf(examination.getStartTime()));
        args.add(String.valueOf(examination.getDuration()));
        args.add(examination.getPrimaryNumberingFormat());
        args.add(examination.getSecondaryNumberingFormat());
        return args;
    }

    public boolean updateExamination(UserSession userSession, Examination examination) {
        try {
            //todo: handle completable future better. Then check if transactionEvent.isValid()
            try {
                BlockEvent.TransactionEvent transactionEvent = invokeChaincode(
                        userSession.getHfClient(),
                        userSession.getChannelExams(),
                        chaincodeExamId,
                        chaincodeFnMap.get(Examination.ExaminationFunction.UPDATE),
                        getUpdateArgsList(examination, userSession.getId())
                ).get(1, TimeUnit.SECONDS);
            } catch (TimeoutException ex) {
                return true;
            }
            throw new Exception("Unexpected error occurred");

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return false;
        }
    }

    public boolean deleteExamination(UserSession userSession, String examId) {
        try {
            ArrayList<String> args = new ArrayList<>();
            args.add(examId);
            args.add(String.valueOf(userSession.getId()));

            //todo: handle completable future better. Then check if transactionEvent.isValid()
            try {
                BlockEvent.TransactionEvent transactionEvent = invokeChaincode(
                        userSession.getHfClient(),
                        userSession.getChannelExams(),
                        chaincodeExamId,
                        chaincodeFnMap.get(Examination.ExaminationFunction.DELETE),
                        args
                ).get(1, TimeUnit.SECONDS);
            } catch (TimeoutException ex) {
                return true;
            }
            throw new Exception("Unexpected error occurred");


        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return false;
        }
    }

    public List<ExaminationHistory> getHistoryById(UserSession userSession, String examId) {
        try {
            ArrayList<String> args = new ArrayList<>();
            args.add(examId);

            return unpackHFHistoryPayload(
                    queryChaincode(
                            userSession.getHfClient(),
                            userSession.getChannelExams(),
                            chaincodeExamId,
                            chaincodeFnMap.get(Examination.ExaminationFunction.GET_HISTORY),
                            args
                    )
            );

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public Examination getExaminationById(UserSession userSession, String examId) {
        try {
            ArrayList<String> args = new ArrayList<>();
            args.add(examId);

            Collection<ProposalResponse> proposalResponses = queryChaincode(
                    userSession.getHfClient(),
                    userSession.getChannelExams(),
                    chaincodeExamId,
                    chaincodeFnMap.get(Examination.ExaminationFunction.GET_BY_ID),
                    args
            );

            List<Examination> exams = unpackHFPayload(proposalResponses);
            if (exams.isEmpty()) {
                return null;
            } else {
                return exams.get(0);
            }

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public List<Examination> getExamsByLecturerId(UserSession userSession, long lecturerId) {
        try {
            ArrayList<String> args = new ArrayList<>();
            args.add(String.valueOf(lecturerId));

            return unpackHFPayload(
                    queryChaincode(
                            userSession.getHfClient(),
                            userSession.getChannelExams(),
                            chaincodeExamId,
                            chaincodeFnMap.get(Examination.ExaminationFunction.GET_BY_LECTURER),
                            args
                    )
            );

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public List<Examination> getExamsByCourseId(UserSession userSession, long courseId) {
        try {
            ArrayList<String> args = new ArrayList<>();
            args.add(String.valueOf(courseId));

            return unpackHFPayload(
                    queryChaincode(
                            userSession.getHfClient(),
                            userSession.getChannelExams(),
                            chaincodeExamId,
                            chaincodeFnMap.get(Examination.ExaminationFunction.GET_BY_COURSE),
                            args
                    )
            );

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public List<Examination> getAllExaminations(UserSession userSession) {
        try {
            return unpackHFPayload(
                    queryChaincode(
                            userSession.getHfClient(),
                            userSession.getChannelExams(),
                            chaincodeExamId,
                            chaincodeFnMap.get(Examination.ExaminationFunction.GET_ALL),
                            new ArrayList<>()
                    )
            );
        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public Examination createExamination(UserSession userSession, Examination examination) {
        try {
            String examinationId = getUUID();
            examination.setId(examinationId);

            //todo: handle completable future better. Then check if transactionEvent.isValid()
            try {
                BlockEvent.TransactionEvent transactionEvent = invokeChaincode(
                        userSession.getHfClient(),
                        userSession.getChannelExams(),
                        chaincodeExamId,
                        chaincodeFnMap.get(Examination.ExaminationFunction.CREATE),
                        getCreateArgsList(examination)
                ).get(1, TimeUnit.SECONDS);
            } catch (TimeoutException ex) {
                //todo: See if it's possible to getExaminationById after some delay???/ separate thread
//                return getExaminationById(userSession, examinationId);
                return examination;

            }
            throw new Exception("Unexpected error occurred");

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    //todo: Finish implementing validation
    public ValidationResponse validateExamination(Examination examination) {
        ValidationResponse validationResponse = new ValidationResponse();
        try {
            validationResponse.setValid(true);
            validationResponse.setMessage("");

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            validationResponse.setValid(false);
            validationResponse.setMessage(ex.toString());
        }

        return validationResponse;
    }

    public List<ExaminationView> convertApprovalsToExamViews(UserSession userSession,
                                                             List<ExamApproval> approvals) throws Exception {
        logger.debug("Executing convertApprovalsToExamViews() approvals=" + approvals);
        if (approvals.isEmpty()) {
            return new ArrayList<>();
        }

        List<ExamApprovalView> approvalViews = examApprovalService.convertApprovalsToApprovalViews(approvals);

        HashMap<String, ExamApprovalView> approvalViewMap = new HashMap<>();
        List<String> examIds = new ArrayList<>();
        for (ExamApprovalView approvalView : approvalViews) {
            examIds.add(approvalView.getExamId());
            approvalViewMap.put(approvalView.getExamId(), approvalView);
        }

        List<Examination> exams = new ArrayList<>();
        Examination exam;
        for (String examId : examIds) {
            exam = getExaminationById(userSession, examId);
            if (exam != null) {
                exams.add(exam);
            }
        }

        List<ExaminationView> basicExamViews = convertExamsToBasicExamViews(exams);
        List<ExaminationView> approvalExamViews = new ArrayList<>();

        ExamApprovalView approvalView;
        for (ExaminationView examView : basicExamViews) {
            approvalView = approvalViewMap.get(examView.getId());
            examView.setApprovalDetails(approvalView);
            approvalExamViews.add(examView);
        }

        return approvalExamViews;
    }

    public List<ExaminationView> convertExamsToExamViews(UserSession userSession, List<Examination> exams) throws Exception {
        logger.info("Executing convertExamsToExamViews() exams=" + exams);
        List<ExaminationView> basicExamViews = convertExamsToBasicExamViews(exams);
        logger.debug("basicExamViews: = " + basicExamViews);

        List<String> examIds = new ArrayList<>();
        for (ExaminationView examView : basicExamViews) {
            examIds.add(examView.getId());
        }

        List<ExamApproval> approvals = new ArrayList<>();
        List<ItemModeration> moderations = new ArrayList<>();
        List<ExamGeneration> generations = new ArrayList<>();

        ExamApproval approval;
        ItemModeration moderation;
        ExamGeneration examGeneration;
        for (String examId : examIds) {
            approval = examApprovalService.getApprovalByExamId(userSession, examId);
            if (approval != null) {
                approvals.add(approval);
            }
            moderation = itemModerationService.getModerationByItem(
                    userSession, ItemModeration.ItemType.EXAM, examId);
            if (moderation != null) {
                moderations.add(moderation);
            }
            examGeneration = examGenerationService.getExamGenerationByExamId(userSession, examId);
            if (examGeneration != null) {
                generations.add(examGeneration);
            }
        }

        List<ExamApprovalView> approvalViews = examApprovalService.convertApprovalsToApprovalViews(approvals);
        HashMap<String, ExamApprovalView> examIdApprovalMap = new HashMap<>();
        for (ExamApprovalView approvalView : approvalViews) {
            examIdApprovalMap.put(approvalView.getExamId(), approvalView);
        }

        List<ItemModerationView> moderationViews = itemModerationService.convertToItemModerationView(moderations);
        HashMap<String, ItemModerationView> examIdModerationMap = new HashMap<>();
        for (ItemModerationView moderationView : moderationViews) {
            examIdModerationMap.put(moderationView.getItemId(), moderationView);
        }

        List<ExamGenerationView> generationViews = examGenerationService.convertGenerationsToGenerationViews(generations);
        HashMap<String, ExamGenerationView> examIdGenerationMap = new HashMap<>();
        for (ExamGenerationView generationView : generationViews) {
            examIdGenerationMap.put(generationView.getExamId(), generationView);
        }

        List<ExaminationView> completeExamViews = new ArrayList<>();
        for (ExaminationView examView : basicExamViews) {
            String examId = examView.getId();
            examView.setApprovalDetails(examIdApprovalMap.get(examId));
            examView.setModerationDetails(examIdModerationMap.get(examId));
            examView.setGenerationDetails(examIdGenerationMap.get(examId));
            completeExamViews.add(examView);
        }
        return completeExamViews;
    }

    private List<ExaminationView> convertExamsToBasicExamViews(List<Examination> exams) throws Exception {
        logger.debug("Executing convertExamsToBasicExamViews() exams=" + exams);
        if (exams.isEmpty()) {
            return new ArrayList<>();
        }

        //Basic exam view has no approvalDetails / moderationDetails
        HashSet<Long> courseIds = new HashSet<>();
        HashSet<Long> lecturerIds = new HashSet<>();

        for (Examination exam : exams) {
            if (!courseIds.contains(exam.getCourseId())) {
                courseIds.add(exam.getCourseId());
            }
            if (!lecturerIds.contains(exam.getCreatedByLecturerId())) {
                lecturerIds.add(exam.getCreatedByLecturerId());
            }
        }

        List<Course> courses = courseService.getCoursesByIds(new ArrayList<>(courseIds));
        List<Lecturer> lecturers = lecturerService.getLecturersByIds(new ArrayList<>(lecturerIds));

        HashMap<Long, Course> courseIdMap = new HashMap<>();
        HashMap<Long, Lecturer> lecturerMap = new HashMap<>();

        for (Course course : courses) {
            courseIdMap.put(course.getId(), course);
        }
        for (Lecturer lecturer : lecturers) {
            lecturerMap.put(lecturer.getId(), lecturer);
        }

        List<ExaminationView> examViews = new ArrayList<>();

        for (Examination exam : exams) {
            Course course = courseIdMap.get(exam.getCourseId());
            Lecturer lecturer = lecturerMap.get(exam.getCreatedByLecturerId());

            ExaminationView examinationView = new ExaminationView();
            examinationView.setId(exam.getId());
            examinationView.setName(exam.getName());
            //todo: temp fix. Find how to cast enum
//            examinationView.setExaminationType(Examination.ExaminationType.valueOf(exam.getExaminationType()));
            examinationView.setCourseId(exam.getCourseId());
            examinationView.setCourseCode(course.getCourseCode());
            examinationView.setCourseName(course.getName());
            examinationView.setYearOfStudy(course.getYearOfStudy());
            examinationView.setCreatedByLecturerId(exam.getCreatedByLecturerId());
            examinationView.setLecturerFirstName(lecturer.getFirstName());
            examinationView.setLecturerSurname(lecturer.getSurname());
            examinationView.setExamDate(exam.getExamDate());
            examinationView.setStartTime(exam.getStartTime());
            examinationView.setDuration(exam.getDuration());
            examinationView.setPrimaryNumberingFormat(exam.getPrimaryNumberingFormat());
            examinationView.setSecondaryNumberingFormat(exam.getSecondaryNumberingFormat());
            examinationView.setCreateDate(exam.getCreateDate());
            examinationView.setLastUpdate(exam.getLastUpdate());
            examinationView.setDeleted(exam.isDeleted());
            examinationView.setDeleteDate(exam.getDeleteDate());

            examViews.add(examinationView);
        }

        return examViews;
    }

    private List<ExaminationHistory> unpackHFHistoryPayload(Collection<ProposalResponse> proposalResponses) throws Exception {
        List<ExaminationHistory> examHistoryList = new ArrayList<>();

        String payload;
        for (ProposalResponse response : proposalResponses) {
            payload = new String(response.getChaincodeActionResponsePayload());
            logger.info("response: " + payload);

            if (response.isVerified()) {
                if (response.getStatus() == ChaincodeResponse.Status.SUCCESS) {
                    HFHistoryResponse[] responses = gson.fromJson(payload, HFHistoryResponse[].class);

                    for (HFHistoryResponse r : responses) {
                        Examination examination = gson.fromJson(r.getValue(), Examination.class);

                        ExaminationHistory examinationHistory = new ExaminationHistory();
                        examinationHistory.setTxId(r.getTxId());
                        examinationHistory.setValue(examination);
                        examinationHistory.setTimestamp(r.getTimestamp());
                        examinationHistory.setDelete(r.isDelete());

                        examHistoryList.add(examinationHistory);
                    }

                } else {
                    logger.error("Response failed: status=" + response.getStatus() + ", msg=" + payload);
                }

            } else {
                logger.error("Response unverified: status=" + response.getStatus() + ", msg=" + payload);
            }

        }
        return examHistoryList;
    }

    private List<Examination> unpackHFPayload(Collection<ProposalResponse> proposalResponses) throws Exception {
        List<Examination> examinations = new ArrayList<>();

        for (ProposalResponse response : proposalResponses) {
            String payload = new String(response.getChaincodeActionResponsePayload());
            if (response.isVerified()) {
                if (response.getStatus() == ChaincodeResponse.Status.SUCCESS) {
                   logger.info("response: " + payload);

                    HFResponse[] responses = gson.fromJson(payload, HFResponse[].class);

                    for (HFResponse r : responses) {
                        Examination examination = gson.fromJson(r.getRecord(), Examination.class);
                        examinations.add(examination);
                    }
                } else {
                    logger.error("Response failed: status=" + response.getStatus() + ", message=" + payload);
                }
            } else {
                logger.error("Response is unverified: status=" + response.getStatus() + ", message=" + payload);
            }
        }

        return examinations;
    }

    public void insertIntoExamsCreated(long examPeriodId, String examId, long examCourseId) {
        try {
            Map<String, Object> params = new HashMap<>();
            params.put("examPeriodId", examPeriodId);
            params.put("examId", examId);
            params.put("examCourseId", examCourseId);

            String sql = SqlUtils.instance.findSqlStatement("insertIntoExamsCreated");
            jdbcTemplateService.theACJdbcTemplate().update(sql, params);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
        }
    }

    public void deleteExamCreated(String examId) {
        try {
            Map<String, String> param = new HashMap<>();
            param.put("examId", examId);

            String sql = SqlUtils.instance.findSqlStatement("deleteExamCreated");
            jdbcTemplateService.theACJdbcTemplate().update(sql, param);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
        }
    }

    public List<String> getExamsByExamPeriod(long periodId) {
        try {
            Map<String, Long> params = new HashMap<>();
            params.put("periodId", periodId);

            String sql = SqlUtils.instance.findSqlStatement("getExamsByExamPeriod");
            return jdbcTemplateService.theACJdbcTemplate().query(sql, params, new ExamCreatedRowMapper());

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    private static class ExamCreatedRowMapper implements RowMapper<String> {
        @Override
        public String mapRow(ResultSet rs, int i) throws SQLException {
            String examId = rs.getString("exam_id");
            return examId;
        }
    }

}
