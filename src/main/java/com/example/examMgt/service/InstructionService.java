package com.example.examMgt.service;

import com.example.examMgt.model.HFResponse;
import com.example.examMgt.model.UserSession;
import com.example.examMgt.model.ValidationResponse;
import com.example.examMgt.model.hf.Instruction;
import com.example.examMgt.model.hf.ItemModeration;
import com.example.examMgt.model.hf.history.HFHistoryResponse;
import com.example.examMgt.model.hf.history.InstructionHistory;
import com.example.examMgt.model.viewModel.InstructionView;
import com.example.examMgt.model.viewModel.ItemModerationView;
import org.apache.log4j.Logger;
import org.hyperledger.fabric.sdk.BlockEvent;
import org.hyperledger.fabric.sdk.ChaincodeResponse;
import org.hyperledger.fabric.sdk.ProposalResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@Service
public class InstructionService extends BaseService {

    private static final Logger logger = Logger.getLogger(InstructionService.class);

    private static EnumMap<Instruction.InstructionFunction, String> chaincodeFnMap = new EnumMap<>(Instruction.InstructionFunction.class);

    private @Value("${hf.chaincode.instruction.chaincodeId}") String chaincodeInstructionId;
    private @Value("${hf.chaincode.instruction.create}") String create;
    private @Value("${hf.chaincode.instruction.update}") String update;
    private @Value("${hf.chaincode.instruction.delete}") String delete;
    private @Value("${hf.chaincode.instruction.getByExamId}") String getByExamId;
    private @Value("${hf.chaincode.instruction.getById}") String getById;
    private @Value("${hf.chaincode.instruction.getInstructionHistoryById}") String getHistoryById;

    @Autowired private ItemModerationService itemModerationService;

    @PostConstruct
    public void init() {
        initChaincodeFnMap();
    }

    private void initChaincodeFnMap() {
        chaincodeFnMap.put(Instruction.InstructionFunction.CREATE, create);
        chaincodeFnMap.put(Instruction.InstructionFunction.UPDATE, update);
        chaincodeFnMap.put(Instruction.InstructionFunction.DELETE, delete);
        chaincodeFnMap.put(Instruction.InstructionFunction.GET_BY_EXAM_ID, getByExamId);
        chaincodeFnMap.put(Instruction.InstructionFunction.GET_BY_ID, getById);
       chaincodeFnMap.put(Instruction.InstructionFunction.GET_HISTORY_BY_ID, getHistoryById);
    }

    public List<InstructionView> convertToInstructionViews(UserSession userSession,
                                                           List<Instruction> instructions) {
        if (instructions.isEmpty()) {
            return new ArrayList<>();
        }

        ItemModeration itemModeration;
        List<ItemModeration> moderations = new ArrayList<>();
        for (Instruction instruction : instructions) {
            itemModeration = itemModerationService.getModerationByItem(
                    userSession, ItemModeration.ItemType.INSTRUCTION, instruction.getId()
            );
            if (itemModeration != null) {
                moderations.add(itemModeration);
            }
        }

        List<ItemModerationView> moderationViews = itemModerationService.convertToItemModerationView(
                moderations
        );
        HashMap<String, ItemModerationView> moderationViewHashMap = new HashMap<>();
        for (ItemModerationView moderationView : moderationViews) {
            moderationViewHashMap.put(moderationView.getItemId(), moderationView);
        }

        List<InstructionView> instructionViews = new ArrayList<>();
        for (Instruction instruction : instructions) {
            InstructionView instructionView = new InstructionView();
            instructionView.setId(instruction.getId());
            instructionView.setExamId(instruction.getExamId());
            instructionView.setDisplayIndex(instruction.getDisplayIndex());
            instructionView.setType(instruction.getType());
            instructionView.setText(instruction.getText());
            instructionView.setModerationDetails(
                    moderationViewHashMap.get(instruction.getId())
            );
            instructionView.setCreateDate(instruction.getCreateDate());
            instructionView.setLastUpdate(instruction.getLastUpdate());
            instructionView.setDeleted(instruction.isDeleted());
            instructionView.setDeleteDate(instruction.getDeleteDate());
            instructionView.setDocType(instruction.getDocType());

            instructionViews.add(instructionView);
        }
        return instructionViews;
    }

    public List<InstructionHistory> getInstructionHistoryById(UserSession userSession, String id) {
        try {
            ArrayList<String> args = new ArrayList<>();
            args.add(id);

            return unpackHFHistoryPayload(
                    queryChaincode(
                            userSession.getHfClient(),
                            userSession.getChannelExams(),
                            chaincodeInstructionId,
                            chaincodeFnMap.get(Instruction.InstructionFunction.GET_HISTORY_BY_ID),
                            args
                    )
            );

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public boolean deleteInstruction(UserSession userSession, String instructionId) {
        try {
            ArrayList<String> args = new ArrayList<>();
            args.add(String.valueOf(userSession.getId()));
            args.add(instructionId);

            //todo: handle completable future better
            try {
                BlockEvent.TransactionEvent transactionEvent = invokeChaincode(
                        userSession.getHfClient(),
                        userSession.getChannelExams(),
                        chaincodeInstructionId,
                        chaincodeFnMap.get(Instruction.InstructionFunction.DELETE),
                        args
                ).get(1, TimeUnit.SECONDS);

            } catch (TimeoutException ex) {
                //todo: see if possible to return updated obj
                return true;
            }
            throw new Exception("Unexpected error");

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return false;
        }
    }

    public List<Instruction> getInstructionsByExamId(UserSession userSession, String examId) {
        try {
            ArrayList<String> args = new ArrayList<>();
            args.add(examId);

            return unpackHFPayload(
                    queryChaincode(
                            userSession.getHfClient(),
                            userSession.getChannelExams(),
                            chaincodeInstructionId,
                            chaincodeFnMap.get(Instruction.InstructionFunction.GET_BY_EXAM_ID),
                            args
                    )
            );

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public Instruction getInstructionById(UserSession userSession, String id) {
        try {
            ArrayList<String> args = new ArrayList<>();
            args.add(id);

            List<Instruction> instructions = unpackHFPayload(
                    queryChaincode(
                            userSession.getHfClient(),
                            userSession.getChannelExams(),
                            chaincodeInstructionId,
                            chaincodeFnMap.get(Instruction.InstructionFunction.GET_BY_ID),
                            args
                    )
            );

            if (instructions.isEmpty()) {
                return null;
            }
            return instructions.get(0);
        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public boolean updateInstruction(UserSession userSession, Instruction instruction) {
        try {
            //todo: handle completable future better
            try {
                BlockEvent.TransactionEvent transactionEvent = invokeChaincode(
                        userSession.getHfClient(),
                        userSession.getChannelExams(),
                        chaincodeInstructionId,
                        chaincodeFnMap.get(Instruction.InstructionFunction.UPDATE),
                        getUpdateArgsList(userSession.getId(), instruction)
                ).get(1, TimeUnit.SECONDS);

            } catch (TimeoutException ex) {
                //todo: see if possible to return updated obj
                return true;
            }
            throw new Exception("Unexpected error");

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return false;
        }
    }

    public Instruction createInstruction(UserSession userSession, Instruction instruction) {
        try {
            String instructionId = getUUID();
            instruction.setId(instructionId);

            //todo: handle completable future better
            try {
                BlockEvent.TransactionEvent transactionEvent = invokeChaincode(
                        userSession.getHfClient(),
                        userSession.getChannelExams(),
                        chaincodeInstructionId,
                        chaincodeFnMap.get(Instruction.InstructionFunction.CREATE),
                        getCreateArgsList(instruction)
                ).get(1, TimeUnit.SECONDS);
            } catch (TimeoutException ex) {
                //todo: find way of returning inserted object?
                instruction.setCreateDate(new Timestamp(System.currentTimeMillis()).getTime());
                instruction.setLastUpdate(new Timestamp(System.currentTimeMillis()).getTime());
                instruction.setDocType("instruction");
                return instruction;
            }
            throw new Exception("Unexpected error occurred");

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    //todo: complete validation method
    public ValidationResponse validateInstruction(Instruction instruction) {
        ValidationResponse validationResponse = new ValidationResponse();
        try {
            validationResponse.setValid(true);
            validationResponse.setMessage("");
        } catch (Exception ex) {
            validationResponse.setValid(false);
            validationResponse.setMessage(ex.toString());
        }
        return validationResponse;
    }

    private List<InstructionHistory> unpackHFHistoryPayload(Collection<ProposalResponse> proposalResponses) throws Exception {
        List<InstructionHistory> instructionHistories = new ArrayList<>();

        String payload;
        for (ProposalResponse response : proposalResponses) {
            payload = new String(response.getChaincodeActionResponsePayload());
            logger.info("response: " + payload);

            if (response.isVerified()) {
                if (response.getStatus() == ChaincodeResponse.Status.SUCCESS) {
                    HFHistoryResponse[] responses = gson.fromJson(payload, HFHistoryResponse[].class);

                    for (HFHistoryResponse r : responses) {
                        Instruction instruction = gson.fromJson(r.getValue(), Instruction.class);

                        InstructionHistory instructionHistory = new InstructionHistory();
                        instructionHistory.setTxId(r.getTxId());
                        instructionHistory.setValue(instruction);
                        instructionHistory.setTimestamp(r.getTimestamp());
                        instructionHistory.setDelete(r.isDelete());

                        instructionHistories.add(instructionHistory);
                    }

                } else {
                    logger.error("response failed: status=" + response.getStatus() + ", msg=" + payload);
                }

            } else {
                logger.error("response unverified: status=" + response.getStatus() + ", msg=" + payload);
            }

        }

        return instructionHistories;
    }

    private List<Instruction> unpackHFPayload(Collection<ProposalResponse> proposalResponses) throws Exception {
        List<Instruction> instructions = new ArrayList<>();

        for (ProposalResponse response : proposalResponses) {
            String payload = new String(response.getChaincodeActionResponsePayload());
            logger.info("response: " + payload);

            if (response.getStatus() == ChaincodeResponse.Status.SUCCESS) {
                if (response.isVerified()) {
                    HFResponse[] responses = gson.fromJson(payload, HFResponse[].class);

                    for (HFResponse r : responses) {
                        Instruction instruction = gson.fromJson(r.getRecord(), Instruction.class);
                        instructions.add(instruction);
                    }

                } else {
                    logger.error("Response unverified: status=" + response.getStatus() + ", message=" + payload);
                }
            } else {
                logger.error("Response unsuccessful: status=" + response.getStatus() + ", message=" + payload);
            }
        }
        return instructions;
    }

    private ArrayList<String> getUpdateArgsList(Long lecturerId, Instruction instruction) {
        //order of args should match order of chaincode params
        ArrayList<String> args = new ArrayList<>();
        args.add(instruction.getId());
        args.add(String.valueOf(lecturerId));
        args.add(String.valueOf(instruction.getDisplayIndex()));
        args.add(instruction.getType());
        args.add(instruction.getText());
        return args;
    }

    private ArrayList<String> getCreateArgsList(Instruction instruction) {
        //order of args should match order of chaincode params
        ArrayList<String> args = new ArrayList<>();
        args.add(instruction.getId());
        args.add(instruction.getExamId());
        args.add(String.valueOf(instruction.getDisplayIndex()));
        args.add(instruction.getType());
        args.add(instruction.getText());
        return args;
    }
}
