package com.example.examMgt.service;

import com.example.examMgt.model.HFResponse;
import com.example.examMgt.model.UserSession;
import com.example.examMgt.model.db.ExamCentreStaff;
import com.example.examMgt.model.db.Lecturer;
import com.example.examMgt.model.hf.ExamGeneration;
import com.example.examMgt.model.viewModel.ExamGenerationView;
import org.apache.log4j.Logger;
import org.hyperledger.fabric.sdk.BlockEvent;
import org.hyperledger.fabric.sdk.ChaincodeResponse;
import org.hyperledger.fabric.sdk.ProposalResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@Service
public class ExamGenerationService extends BaseService {

    private static final Logger logger = Logger.getLogger(ExamGenerationService.class);

    private @Value("${hf.chaincode.examGeneration.chaincodeId}") String chaincodeId;
    private @Value("${hf.chaincode.examGeneration.create}") String create;
    private @Value("${hf.chaincode.examGeneration.update}") String update;
    private @Value("${hf.chaincode.examGeneration.getByExamId}") String getByExamId;
    private @Value("${hf.chaincode.examGeneration.getByExamCentreStaffId}") String getByExamCentreStaffId;
    private @Value("${hf.chaincode.examGeneration.getAll}") String getAll;

    private EnumMap<ExamGeneration.ExamGenerationFn, String> chaincodeFnMap = new EnumMap<>(ExamGeneration.ExamGenerationFn.class);

    @Autowired private ExamCentreStaffService examCentreStaffService;
    @Autowired private LecturerService lecturerService;

    @PostConstruct
    public void init() {
        initChaincodeFnMap();
    }

    private void initChaincodeFnMap() {
        chaincodeFnMap.put(ExamGeneration.ExamGenerationFn.CREATE, create);
        chaincodeFnMap.put(ExamGeneration.ExamGenerationFn.UPDATE, update);
        chaincodeFnMap.put(ExamGeneration.ExamGenerationFn.GET_BY_EXAM_ID, getByExamId);
        chaincodeFnMap.put(ExamGeneration.ExamGenerationFn.GET_BY_EXAM_CENTRE_STAFF_ID, getByExamCentreStaffId);
        chaincodeFnMap.put(ExamGeneration.ExamGenerationFn.GET_ALL, getAll);
    }

    public List<ExamGenerationView> convertGenerationsToGenerationViews(List<ExamGeneration> examGenerations) {
        if (examGenerations.isEmpty()) {
            return new ArrayList<>();
        }

        HashSet<Long> examCentreStaffIds = new HashSet<>();
        HashSet<Long> lecturerIds = new HashSet<>();

        Long staffId;
        Long lecId;
        for (ExamGeneration examGeneration : examGenerations) {
            staffId = examGeneration.getExamCentreStaffId();
            if (!examCentreStaffIds.contains(staffId)) {
                examCentreStaffIds.add(staffId);
            }

            lecId = examGeneration.getLecturerId();
            if (!lecturerIds.contains(lecId)) {
                lecturerIds.add(lecId);
            }
        }

        List<ExamCentreStaff> staffList = examCentreStaffService.getExamCentrStaffByIds(
               new ArrayList<>(examCentreStaffIds));
        List<Lecturer> lecturers = lecturerService.getLecturersByIds(
                new ArrayList<>(lecturerIds));

        HashMap<Long, ExamCentreStaff> examCentreStaffHashMap = new HashMap<>();
        HashMap<Long, Lecturer> lecturerHashMap = new HashMap<>();
        for (ExamCentreStaff staff : staffList) {
            examCentreStaffHashMap.put(staff.getId(), staff);
        }
        for (Lecturer lecturer : lecturers) {
            lecturerHashMap.put(lecturer.getId(), lecturer);
        }

        String staffFirstName;
        String staffSurname;
        String lecFirstName;
        String lecSurname;
        List<ExamGenerationView> examGenerationViews = new ArrayList<>();

        for (ExamGeneration examGeneration : examGenerations) {
            staffFirstName = examCentreStaffHashMap.get(examGeneration.getExamCentreStaffId()).getFirstName();
            staffSurname = examCentreStaffHashMap.get(examGeneration.getExamCentreStaffId()).getSurname();
            lecFirstName = lecturerHashMap.get(examGeneration.getLecturerId()).getFirstName();
            lecSurname = lecturerHashMap.get(examGeneration.getLecturerId()).getSurname();

            ExamGenerationView examGenerationView = new ExamGenerationView();
            examGenerationView.setExamId(examGeneration.getExamId());
            examGenerationView.setExamCentreStaffId(examGeneration.getExamCentreStaffId());
            examGenerationView.setExamCentreStaffFirstName(staffFirstName);
            examGenerationView.setExamCentreStaffSurname(staffSurname);
            examGenerationView.setExamCentreStatus(examGeneration.isExamCentreStatus());
            examGenerationView.setExamCentreNumCopies(examGeneration.getExamCentreNumCopies());
            examGenerationView.setLecturerId(examGeneration.getLecturerId());
            examGenerationView.setLecturerFirstName(lecFirstName);
            examGenerationView.setLecturerSurname(lecSurname);
            examGenerationView.setLecturerStatus(examGeneration.isLecturerStatus());
            examGenerationView.setLecturerNumCopies(examGeneration.getLecturerNumCopies());
            examGenerationView.setCreateDate(examGeneration.getCreateDate());
            examGenerationView.setLastUpdate(examGeneration.getLastUpdate());
            examGenerationView.setDocType(examGeneration.getDocType());

            examGenerationViews.add(examGenerationView);
        }

        return examGenerationViews;
    }

    public List<ExamGeneration> getAllExamGenerations(UserSession userSession) {
        try {
            return unpackHFPayload(
                    queryChaincode(
                            userSession.getHfClient(),
                            userSession.getChannelExams(),
                            chaincodeId,
                            chaincodeFnMap.get(ExamGeneration.ExamGenerationFn.GET_ALL),
                            new ArrayList<>()
                    )
            );

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public List<ExamGeneration> getByExamCentreStaffId(UserSession userSession) {
        try {
            ArrayList<String> args = new ArrayList<>();
            args.add(String.valueOf(userSession.getId()));

            List<ExamGeneration> examGenerations = unpackHFPayload(
                    queryChaincode(
                            userSession.getHfClient(),
                            userSession.getChannelExams(),
                            chaincodeId,
                            chaincodeFnMap.get(ExamGeneration.ExamGenerationFn.GET_BY_EXAM_CENTRE_STAFF_ID),
                            args
                    )
            );

            return examGenerations;
        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public ExamGeneration getExamGenerationByExamId(UserSession userSession, String examId) {
        try {
            ArrayList<String> args = new ArrayList<>();
            args.add(examId);

            List<ExamGeneration> examGenerations = unpackHFPayload(
                    queryChaincode(
                            userSession.getHfClient(),
                            userSession.getChannelExams(),
                            chaincodeId,
                            chaincodeFnMap.get(ExamGeneration.ExamGenerationFn.GET_BY_EXAM_ID),
                            args
                    )
            );

            if (examGenerations.isEmpty()) {
                return null;
            }

            return examGenerations.get(0);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public boolean updateExamGeneration(UserSession userSession, ExamGeneration examGeneration) {
        try {
            //todo: handle completable future better
            try {
                BlockEvent.TransactionEvent transactionEvent = invokeChaincode(
                        userSession.getHfClient(),
                        userSession.getChannelExams(),
                        chaincodeId,
                        chaincodeFnMap.get(ExamGeneration.ExamGenerationFn.UPDATE),
                        getUpdateArgsList(examGeneration, userSession)
                ).get(1, TimeUnit.SECONDS);
            } catch (TimeoutException ex) {
                //todo: find way of returning inserted object?
                return true;
            }
            throw new Exception("Unexpected error occurred");


        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return false;
        }
    }

    public ExamGeneration createExamGeneration(UserSession userSession, ExamGeneration examGeneration) {
        try {
            try {
                //todo: handle completable future better
                BlockEvent.TransactionEvent transactionEvent = invokeChaincode(
                        userSession.getHfClient(),
                        userSession.getChannelExams(),
                        chaincodeId,
                        chaincodeFnMap.get(ExamGeneration.ExamGenerationFn.CREATE),
                        getCreateArgsList(examGeneration)
                ).get(1, TimeUnit.SECONDS);
            } catch (TimeoutException ex) {
                examGeneration.setCreateDate(System.currentTimeMillis());
                examGeneration.setLastUpdate(System.currentTimeMillis());
                return examGeneration;
            }
            throw new Exception("Unexpected error occurred");

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    private List<ExamGeneration> unpackHFPayload(Collection<ProposalResponse> proposalResponses) throws Exception {
        List<ExamGeneration> examGenerations = new ArrayList<>();

        String payload;
        for (ProposalResponse response : proposalResponses) {
            payload = new String(response.getChaincodeActionResponsePayload());
            logger.info("response: " + payload);

            if (response.isVerified()) {
                if (response.getStatus() == ChaincodeResponse.Status.SUCCESS) {
                    HFResponse[] responses = gson.fromJson(payload, HFResponse[].class);

                    for (HFResponse r : responses) {
                        ExamGeneration examGeneration = gson.fromJson(r.getRecord(), ExamGeneration.class);
                        examGenerations.add(examGeneration);
                    }

                } else {
                    logger.error("Response failed: status=" + response.getStatus() + ", msg=" + payload);
                }

            } else {
                logger.error("Response unverified: status=" + response.getStatus() + ", msg=" + payload);
            }
        }
        return examGenerations;
    }

    private ArrayList<String> getUpdateArgsList(ExamGeneration examGeneration, UserSession userSession) {
        ArrayList<String> args = new ArrayList<>();
        args.add(examGeneration.getExamId());
        args.add(String.valueOf(userSession.getUserType()));
        args.add(String.valueOf(userSession.getId()));
        if (UserSession.UserType.LECTURER == userSession.getUserType()) {
            args.add(String.valueOf(examGeneration.isLecturerStatus()));
            args.add(String.valueOf(examGeneration.getLecturerNumCopies()));
        }
        if (UserSession.UserType.EXAM_CENTRE_STAFF == userSession.getUserType()) {
            args.add(String.valueOf(examGeneration.isExamCentreStatus()));
            args.add(String.valueOf(examGeneration.getExamCentreNumCopies()));
        }
        return args;
    }

    private ArrayList<String> getCreateArgsList(ExamGeneration examGeneration) {
        ArrayList<String> args = new ArrayList<>();
        args.add(examGeneration.getExamId());
        args.add(String.valueOf(examGeneration.getExamCentreStaffId()));
        args.add(String.valueOf(examGeneration.isExamCentreStatus()));
        args.add(String.valueOf(examGeneration.getExamCentreNumCopies()));
        args.add(String.valueOf(examGeneration.getLecturerId()));
        args.add(String.valueOf(examGeneration.isLecturerStatus()));
        args.add(String.valueOf(examGeneration.getLecturerNumCopies()));
        return args;
    }

}
