package com.example.examMgt.service;

import com.google.common.base.Charsets;
import com.google.gson.Gson;
import org.apache.log4j.Logger;
import org.hyperledger.fabric.sdk.*;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

public class BaseService {

    private static final Logger logger = Logger.getLogger(BaseService.class);

    protected Gson gson = new Gson();

    protected String getUUID() {
        String source = "org1.example.org" + String.valueOf(new Date().getTime());
        byte[] bytes = source.getBytes(Charsets.UTF_8);
        UUID uuid = UUID.nameUUIDFromBytes(bytes);
        return uuid.toString();
    }

    protected CompletableFuture<BlockEvent.TransactionEvent> invokeChaincode(
            HFClient hfClient,
            Channel channel,
            String chaincodeId,
            String fnName,
            ArrayList<String> args
    ) throws Exception {
        logger.debug("Executing invokeChaincode() hfClient=" + hfClient + ", channel=" + channel +
        "chaincodeId=" + chaincodeId + ", fnname=" + fnName + ", args=" + args);

        if (hfClient == null) {
            throw new Exception("HfClient cannot be null");
        }
        if (channel == null) {
            throw new Exception("Channel cannot be null");
        }
        if (chaincodeId.isEmpty()) {
            throw new Exception("ChaincodeId cannot be empty");
        }
        if (fnName.isEmpty()) {
            throw new Exception("FnName cannot be empty");
        }

        ChaincodeID chaincodeID = ChaincodeID.newBuilder().setName(chaincodeId).build();
        TransactionProposalRequest transactionProposalRequest = hfClient.newTransactionProposalRequest();
        transactionProposalRequest.setChaincodeID(chaincodeID);
        transactionProposalRequest.setFcn(fnName);
        transactionProposalRequest.setArgs(args);

        Collection<ProposalResponse> responses = channel.sendTransactionProposal(transactionProposalRequest);
        List<ProposalResponse> invalidResponses = responses.stream().filter(r -> r.isInvalid()).collect(Collectors.toList());

        if (!invalidResponses.isEmpty()) {
            for (ProposalResponse response : invalidResponses) {
                logger.error(response.getMessage());
            }
            throw new RuntimeException("invalid response(s) found");
        }

        return channel.sendTransaction(responses, hfClient.getUserContext());
    }

   protected Collection<ProposalResponse> queryChaincode(
           HFClient hfClient,
           Channel channel,
           String chaincodeId,
           String fnName,
           ArrayList<String> args
   ) throws Exception {
       if (hfClient == null) {
           throw new Exception("HfClient cannot be null");
       }
       if (channel == null) {
           throw new Exception("Channel cannot be null");
       }
       if (chaincodeId.isEmpty()) {
           throw new Exception("ChaincodeId cannot be empty");
       }
       if (fnName.isEmpty()) {
           throw new Exception("FnName cannot be empty");
       }

       ChaincodeID chaincodeID = ChaincodeID.newBuilder().setName(chaincodeId).build();
       QueryByChaincodeRequest queryByChaincodeRequest = hfClient.newQueryProposalRequest();
       queryByChaincodeRequest.setChaincodeID(chaincodeID);
       queryByChaincodeRequest.setFcn(fnName);

       if (!args.isEmpty()) {
           queryByChaincodeRequest.setArgs(args);
       }
       return channel.queryByChaincode(queryByChaincodeRequest);
   }
}
