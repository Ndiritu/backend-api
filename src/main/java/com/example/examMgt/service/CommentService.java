package com.example.examMgt.service;

import com.example.examMgt.model.hf.ItemModeration;
import com.example.examMgt.model.hf.history.HFHistoryResponse;
import com.example.examMgt.model.HFResponse;
import com.example.examMgt.model.UserSession;
import com.example.examMgt.model.ValidationResponse;
import com.example.examMgt.model.db.ExternalExaminer;
import com.example.examMgt.model.db.Lecturer;
import com.example.examMgt.model.hf.Comment;
import com.example.examMgt.model.hf.history.CommentHistory;
import com.example.examMgt.model.viewModel.CommentHistoryView;
import com.example.examMgt.model.viewModel.CommentView;
import org.apache.log4j.Logger;
import org.hyperledger.fabric.sdk.BlockEvent;
import org.hyperledger.fabric.sdk.ChaincodeResponse;
import org.hyperledger.fabric.sdk.ProposalResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@Service
public class CommentService extends BaseService {

    private static final Logger logger = Logger.getLogger(CommentService.class);

    private @Value("${hf.chaincode.comment.chaincodeId}") String chaincodeCommentId;

    private static EnumMap<Comment.CommentFunction, String> chaincodeFnMap = new EnumMap<>(Comment.CommentFunction.class);

    private @Value("${hf.chaincode.comment.create}") String create;
    private @Value("${hf.chaincode.comment.getByItem}") String getByItem;
    private @Value("${hf.chaincode.comment.getByExam}") String getbyExam;
    private @Value("${hf.chaincode.comment.getBySender}") String getBySender;
    private @Value("${hf.chaincode.comment.getByRecipient}") String getByRecipient;
    private @Value("${hf.chaincode.comment.delete}") String delete;
    private @Value("${hf.chaincode.comment.getAll}") String getAll;
    private @Value("${hf.chaincode.comment.getHistory}") String getHistory;
    private @Value("${hf.chaincode.comment.getById}") String getById;

    @Autowired
    private LecturerService lecturerService;

    @Autowired
    private ExternalExaminerService externalExaminerService;

    @PostConstruct
    public void init() {
        initChaincodeFnMap();
    }

    private void initChaincodeFnMap() {
        chaincodeFnMap.put(Comment.CommentFunction.CREATE, create);
        chaincodeFnMap.put(Comment.CommentFunction.GET_BY_ITEM, getByItem);
        chaincodeFnMap.put(Comment.CommentFunction.GET_BY_EXAM, getbyExam);
        chaincodeFnMap.put(Comment.CommentFunction.GET_BY_SENDER, getBySender);
        chaincodeFnMap.put(Comment.CommentFunction.GET_BY_RECIPIENT, getByRecipient);
        chaincodeFnMap.put(Comment.CommentFunction.DELETE, delete);
        chaincodeFnMap.put(Comment.CommentFunction.GET_ALL, getAll);
        chaincodeFnMap.put(Comment.CommentFunction.GET_HISTORY, getHistory);
        chaincodeFnMap.put(Comment.CommentFunction.GET_BY_ID, getById);
    }

    public Comment getCommentById(UserSession userSession, String commentId) {
        try {
            ArrayList<String> args = new ArrayList<>();
            args.add(commentId);

            List<Comment> comments = unpackHFPayload(
                    queryChaincode(
                            userSession.getHfClient(),
                            userSession.getChannelExams(),
                            chaincodeCommentId,
                            chaincodeFnMap.get(Comment.CommentFunction.GET_BY_ID),
                            args
                    )
            );

            if (comments.isEmpty()) {
                return null;
            }
            return comments.get(0);

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public List<CommentHistoryView> convertCommentHistoryToCommentHistoryView(
            List<CommentHistory> commentHistoryList
    ) {
        if (commentHistoryList.isEmpty()) {
            return new ArrayList<>();
        }

        List<Comment> comments = new ArrayList<>();
        for (CommentHistory commentHistory : commentHistoryList) {
            comments.add(commentHistory.getValue());
        }

        List<CommentView> commentViews = convertToCommentView(comments);
        HashMap<String, CommentView> commentIdCommentViewMap = new HashMap<>();
        for (CommentView commentView : commentViews) {
            commentIdCommentViewMap.put(commentView.getId(), commentView);
        }

        List<CommentHistoryView> commentHistoryViews = new ArrayList<>();
        for (CommentHistory commentHistory : commentHistoryList) {
            CommentView commentView = commentIdCommentViewMap.get(
                    commentHistory.getValue().getId()
            );

            CommentHistoryView commentHistoryView = new CommentHistoryView();
            commentHistoryView.setTxId(commentHistory.getTxId());
            commentHistoryView.setValue(commentView);
            commentHistoryView.setTimestamp(commentHistory.getTimestamp());
            commentHistoryView.setDelete(commentHistory.isDelete());

            commentHistoryViews.add(commentHistoryView);
        }

        return commentHistoryViews;
    }

    public List<CommentView> convertToCommentView(List<Comment> comments) {
        if (comments.isEmpty()) {
            return new ArrayList<>();
        }

        HashSet<Long> lecturerIds = new HashSet<>();
        HashSet<Long> extExaminerIds = new HashSet<>();

        for (Comment comment : comments) {
            //handle sender types
            long senderId = comment.getSenderId();
            Comment.SenderType senderType = Comment.SenderType.valueOf(comment.getSenderType());
            if (senderType == Comment.SenderType.LECTURER) {
                if (!lecturerIds.contains(senderId)) {
                    lecturerIds.add(senderId);
                }
            }
            if (senderType == Comment.SenderType.EXT_EXAMINER) {
                if (!extExaminerIds.contains(senderId)) {
                    extExaminerIds.add(senderId);
                }
            }
            //handle recipient types
            Comment.RecipientType recipientType = Comment.RecipientType.valueOf(comment.getRecipientType());
            long recipientId = comment.getRecipientId();
            if (recipientType == Comment.RecipientType.LECTURER) {
                if (!lecturerIds.contains(recipientId)) {
                    lecturerIds.add(recipientId);
                }
            }
            if (recipientType == Comment.RecipientType.EXT_EXAMINER) {
                if (!extExaminerIds.contains(recipientId)) {
                    extExaminerIds.add(recipientId);
                }
            }
        }

        List<Lecturer> lecturers = lecturerService.getLecturersByIds(new ArrayList<>(lecturerIds));
        List<ExternalExaminer> externalExaminers = externalExaminerService.getExtExaminersByIds(
                new ArrayList<>(extExaminerIds)
        );

        HashMap<Long, Lecturer> lecIdMap = new HashMap<>();
        HashMap<Long, ExternalExaminer> extExaminerIdMap = new HashMap<>();

        for (Lecturer lec : lecturers) {
            lecIdMap.put(lec.getId(), lec);
        }
        for (ExternalExaminer externalExaminer : externalExaminers) {
            extExaminerIdMap.put(externalExaminer.getId(), externalExaminer);
        }

        List<CommentView> commentViews = new ArrayList<>();
        for (Comment comment : comments) {
            CommentView commentView = new CommentView();
            commentView.setId(comment.getId());
            commentView.setText(comment.getText());
            commentView.setSenderId(comment.getSenderId());
            commentView.setSenderType(Comment.SenderType.valueOf(comment.getSenderType()));

            if (commentView.getSenderType() == Comment.SenderType.LECTURER) {
                Lecturer lecturer = lecIdMap.get(comment.getSenderId());
                commentView.setSenderFirstName(lecturer.getFirstName());
                commentView.setSenderSurname(lecturer.getSurname());
            }
            if (commentView.getSenderType() == Comment.SenderType.EXT_EXAMINER) {
                ExternalExaminer externalExaminer = extExaminerIdMap.get(comment.getSenderId());
                commentView.setSenderFirstName(externalExaminer.getFirstName());
                commentView.setSenderSurname(externalExaminer.getSurname());
            }

            commentView.setRecipientId(comment.getRecipientId());
            commentView.setRecipientType(Comment.RecipientType.valueOf(comment.getRecipientType()));

            if (commentView.getRecipientType() == Comment.RecipientType.LECTURER) {
                Lecturer lecturer = lecIdMap.get(comment.getRecipientId());
                commentView.setRecipientFirstName(lecturer.getFirstName());
                commentView.setRecipientSurname(lecturer.getSurname());
            }
            if (commentView.getRecipientType() == Comment.RecipientType.EXT_EXAMINER) {
                ExternalExaminer externalExaminer = extExaminerIdMap.get(comment.getRecipientId());
                commentView.setRecipientFirstName(externalExaminer.getFirstName());
                commentView.setRecipientSurname(externalExaminer.getSurname());
            }

            commentView.setItemId(comment.getItemId());
            commentView.setItemType(ItemModeration.ItemType.valueOf(comment.getItemType()));
            commentView.setExamId(comment.getExamId());
            commentView.setCreateDate(comment.getCreateDate());
            commentView.setLastUpdate(comment.getLastUpdate());
            commentView.setDeleted(comment.isDeleted());
            commentView.setDeleteDate(comment.getDeleteDate());

            commentViews.add(commentView);
        }
        return commentViews;
    }

    public boolean deleteComment(UserSession userSession, String commentId) {
        try {
            ArrayList<String> args = new ArrayList<>();
            args.add(String.valueOf(userSession.getId()));
            args.add(commentId);

            //todo: handle completable future better
            try {
                BlockEvent.TransactionEvent transactionEvent = invokeChaincode(
                        userSession.getHfClient(),
                        userSession.getChannelExams(),
                        chaincodeCommentId,
                        chaincodeFnMap.get(Comment.CommentFunction.DELETE),
                        args
                ).get(1, TimeUnit.SECONDS);
            } catch (TimeoutException ex) {
                return true;
            }
            throw new Exception("Unexpected error occurred");


        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return false;
        }
    }

    public List<Comment> getAllComments(UserSession userSession) {
        try {
            return unpackHFPayload(
                    queryChaincode(
                            userSession.getHfClient(),
                            userSession.getChannelExams(),
                            chaincodeCommentId,
                            chaincodeFnMap.get(Comment.CommentFunction.GET_ALL),
                            new ArrayList<>()
                    )
            );

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public List<CommentHistory> getCommentHistoryById(UserSession userSession, String commentId) {
        try {
            ArrayList<String> args = new ArrayList<>();
            args.add(commentId);

            return unpackHFHistoryPayload(
                    queryChaincode(
                            userSession.getHfClient(),
                            userSession.getChannelExams(),
                            chaincodeCommentId,
                            chaincodeFnMap.get(Comment.CommentFunction.GET_HISTORY),
                            args
                    )
            );

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public List<Comment> getCommentsByRecipient(UserSession userSession, Comment.RecipientType recipientType, long recipientId) {
        try {
            ArrayList<String> args = new ArrayList<>();
            args.add(String.valueOf(recipientType));
            args.add(String.valueOf(recipientId));

            return unpackHFPayload(
                    queryChaincode(
                            userSession.getHfClient(),
                            userSession.getChannelExams(),
                            chaincodeCommentId,
                            chaincodeFnMap.get(Comment.CommentFunction.GET_BY_RECIPIENT),
                            args
                    )
            );

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public List<Comment> getCommentsBySender(UserSession userSession, Comment.SenderType senderType, long senderId) {
        try {
            ArrayList<String> args = new ArrayList<>();
            args.add(String.valueOf(senderType));
            args.add(String.valueOf(senderId));

            return unpackHFPayload(
                    queryChaincode(
                            userSession.getHfClient(),
                            userSession.getChannelExams(),
                            chaincodeCommentId,
                            chaincodeFnMap.get(Comment.CommentFunction.GET_BY_SENDER),
                            args
                    )
            );

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public List<Comment> getCommentsByExam(UserSession userSession, String examId) {
        try {
            ArrayList<String> args = new ArrayList<>();
            args.add(examId);

            return unpackHFPayload(
                    queryChaincode(
                            userSession.getHfClient(),
                            userSession.getChannelExams(),
                            chaincodeCommentId,
                            chaincodeFnMap.get(Comment.CommentFunction.GET_BY_EXAM),
                            args
                    )
            );

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public List<Comment> getCommentsByItem(UserSession userSession, ItemModeration.ItemType itemType, String itemId) {
        try {
            ArrayList<String> args = new ArrayList<>();
            args.add(String.valueOf(itemType));
            args.add(itemId);

            return unpackHFPayload(
                    queryChaincode(
                            userSession.getHfClient(),
                            userSession.getChannelExams(),
                            chaincodeCommentId,
                            chaincodeFnMap.get(Comment.CommentFunction.GET_BY_ITEM),
                            args
                    )
            );

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public Comment createComment(UserSession userSession, Comment comment) {
        try {
            logger.debug("Executing createComment() comment=" + comment);
            comment.setId(getUUID());

            //todo: handle completable future better
            try {
                BlockEvent.TransactionEvent transactionEvent = invokeChaincode(
                        userSession.getHfClient(),
                        userSession.getChannelExams(),
                        chaincodeCommentId,
                        chaincodeFnMap.get(Comment.CommentFunction.CREATE),
                        getCreateArgsList(comment)
                ).get(1, TimeUnit.SECONDS);
            } catch (TimeoutException ex) {
                //todo: find way of returning inserted object?
                comment.setCreateDate(System.currentTimeMillis());
                comment.setLastUpdate(System.currentTimeMillis());
                return comment;
            }
            throw new Exception("Unexpected error occurred");

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    private List<CommentHistory> unpackHFHistoryPayload(Collection<ProposalResponse> proposalResponses) throws Exception {
        List<CommentHistory> commentHistoryList = new ArrayList<>();

        String payload;
        for (ProposalResponse response : proposalResponses) {
            payload = new String(response.getChaincodeActionResponsePayload());
            logger.info("response: " + payload);

            if (response.getStatus() == ChaincodeResponse.Status.SUCCESS) {
                if (response.isVerified()) {
                    HFHistoryResponse[] responses = gson.fromJson(payload, HFHistoryResponse[].class);

                    for (HFHistoryResponse r : responses) {
                        Comment comment = gson.fromJson(r.getValue(), Comment.class);

                        CommentHistory commentHistory = new CommentHistory();
                        commentHistory.setTxId(r.getTxId());
                        commentHistory.setValue(comment);
                        commentHistory.setTimestamp(r.getTimestamp());
                        commentHistory.setDelete(r.isDelete());

                        commentHistoryList.add(commentHistory);
                    }

                } else {
                    logger.error("response unverified: status=" + response.getStatus() + ", msg=" + payload);
                }

            } else {
                logger.error("response unsuccessful: status=" + response.getStatus() + ", msg=" + payload);
            }
        }
        return commentHistoryList;
    }

    private List<Comment> unpackHFPayload(Collection<ProposalResponse> proposalResponses) throws Exception {
        List<Comment> comments = new ArrayList<>();

        String payload;
        for (ProposalResponse response : proposalResponses) {
            payload = new String(response.getChaincodeActionResponsePayload());
            logger.info("response=" + payload);

            if (response.getStatus() == ChaincodeResponse.Status.SUCCESS) {
                if (response.isVerified()) {
                    HFResponse[] responses = gson.fromJson(payload, HFResponse[].class);

                    for (HFResponse r : responses) {
                        Comment comment = gson.fromJson(r.getRecord(), Comment.class);
                        comments.add(comment);
                    }
                } else {
                    logger.error("Response unverified: status=" + response.getStatus() + ", msg=" + payload);
                }
            } else {
                logger.error("Response unsuccessful: status=" + response.getStatus() + ", msg=" + payload);
            }
        }

        return comments;
    }

    private ArrayList<String> getCreateArgsList(Comment comment) {
        ArrayList<String> args = new ArrayList<>();
        args.add(comment.getId());
        args.add(comment.getText());
        args.add(comment.getSenderType());
        args.add(String.valueOf(comment.getSenderId()));
        args.add(comment.getRecipientType());
        args.add(String.valueOf(comment.getRecipientId()));
        args.add(comment.getItemType());
        args.add(comment.getItemId());
        args.add(comment.getExamId());
        return args;
    }

    //todo: Complete validation
    public ValidationResponse validateComment(Comment comment) {
        ValidationResponse validationResponse = new ValidationResponse();
        validationResponse.setMessage("");
        validationResponse.setValid(true);
        return validationResponse;
    }

}
