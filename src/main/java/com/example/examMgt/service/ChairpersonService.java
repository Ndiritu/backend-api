package com.example.examMgt.service;

import com.example.examMgt.model.viewModel.ChairpersonView;
import com.example.examMgt.utils.SqlUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ChairpersonService {

    private static final Logger logger = Logger.getLogger(ChairpersonService.class);

    @Autowired
    private JdbcTemplateService jdbcTemplateService;

    public ChairpersonView getChairpersonByEmail(String email) {
        try {
            Map<String, String> params = new HashMap<>();
            params.put("email", email);

            String sql = SqlUtils.instance.findSqlStatement("getChairpersonByEmail");
            ChairpersonViewCallbackHandler chairpersonViewCallbackHandler = new ChairpersonViewCallbackHandler();
            jdbcTemplateService.theACJdbcTemplate().query(sql, params, chairpersonViewCallbackHandler);

            ChairpersonView chairpersonView = chairpersonViewCallbackHandler.getChairpersonView();
            if (chairpersonView == null || chairpersonView.getId() == 0) {
                return null;
            }
            return chairpersonView;

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public ChairpersonView getCurrentChairperson() {
        try {
            String sql = SqlUtils.instance.findSqlStatement("getCurrentChairperson");
            ChairpersonViewCallbackHandler chairpersonViewCallbackHandler = new ChairpersonViewCallbackHandler();
            jdbcTemplateService.theACJdbcTemplate().query(sql, chairpersonViewCallbackHandler);

            ChairpersonView chairpersonView = chairpersonViewCallbackHandler.getChairpersonView();
            if (chairpersonView.getId() == 0) {
                return null;
            }
            return chairpersonView;

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public ChairpersonView getChairpersonById(long chairpersonId) {
        try {
            Map<String, Long> params = new HashMap<>();
            params.put("chairpersonId", chairpersonId);

            String sql = SqlUtils.instance.findSqlStatement("getChairpersonById");
            ChairpersonViewCallbackHandler chairpersonViewCallbackHandler = new ChairpersonViewCallbackHandler();
            jdbcTemplateService.theACJdbcTemplate().query(sql, params, chairpersonViewCallbackHandler);

            ChairpersonView chairpersonView = chairpersonViewCallbackHandler.getChairpersonView();
            if (chairpersonView == null || chairpersonView.getId() == 0) {
                return null;
            }
            return chairpersonView;

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }

    public List<ChairpersonView> getChairpersonsByIds(List<Long> ids) {
        try {
            Array chairIdsArr = SqlUtils.getPostgresBigintArray(ids, jdbcTemplateService.getDBConnection());
            Map<String, Object> params = new HashMap<>();
            params.put("ids", chairIdsArr);

            String sql = SqlUtils.instance.findSqlStatement("getChairpersonsById");
            return jdbcTemplateService.theACJdbcTemplate().query(sql, params, new ChairpersonViewRowMapper());

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return new ArrayList<>();
        }
    }

    private static class ChairpersonViewRowMapper implements RowMapper<ChairpersonView> {
        @Override
        public ChairpersonView mapRow(ResultSet rs, int i) throws SQLException {
            long id = rs.getLong("id");
            long lecturerId = rs.getLong("lecturer_id");
            String firstName = rs.getString("first_name");
            String otherNames = rs.getString("other_names");
            String surname = rs.getString("surname");
            String email = rs.getString("email");
            boolean active = rs.getBoolean("active");
            Timestamp startDate = rs.getTimestamp("start_date");
            Timestamp endDate = rs.getTimestamp("end_date");

            ChairpersonView chairpersonView = new ChairpersonView();
            chairpersonView.setId(id);
            chairpersonView.setLecturerId(lecturerId);
            chairpersonView.setFirstName(firstName);
            chairpersonView.setOtherNames(otherNames);
            chairpersonView.setSurname(surname);
            chairpersonView.setEmail(email);
            chairpersonView.setActive(active);
            chairpersonView.setStartDate(startDate);
            chairpersonView.setEndDate(endDate);
            return chairpersonView;
        }
    }

    private static class ChairpersonViewCallbackHandler implements RowCallbackHandler {
        private ChairpersonView chairpersonView;

        private ChairpersonViewCallbackHandler() {}

        public ChairpersonView getChairpersonView() {
            return chairpersonView;
        }

        private void setChairpersonView(ChairpersonView chairpersonView) {
            this.chairpersonView = chairpersonView;
        }

        @Override
        public void processRow(ResultSet rs) throws SQLException {
            long id = rs.getLong("id");
            long lecturerId = rs.getLong("lecturer_id");
            String firstName = rs.getString("first_name");
            String otherNames = rs.getString("other_names");
            String surname = rs.getString("surname");
            String email = rs.getString("email");
            boolean active = rs.getBoolean("active");
            Timestamp startDate = rs.getTimestamp("start_date");
            Timestamp endDate = rs.getTimestamp("end_date");

            ChairpersonView chairpersonView = new ChairpersonView();
            chairpersonView.setId(id);
            chairpersonView.setLecturerId(lecturerId);
            chairpersonView.setFirstName(firstName);
            chairpersonView.setOtherNames(otherNames);
            chairpersonView.setSurname(surname);
            chairpersonView.setEmail(email);
            chairpersonView.setActive(active);
            chairpersonView.setStartDate(startDate);
            chairpersonView.setEndDate(endDate);

            setChairpersonView(chairpersonView);
        }
    }
}
