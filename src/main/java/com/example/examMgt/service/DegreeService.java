package com.example.examMgt.service;

import com.example.examMgt.model.db.Degree;
import com.example.examMgt.utils.SqlUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@Service
public class DegreeService {

    private static final Logger logger = Logger.getLogger(DegreeService.class);

    @Autowired private JdbcTemplateService jdbcTemplateService;

    public Degree getDegreeById(long id) {
        try {
            Map<String, Long> params = new HashMap<>();
            params.put("id", id);

            String sql = SqlUtils.instance.findSqlStatement("getDegreeById");
            DegreeCallbackHandler degreeCallbackHandler = new DegreeCallbackHandler();
            jdbcTemplateService.theACJdbcTemplate().query(sql, params, degreeCallbackHandler);

            if (degreeCallbackHandler.getDegree() == null) {
                return null;
            }
            return degreeCallbackHandler.getDegree();

        } catch (Exception ex) {
            logger.error(ex.toString(), ex);
            return null;
        }
    }


    private static class DegreeCallbackHandler implements RowCallbackHandler {
        private Degree degree;

        public Degree getDegree() {
            return degree;
        }

        private void setDegree(Degree degree) {
            this.degree = degree;
        }

        @Override
        public void processRow(ResultSet rs) throws SQLException {
            long id = rs.getLong("id");
            String name = rs.getString("name");
            Degree.DegreeType degreeType = Degree.DegreeType.valueOf(rs.getString("type"));
            String degreeTypePrefix = rs.getString("type_prefix");
            Degree.DegreeLevel degreeLevel = Degree.DegreeLevel.valueOf(rs.getString("level"));

            Degree degree = new Degree();
            degree.setId(id);
            degree.setName(name);
            degree.setDegreeType(degreeType);
            degree.setDegreeTypePrefix(degreeTypePrefix);
            degree.setDegreeLevel(degreeLevel);

            setDegree(degree);
        }
    }

}
