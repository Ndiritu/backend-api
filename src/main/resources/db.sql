CREATE ROLE dba WITH LOGIN PASSWORD 'database';

DROP DATABASE IF EXISTS exam_mgt;
CREATE DATABASE exam_mgt
    ENCODING = 'UTF8'
    LC_COLLATE = 'en_US.UTF-8'
    LC_CTYPE = 'en_US.UTF-8';

\c exam_mgt;

GRANT ALL PRIVILEGES ON DATABASE exam_mgt to dba;
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO dba;
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT, INSERT, UPDATE, DELETE ON TABLES TO dba;
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT ALL ON SEQUENCES TO dba;

-- enum pattern [tablename_enum]

CREATE TYPE degree_type AS ENUM ('BACHELOR_OF_SCIENCE');
CREATE TYPE degree_level AS ENUM ('UNDERGRADUATE', 'POSTGRADUATE');
CREATE TYPE course_semester AS ENUM ('FIRST_SEMESTER', 'SECOND_SEMESTER');

CREATE TABLE university(
    id SERIAL PRIMARY KEY,
    name TEXT NOT NULL UNIQUE,
    logo TEXT NOT NULL
);

CREATE TABLE degree(
    id SERIAL PRIMARY KEY,
    name TEXT NOT NULL UNIQUE,
    type degree_type,
    type_prefix TEXT NOT NULL,
    level degree_level
);

CREATE TABLE course(
	id SERIAL PRIMARY KEY,
	name TEXT NOT NULL,
	course_code TEXT NOT NULL UNIQUE,
	year_of_study INT NOT NULL,
	semester course_semester,
	degree_id BIGINT REFERENCES degree(id)
);


CREATE TABLE student(
	id SERIAL PRIMARY KEY,
	first_name TEXT NOT NULL,
	other_names TEXT,
	surname TEXT NOT NULL,
	registration_number TEXT NOT NULL UNIQUE,
	natl_id_number BIGINT NOT NULL UNIQUE,
	email TEXT NOT NULL,
	password TEXT NOT NULL,
	year_of_study INT NOT NULL,
	create_date TIMESTAMP DEFAULT NOW(),
	last_update TIMESTAMP DEFAULT NOW()
);


CREATE TABLE student_course(
	student_id BIGINT REFERENCES student(id),
	course_id BIGINT REFERENCES course(id),
	create_date TIMESTAMP DEFAULT NOW(),
	PRIMARY KEY(student_id, course_id)
);


CREATE TABLE lecturer(
	id SERIAL PRIMARY KEY,
	first_name TEXT NOT NULL,
	other_names TEXT,
	surname TEXT NOT NULL,
	email TEXT NOT NULL UNIQUE,
	password TEXT,
	active BOOLEAN DEFAULT TRUE,
	create_date TIMESTAMP DEFAULT NOW(),
	last_update TIMESTAMP DEFAULT NOW()
);


CREATE TABLE lecturer_course(
	lecturer_id BIGINT REFERENCES lecturer(id),
	course_id BIGINT REFERENCES course(id),
	create_date TIMESTAMP DEFAULT NOW(),
	PRIMARY KEY(lecturer_id, course_id)
);


CREATE TABLE chairperson(
	id SERIAL PRIMARY KEY,
	lecturer_id BIGINT REFERENCES lecturer(id),
	start_date TIMESTAMP DEFAULT NOW(),
	end_date TIMESTAMP NOT NULL
);


CREATE TABLE exam_centre_staff(
	id SERIAL PRIMARY KEY,
	first_name TEXT NOT NULL,
	other_names TEXT,
	surname TEXT NOT NULL,
	email TEXT NOT NULL UNIQUE,
	password TEXT,
	active BOOLEAN DEFAULT TRUE,
	create_date TIMESTAMP DEFAULT NOW(),
	last_update TIMESTAMP DEFAULT NOW()
);


CREATE TABLE external_examiner(
	id SERIAL PRIMARY KEY,
	first_name TEXT NOT NULL,
	other_names TEXT,
	surname TEXT NOT NULL,
	email TEXT NOT NULL UNIQUE,
	password TEXT,
	active BOOLEAN DEFAULT TRUE,
	approved BOOLEAN DEFAULT TRUE,
	approved_by_chairperson_id BIGINT REFERENCES chairperson(id),
	create_date TIMESTAMP DEFAULT NOW(),
	last_update TIMESTAMP DEFAULT NOW()
);

CREATE TABLE external_examiner_course(
    id SERIAL PRIMARY KEY,
    ext_examiner_id BIGINT REFERENCES external_examiner(id),
    course_id BIGINT REFERENCES course(id),
    start_date TIMESTAMP NOT NULL,
    end_date TIMESTAMP NOT NULL,
    create_date TIMESTAMP DEFAULT NOW(),
    last_update TIMESTAMP DEFAULT NOW()
);

CREATE TABLE login_session(
    id SERIAL PRIMARY KEY,
    user_id BIGINT NOT NULL,
    user_type TEXT NOT NULL,
    two_fa_code TEXT NOT NULL,
    session_expiry TIMESTAMP NOT NULL,
    UNIQUE(user_id, user_type)
);

CREATE TABLE sys_admin(
    id SERIAL PRIMARY KEY,
    first_name TEXT NOT NULL,
    other_names TEXT,
    surname TEXT NOT NULL,
    email TEXT NOT NULL UNIQUE,
    password TEXT,
    active BOOLEAN DEFAULT TRUE,
    create_date TIMESTAMP DEFAULT NOW(),
    last_update TIMESTAMP DEFAULT NOW()
);

CREATE TABLE exam_period(
    id SERIAL PRIMARY KEY,
    name TEXT NOT NULL,
    start_date TIMESTAMP NOT NULL,
    create_date TIMESTAMP DEFAULT NOW(),
    last_update TIMESTAMP DEFAULT NOW()
);

CREATE TABLE course_schedule(
    id SERIAL PRIMARY KEY,
    course_id BIGINT REFERENCES course(id),
    exam_period_id BIGINT REFERENCES exam_period(id),
    exam_date TIMESTAMP NOT NULL,
    start_time TIMESTAMP NOT NULL,
    end_time TIMESTAMP NOT NULL,
    create_date TIMESTAMP DEFAULT NOW(),
    last_update TIMESTAMP DEFAULT NOW()
);

CREATE TABLE exams_created(
    id SERIAL PRIMARY KEY,
    exam_id TEXT NOT NULL UNIQUE,
    exam_course_id BIGINT REFERENCES course(id),
    exam_period_id BIGINT REFERENCES exam_period(id)
);

CREATE TABLE notification(
    id SERIAL PRIMARY KEY,
    type TEXT NOT NULL,
    sender_id BIGINT NOT NULL,
    sender_type TEXT NOT NULL,
    recipient_id BIGINT NOT NULL,
    recipient_type TEXT NOT NULL,
    seen BOOLEAN DEFAULT FALSE,
    target_type TEXT,
    target_id TEXT,
    create_date TIMESTAMP DEFAULT NOW(),
    last_update TIMESTAMP DEFAULT NOW()
);

-- todo: Add indexes


-- add insert statements
INSERT INTO university(name, logo)
VALUES
('University of Nairobi', '');

INSERT INTO degree(name, type, type_prefix, level)
VALUES
('Computer Science', 'BACHELOR_OF_SCIENCE'::degree_type, 'BSc', 'UNDERGRADUATE'::degree_level);

INSERT INTO course(name, course_code, year_of_study, semester, degree_id)
VALUES
('PRINCIPLES OF ENTREPRENEURSHIP AND BUSINESS MANAGEMENT', 'CSC_413', 4, 'FIRST_SEMESTER'::course_semester, 1),
('PROGRAMMING AND PROBLEM SOLVING', 'CSC_121', 1, 'SECOND_SEMESTER'::course_semester, 1),
('COMPUTER NETWORKS', 'CSC_225', 2, 'SECOND_SEMESTER'::course_semester, 1),
('SYSTEMS ANALYSIS AND DESIGN', 'CSC_212', 2, 'FIRST_SEMESTER'::course_semester, 1),
('DATABASE SYSTEMS', 'CSC_122', 1, 'SECOND_SEMESTER'::course_semester, 1),
('NETWORK AND DISTRIBUTED PROGRAMMING', 'CSC_322', 3, 'SECOND_SEMESTER'::course_semester, 1),
('IS CONTROL AND AUDIT', 'CSC_452', 4, 'SECOND_SEMESTER'::course_semester, 1),
('ICT PROJECT MANAGEMENT', 'CSC_321', 3, 'SECOND_SEMESTER'::course_semester, 1),
('SOFTWARE ENGINEERING', 'CSC_224', 2, 'SECOND_SEMESTER'::course_semester, 1);


INSERT INTO student(first_name, other_names, surname, registration_number, natl_id_number, email, password, year_of_study)
VALUES
('Philip', 'Ndiritu', 'Gichuhi', 'P15-1558-2015', 32664673, 'philipndiritu@gmail.com', 'philip', 4);

INSERT INTO student_course(student_id, course_id)
VALUES
(1, 1);

INSERT INTO lecturer(first_name, other_names, surname, email, password)
VALUES
('Janet', 'Wanjiku', 'Nganga', 'wanjiku.nganga@uonbi.ac.ke', 'wanjiku'),
('Christopher', '', 'Moturi', 'cmoturi@uonbi.ac.ke', 'moturi'),
('Tonny', '', 'Omwansa', 'tonny.omwansa@uonbi.ac.ke', 'omwansa'),
('Agnes', '', 'Wausi', 'wausi@uonbi.ac.ke', 'wausi');

INSERT INTO lecturer_course(lecturer_id, course_id)
VALUES
(1, 1),
(1, 4),
(1, 7),
(4, 5),
(5, 9);

INSERT INTO chairperson(lecturer_id, end_date)
VALUES
(2, (date '2018-07-13' + interval '1 year'));

INSERT INTO exam_centre_staff(first_name, other_names, surname, email, password)
VALUES
('James', 'Kamau', 'Njoroge', 'james.njoroge@uonbi.ac.ke', 'james');

INSERT INTO external_examiner(first_name, other_names, surname, email, password, approved_by_chairperson_id)
VALUES
('Timothy', 'Mwololo', 'Waema', 'timothy.waema@jkuat.ac.ke', 'waema', 1);

INSERT INTO external_examiner_course(ext_examiner_id, course_id, start_date, end_date)
VALUES
(1, 1, '2018-08-13 00:00:00', '2020-08-13 01:00:00'),
(1, 2, '2018-08-13 00:00:00', '2020-08-13 01:00:00'),
(1, 3, '2018-08-13 00:00:00', '2020-08-13 01:00:00'),
(1, 4, '2018-08-13 00:00:00', '2020-08-13 01:00:00'),
(1, 5, '2018-08-13 00:00:00', '2020-08-13 01:00:00'),
(1, 6, '2018-08-13 00:00:00', '2020-08-13 01:00:00'),
(1, 7, '2018-08-13 00:00:00', '2020-08-13 01:00:00'),
(1, 8, '2018-08-13 00:00:00', '2020-08-13 01:00:00'),
(1, 9, '2018-08-13 00:00:00', '2020-08-13 01:00:00');

INSERT INTO sys_admin(first_name, other_names, surname, email, password)
VALUES
('Philip', 'Ndiritu', 'Gichuhi', 'philip.ndiritu@uonbi.ac.ke', 'philip');